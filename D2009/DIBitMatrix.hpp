// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dibitmatrix.pas' rev: 20.00

#ifndef DibitmatrixHPP
#define DibitmatrixHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dibitmatrix
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TDIBitMatrix;
class PASCALIMPLEMENTATION TDIBitMatrix : public Dicontainers::TDIErrorObject
{
	typedef Dicontainers::TDIErrorObject inherited;
	
private:
	Ditypes::TCardinalArray *FMatrix;
	int FColCount;
	int FRowCount;
	
protected:
	void __fastcall SetColCount(const int NewColCount);
	void __fastcall SetRowCount(const int NewRowCount);
	virtual bool __fastcall GetBitValue(const int Row, const int Col);
	virtual void __fastcall SetBitValue(const int Row, const int Col, const bool Value);
	
public:
	__fastcall TDIBitMatrix(const int Rows, const int Cols);
	__fastcall virtual ~TDIBitMatrix(void);
	__property int ColCount = {read=FColCount, write=SetColCount, nodefault};
	__property int RowCount = {read=FRowCount, write=SetRowCount, nodefault};
	void __fastcall IncRowCount(void)/* overload */;
	void __fastcall IncRowCount(const int RowCountDelta)/* overload */;
	void __fastcall IncColCount(void)/* overload */;
	void __fastcall IncColCount(const int ColCountDelta)/* overload */;
	__property bool Bits[const int Row][const int Col] = {read=GetBitValue, write=SetBitValue/*, default*/};
	void __fastcall SetAll(void);
	void __fastcall ClearAll(void);
	virtual void __fastcall SetBit(const int Row, const int Col);
	virtual void __fastcall ClearBit(const int Row, const int Col);
	virtual void __fastcall SetColFromTo(const int Col, const int FirstRow, const int LastRow);
	virtual void __fastcall SetColRange(const int Col, const int FirstRow, int Count);
	virtual void __fastcall ClearColFromTo(const int Col, const int FirstRow, const int LastRow);
	virtual void __fastcall ClearColRange(const int Col, const int FirstRow, int Count);
	virtual void __fastcall SetRowFromTo(const int Row, int FirstCol, const int LastCol);
	virtual void __fastcall SetRowRange(const int Row, const int FirstCol, const int Count);
	virtual void __fastcall ClearRowFromTo(const int Row, int FirstCol, const int LastCol);
	virtual void __fastcall ClearRowRange(const int Row, const int FirstCol, const int Count);
	virtual void __fastcall SetRect(int FirstRow, const int FirstCol, const int LastRow, const int LastCol);
	virtual void __fastcall SetBounds(const int FirstRow, const int FirstCol, const int RowWidth, const int ColWidth);
	virtual void __fastcall ClearRect(int FirstRow, const int FirstCol, const int LastRow, const int LastCol);
	virtual void __fastcall ClearBounds(const int FirstRow, const int FirstCol, const int RowWidth, const int ColWidth);
	int __fastcall NextClearColRight(const int Row, const int StartCol = 0x0);
	int __fastcall NextClearRowDown(const int Col, const int StartRow = 0x0);
	void __fastcall NextClearBitDownRight(int &StartRow, int &StartCol);
public:
	/* TObject.Create */ inline __fastcall TDIBitMatrix(void) : Dicontainers::TDIErrorObject() { }
	
};


class DELPHICLASS TDIBitMatrixAutoGrow;
class PASCALIMPLEMENTATION TDIBitMatrixAutoGrow : public TDIBitMatrix
{
	typedef TDIBitMatrix inherited;
	
protected:
	virtual bool __fastcall GetBitValue(const int Row, const int Col);
	virtual void __fastcall SetBitValue(const int Row, const int Col, const bool Value);
	
public:
	virtual void __fastcall SetBit(const int Row, const int Col);
	virtual void __fastcall ClearBit(const int Row, const int Col);
	virtual void __fastcall SetColFromTo(const int Col, const int FirstRow, const int LastRow);
	virtual void __fastcall ClearColFromTo(const int Col, const int FirstRow, const int LastRow);
	virtual void __fastcall SetRowFromTo(const int Row, int FirstCol, const int LastCol);
	virtual void __fastcall ClearRowFromTo(const int Row, int FirstCol, const int LastCol);
	virtual void __fastcall ClearRect(int FirstRow, const int FirstCol, const int LastRow, const int LastCol);
	virtual void __fastcall SetRect(int FirstRow, const int FirstCol, const int LastRow, const int LastCol);
public:
	/* TDIBitMatrix.CreateWithDimensions */ inline __fastcall TDIBitMatrixAutoGrow(const int Rows, const int Cols) : TDIBitMatrix(Rows, Cols) { }
	/* TDIBitMatrix.Destroy */ inline __fastcall virtual ~TDIBitMatrixAutoGrow(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TDIBitMatrixAutoGrow(void) : TDIBitMatrix() { }
	
};


//-- var, const, procedure ---------------------------------------------------

}	/* namespace Dibitmatrix */
using namespace Dibitmatrix;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DibitmatrixHPP
