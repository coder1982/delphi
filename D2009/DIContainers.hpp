// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dicontainers.pas' rev: 20.00

#ifndef DicontainersHPP
#define DicontainersHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Distreams.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dicontainers
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TDIStreamInfo { siMagic, siVersion, siHeader };
#pragma option pop

typedef Set<TDIStreamInfo, siMagic, siHeader>  TDIStreamInfos;

typedef unsigned TDIStreamMagic;

typedef int __fastcall (*TDICompareItemsFunc)(const void * PItem1, const void * PItem2);

typedef int __fastcall (*TDICompareItemsExFunc)(const void * PItem1, const void * PItem2, const void * Extra);

typedef void __fastcall (*TDICopyItemProc)(const void * PSourceItem, const void * PDestItem);

typedef void __fastcall (*TDIFreeItemProc)(const void * PItem);

typedef void __fastcall (*TDIInitItemProc)(const void * PItem);

typedef void __fastcall (__closure *TDIIterateItemProc)(const void * PItem);

typedef bool __fastcall (__closure *TDIIterateItemExFunc)(const void * PItem, const void * Extra);

typedef bool __fastcall (*TDISameItemsFunc)(const void * PItem1, const void * PItem2);

typedef bool __fastcall (*TDISameItemsExFunc)(const void * PItem1, const void * PItem2, const void * Extra);

class DELPHICLASS TDIContainer;
typedef void __fastcall (*TDIReadHeaderProc)(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * Extra);

typedef void __fastcall (*TDIReadItemProc)(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);

typedef void __fastcall (*TDIWriteHeaderProc)(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * Extra);

typedef void __fastcall (*TDIWriteItemProc)(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);

class DELPHICLASS EDIContainerError;
class PASCALIMPLEMENTATION EDIContainerError : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EDIContainerError(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EDIContainerError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall EDIContainerError(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall EDIContainerError(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall EDIContainerError(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EDIContainerError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EDIContainerError(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EDIContainerError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EDIContainerError(void) { }
	
};


class DELPHICLASS TDIErrorObject;
class PASCALIMPLEMENTATION TDIErrorObject : public Classes::TPersistent
{
	typedef Classes::TPersistent inherited;
	
public:
	__classmethod void __fastcall Error(const System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TDIErrorObject(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TDIErrorObject(void) : Classes::TPersistent() { }
	
};


class DELPHICLASS TDIBaseHandler;
class PASCALIMPLEMENTATION TDIBaseHandler : public TDIErrorObject
{
	typedef TDIErrorObject inherited;
	
protected:
	unsigned FRefCount;
	
public:
	__fastcall virtual ~TDIBaseHandler(void);
	__property unsigned RefCount = {read=FRefCount, nodefault};
public:
	/* TObject.Create */ inline __fastcall TDIBaseHandler(void) : TDIErrorObject() { }
	
};


class DELPHICLASS TDIItemHandler;
class PASCALIMPLEMENTATION TDIItemHandler : public TDIBaseHandler
{
	typedef TDIBaseHandler inherited;
	
private:
	int FItemSize;
	TDIInitItemProc FOnInitItem;
	TDIFreeItemProc FOnFreeItem;
	TDICompareItemsFunc FOnCompareItems;
	TDICopyItemProc FOnCopyItem;
	TDIReadItemProc FOnReadItem;
	TDIWriteItemProc FOnWriteItem;
	TDIReadHeaderProc FOnReadHeader;
	TDIWriteHeaderProc FOnWriteHeader;
	void __fastcall SetItemSize(const int NewItemSize);
	void __fastcall SetOnInitItem(const TDIInitItemProc AValue);
	void __fastcall SetOnFreeItem(const TDIFreeItemProc AValue);
	void __fastcall SetOnCompareItems(const TDICompareItemsFunc AValue);
	void __fastcall SetOnCopyItem(const TDICopyItemProc AValue);
	void __fastcall SetOnReadItem(const TDIReadItemProc AValue);
	void __fastcall SetOnWriteItem(const TDIWriteItemProc AValue);
	void __fastcall SetOnReadHeader(const TDIReadHeaderProc AValue);
	void __fastcall SetOnWriteHeader(const TDIWriteHeaderProc AValue);
	
public:
	__property int ItemSize = {read=FItemSize, write=SetItemSize, nodefault};
	__property TDICompareItemsFunc OnCompareItems = {read=FOnCompareItems, write=SetOnCompareItems};
	__property TDICopyItemProc OnCopyItem = {read=FOnCopyItem, write=SetOnCopyItem};
	__property TDIFreeItemProc OnFreeItem = {read=FOnFreeItem, write=SetOnFreeItem};
	__property TDIInitItemProc OnInitItem = {read=FOnInitItem, write=SetOnInitItem};
	__property TDIReadHeaderProc OnReadHeader = {read=FOnReadHeader, write=SetOnReadHeader};
	__property TDIReadItemProc OnReadItem = {read=FOnReadItem, write=SetOnReadItem};
	__property TDIWriteHeaderProc OnWriteHeader = {read=FOnWriteHeader, write=SetOnWriteHeader};
	__property TDIWriteItemProc OnWriteItem = {read=FOnWriteItem, write=SetOnWriteItem};
public:
	/* TDIBaseHandler.Destroy */ inline __fastcall virtual ~TDIItemHandler(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TDIItemHandler(void) : TDIBaseHandler() { }
	
};


class PASCALIMPLEMENTATION TDIContainer : public TDIErrorObject
{
	typedef TDIErrorObject inherited;
	
private:
	TDIItemHandler* FItemHandler;
	void __fastcall SetItemHandler(const TDIItemHandler* AValue);
	
protected:
	int FCount;
	virtual void __fastcall DefineProperties(Classes::TFiler* Filer);
	
public:
	__fastcall TDIContainer(const TDIItemHandler* AItemHandler);
	__fastcall virtual ~TDIContainer(void);
	virtual void __fastcall Clear(void) = 0 ;
	bool __fastcall ExistAll(void * const *PItems, const int PItems_Size, const TDISameItemsFunc SameItems);
	virtual bool __fastcall Exists(const void * PItem, const TDISameItemsFunc SameItems) = 0 ;
	bool __fastcall ExistsAny(void * const *PItems, const int PItems_Size, const TDISameItemsFunc SameItems);
	virtual bool __fastcall ExistsBack(const void * PItem, const TDISameItemsFunc SameItems) = 0 ;
	bool __fastcall IsEmpty(void);
	bool __fastcall IsNotEmpty(void);
	virtual void __fastcall Iterate(const TDIIterateItemProc IterateItem) = 0 ;
	void __fastcall LoadFromFile(const System::UnicodeString FileName, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	void __fastcall LoadFromStream(Classes::TStream* Stream);
	void __fastcall LoadFromCustomStream(const Classes::TStream* Stream, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	virtual void * __fastcall PItemOf(const void * PItem, const TDISameItemsFunc SameItems) = 0 ;
	virtual void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsFunc SameItems) = 0 ;
	void __fastcall SaveToFile(const System::UnicodeString FileName, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	void __fastcall SaveToStream(Classes::TStream* Stream);
	void __fastcall SaveToCustomStream(const Classes::TStream* Stream, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	virtual void __fastcall ReadContainer(const Distreams::TDIBinaryReader* Reader, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0)) = 0 ;
	virtual void __fastcall WriteContainer(const Distreams::TDIBinaryWriter* Writer, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0)) = 0 ;
	__property int Count = {read=FCount, nodefault};
	__property TDIItemHandler* ItemHandler = {read=FItemHandler, write=SetItemHandler};
};


class DELPHICLASS TDILinearContainer;
class PASCALIMPLEMENTATION TDILinearContainer : public TDIContainer
{
	typedef TDIContainer inherited;
	
public:
	virtual void * __fastcall InsertItemFirst(void) = 0 ;
	virtual void * __fastcall InsertItemFirstNoInit(void) = 0 ;
	virtual void * __fastcall InsertItemLast(void) = 0 ;
	virtual void * __fastcall InsertItemLastNoInit(void) = 0 ;
	virtual void * __fastcall PFirstItem(void) = 0 ;
	virtual void * __fastcall plastitem(void) = 0 ;
public:
	/* TDIContainer.Create */ inline __fastcall TDILinearContainer(const TDIItemHandler* AItemHandler) : TDIContainer(AItemHandler) { }
	/* TDIContainer.Destroy */ inline __fastcall virtual ~TDILinearContainer(void) { }
	
};


typedef int TDIHashNext;

typedef int *PDIHashNext;

typedef StaticArray<int, 536870911> TDIHashNextArray;

typedef TDIHashNextArray *PDIHashNextArray;

typedef unsigned __fastcall (*TDIHashKeyFunc)(const void * PKey);

typedef unsigned __fastcall (*TDIHashKeyBufFunc)(const void * PKey, const unsigned KeySize);

typedef bool __fastcall (*TDISameKeysFunc)(const void * PKey1, const void * PKey2);

typedef bool __fastcall (*TDISameKeyBufFunc)(const void * PKey1, const unsigned Key1Size, const void * PKey2);

typedef void __fastcall (*TDIStoreKeyProc)(const void * PKeySource, const void * PKeyDest);

typedef void __fastcall (*TDIStoreKeyBufProc)(const void * PKeySource, const unsigned PKeySourceSize, const void * PKeyDest);

typedef void __fastcall (*TDIFreeKeyProc)(const void * PKey);

class DELPHICLASS TDIHash;
typedef void __fastcall (*TDIReadKeyProc)(const TDIHash* Sender, const Distreams::TDIBinaryReader* Reader, const void * PKey, const void * Extra = (void *)(0x0));

typedef void __fastcall (*TDIWriteKeyProc)(const TDIHash* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PKey, const void * Extra = (void *)(0x0));

class DELPHICLASS TDIKeyHandler;
class PASCALIMPLEMENTATION TDIKeyHandler : public TDIBaseHandler
{
	typedef TDIBaseHandler inherited;
	
private:
	int FKeySize;
	TDIHashKeyFunc FOnHashKey;
	TDIHashKeyBufFunc FOnHashKeyBuf;
	TDISameKeysFunc FOnSameKeys;
	TDISameKeyBufFunc FOnSameKeyBuf;
	TDIStoreKeyProc FOnStoreKey;
	TDIStoreKeyBufProc FOnStoreKeyBuf;
	TDIFreeKeyProc FOnFreeKey;
	TDIReadKeyProc FOnReadKey;
	TDIWriteKeyProc FOnWriteKey;
	void __fastcall SetKeySize(const int NewKeySize);
	void __fastcall SetOnHashKey(const TDIHashKeyFunc AValue);
	void __fastcall SetOnHashKeyBuf(const TDIHashKeyBufFunc AValue);
	void __fastcall SetOnSameKeys(const TDISameKeysFunc AValue);
	void __fastcall SetOnSameKeyBuf(const TDISameKeyBufFunc AValue);
	void __fastcall SetOnStoreKey(const TDIStoreKeyProc AValue);
	void __fastcall SetOnStoreKeyBuf(const TDIStoreKeyBufProc AValue);
	void __fastcall SetOnFreeKey(const TDIFreeKeyProc AValue);
	void __fastcall SetOnReadKey(const TDIReadKeyProc AValue);
	void __fastcall SetOnWriteKey(const TDIWriteKeyProc AValue);
	
public:
	__property int KeySize = {read=FKeySize, write=SetKeySize, nodefault};
	__property TDIHashKeyFunc OnHashKey = {read=FOnHashKey, write=SetOnHashKey};
	__property TDIHashKeyBufFunc OnHashKeyBuf = {read=FOnHashKeyBuf, write=SetOnHashKeyBuf};
	__property TDISameKeysFunc OnSameKeys = {read=FOnSameKeys, write=SetOnSameKeys};
	__property TDISameKeyBufFunc OnSameKeyBuf = {read=FOnSameKeyBuf, write=SetOnSameKeyBuf};
	__property TDIStoreKeyBufProc OnStoreKeyBuf = {read=FOnStoreKeyBuf, write=SetOnStoreKeyBuf};
	__property TDIStoreKeyProc OnStoreKey = {read=FOnStoreKey, write=SetOnStoreKey};
	__property TDIFreeKeyProc OnFreeKey = {read=FOnFreeKey, write=SetOnFreeKey};
	__property TDIReadKeyProc OnReadKey = {read=FOnReadKey, write=SetOnReadKey};
	__property TDIWriteKeyProc OnWriteKey = {read=FOnWriteKey, write=SetOnWriteKey};
public:
	/* TDIBaseHandler.Destroy */ inline __fastcall virtual ~TDIKeyHandler(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TDIKeyHandler(void) : TDIBaseHandler() { }
	
};


class PASCALIMPLEMENTATION TDIHash : public TDIContainer
{
	typedef TDIContainer inherited;
	
private:
	int FHashCount;
	TDIKeyHandler* FKeyHandler;
	void __fastcall ClearVector(void);
	void __fastcall SetHashCount(const int NewHashCount);
	
protected:
	int FFirstDeleted;
	TDIHashNextArray *FHash;
	void *FVector;
	int FVectorCount;
	int __fastcall GetIndexOfKeyBufEx(const void *AKey, const unsigned AKeySize, /* out */ unsigned &Hash, /* out */ int &Previous);
	int __fastcall GetIndexOfKeyEx(const void *AKey, /* out */ unsigned &Hash, /* out */ int &Previous);
	void * __fastcall GetPItemOfKeyEx(const void *AKey, /* out */ unsigned &Hash, /* out */ int &Previous);
	void * __fastcall GetPItemOfKeyBufEx(const void *AKey, const unsigned AKeySize, /* out */ unsigned &Hash, /* out */ int &Previous);
	void __fastcall GrowCapacity(const int NewCapacity);
	void __fastcall RebuildHash(void);
	void __fastcall ResetHash(void);
	void __fastcall ShrinkCapacity(const int NewCapacity);
	
public:
	__fastcall TDIHash(const TDIItemHandler* AItemHandler, const TDIKeyHandler* AKeyHandler);
	__fastcall virtual ~TDIHash(void);
	int __fastcall Chains(void);
	virtual void __fastcall Clear(void);
	void __fastcall DeleteByKey(const void *AKey);
	void __fastcall DeleteByKeyNoFree(const void *AKey);
	virtual bool __fastcall Exists(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall Exists(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	virtual bool __fastcall ExistsBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall ExistsBack(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	bool __fastcall ExistsKey(const void *AKey);
	bool __fastcall ExistsKeyBuf(const void *AKey, const unsigned AKeySize);
	int __fastcall IndexOfKey(const void *AKey);
	int __fastcall IndexOfKeyBuf(const void *AKey, const unsigned AKeySize);
	void * __fastcall InsertItemByKey(const void *AKey);
	void * __fastcall InsertItemByKeyNoInit(const void *AKey);
	void * __fastcall InsertItemByKeyBuf(const void *AKey, const unsigned AKeySize);
	void * __fastcall InsertItemByKeyBufNoInit(const void *AKey, const unsigned AKeySize);
	bool __fastcall IsDeleted(const int Index);
	virtual void __fastcall Iterate(const TDIIterateItemProc IterateItem)/* overload */;
	HIDESBASE bool __fastcall Iterate(const TDIIterateItemExFunc IterateItem, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	void __fastcall Pack(void);
	void * __fastcall PItemAt(const int Index);
	virtual void * __fastcall PItemOf(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOf(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	virtual void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	void * __fastcall PItemOfKey(const void *AKey);
	void * __fastcall PItemOfKeyBuf(const void *AKey, const unsigned AKeySize);
	void * __fastcall PItemOfKeyOrInsert(const void *AKey);
	void * __fastcall PKeyAt(const int Index);
	virtual void __fastcall ReadContainer(const Distreams::TDIBinaryReader* Reader, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	virtual void __fastcall WriteContainer(const Distreams::TDIBinaryWriter* Writer, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	__property int HashCount = {read=FHashCount, write=SetHashCount, nodefault};
	__property TDIKeyHandler* KeyHandler = {read=FKeyHandler};
	__property void * Vector = {read=FVector};
	__property int VectorCount = {read=FVectorCount, nodefault};
};


struct TDIListNode;
typedef TDIListNode *PDIListNode;

#pragma pack(push,1)
struct TDIListNode
{
	
private:
	struct _TDIListNode__1
	{
		
	};
	
	
	
public:
	TDIListNode *PPreviousNode;
	TDIListNode *PNextNode;
	_TDIListNode__1 Data;
};
#pragma pack(pop)


class DELPHICLASS TDIListEnumerator;
class DELPHICLASS TDIList;
class PASCALIMPLEMENTATION TDIListEnumerator : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TDIListNode *FItem;
	TDIList* FList;
	
public:
	__fastcall TDIListEnumerator(const TDIList* AList);
	void * __fastcall GetCurrent(void);
	bool __fastcall MoveNext(void);
	__property void * Current = {read=GetCurrent};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TDIListEnumerator(void) { }
	
};


class DELPHICLASS TDITree;
class DELPHICLASS TDIVector;
class PASCALIMPLEMENTATION TDIList : public TDILinearContainer
{
	typedef TDILinearContainer inherited;
	
protected:
	TDIListNode *FPFirstNode;
	TDIListNode *FPLastNode;
	void __fastcall InternalConnectNodeAfter(const PDIListNode PNode, const PDIListNode PDestinationNode);
	void __fastcall InternalConnectNodeBefore(const PDIListNode PNode, PDIListNode PDestinationNode);
	void __fastcall InternalConnectNodeFirst(const PDIListNode PNode);
	void __fastcall InternalConnectNodeLast(const PDIListNode PNode);
	void __fastcall InternalDisconnectNode(const PDIListNode PNode);
	
public:
	__fastcall virtual ~TDIList(void);
	virtual void __fastcall Assign(Classes::TPersistent* Source);
	void __fastcall AssignDIList(const TDIList* Source);
	void __fastcall AssignDITree(const TDITree* Source);
	void __fastcall AssignDIVector(const TDIVector* Source);
	virtual void __fastcall Clear(void);
	void __fastcall Delete(const void * PItem);
	void __fastcall DeleteNoFree(const void * PItem);
	void __fastcall DeleteFirst(void);
	void __fastcall DeleteFirstNoFree(void);
	void __fastcall DeleteLast(void);
	void __fastcall DeleteLastNoFree(void);
	virtual bool __fastcall Exists(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall Exists(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	virtual bool __fastcall ExistsBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall ExistsBack(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	HIDESBASE bool __fastcall ExistAll(void * const *PItems, const int PItems_Size, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	HIDESBASE bool __fastcall ExistsAny(void * const *PItems, const int PItems_Size, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	TDIListEnumerator* __fastcall GetEnumerator(void);
	void * __fastcall InsertItemAfter(const void * PItem);
	void * __fastcall InsertItemAfterNoInit(const void * PItem);
	void * __fastcall InsertItemBefore(const void * PItem);
	void * __fastcall InsertItemBeforeNoInit(const void * PItem);
	virtual void * __fastcall InsertItemFirst(void);
	virtual void * __fastcall InsertItemFirstNoInit(void);
	virtual void * __fastcall InsertItemLast(void);
	virtual void * __fastcall InsertItemLastNoInit(void);
	virtual void __fastcall Iterate(const TDIIterateItemProc AItemCallback)/* overload */;
	HIDESBASE bool __fastcall Iterate(const TDIIterateItemExFunc AItemCallback, const void * AStartItemPtr, const void * AExtraPtr = (void *)(0x0))/* overload */;
	void __fastcall MoveAfter(const void * PSourceItem, const void * PTargetItem);
	void __fastcall MoveBefore(const void * PSourceItem, const void * PTargetItem);
	void __fastcall MoveFirst(const void * PItem);
	void __fastcall MoveLast(const void * PItem);
	virtual void * __fastcall PFirstItem(void);
	virtual void * __fastcall plastitem(void);
	void * __fastcall PNextItem(const void * PItem);
	void * __fastcall PPreviousItem(const void * AItem);
	virtual void * __fastcall PItemOf(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOf(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	virtual void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	virtual void __fastcall ReadContainer(const Distreams::TDIBinaryReader* Reader, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	void __fastcall RemoveAll(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	void __fastcall RemoveAll(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	void __fastcall RemoveAllBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	void __fastcall RemoveAllBack(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	void __fastcall Sort(const TDICompareItemsFunc CompareItems)/* overload */;
	void __fastcall Sort(const TDICompareItemsExFunc CompareItems, const void * Extra = (void *)(0x0))/* overload */;
	void __fastcall SortDesc(const TDICompareItemsFunc CompareItems)/* overload */;
	void __fastcall SortDesc(const TDICompareItemsExFunc CompareItems, const void * Extra = (void *)(0x0))/* overload */;
	virtual void __fastcall WriteContainer(const Distreams::TDIBinaryWriter* Writer, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
public:
	/* TDIContainer.Create */ inline __fastcall TDIList(const TDIItemHandler* AItemHandler) : TDILinearContainer(AItemHandler) { }
	
};


struct TDITreeNode;
typedef TDITreeNode *PDITreeNode;

#pragma pack(push,1)
struct TDITreeNode
{
	
private:
	struct _TDITreeNode__1
	{
		
	};
	
	
	
public:
	TDITreeNode *PParentNode;
	TDITreeNode *PPrevSiblingNode;
	TDITreeNode *PNextSiblingNode;
	TDITreeNode *PFirstChildNode;
	TDITreeNode *PLastChildNode;
	_TDITreeNode__1 Data;
};
#pragma pack(pop)


class DELPHICLASS TDITreeEnumerator;
class PASCALIMPLEMENTATION TDITreeEnumerator : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TDITreeNode *FItem;
	TDITree* FTree;
	
public:
	__fastcall TDITreeEnumerator(const TDITree* ATree);
	void * __fastcall GetCurrent(void);
	bool __fastcall MoveNext(void);
	__property void * Current = {read=GetCurrent};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TDITreeEnumerator(void) { }
	
};


class PASCALIMPLEMENTATION TDITree : public TDILinearContainer
{
	typedef TDILinearContainer inherited;
	
private:
	void __fastcall ReadNode(const PDITreeNode PNode, const Distreams::TDIBinaryReader* Reader, const void * Extra);
	void __fastcall ReadNodeChildren(const PDITreeNode PNode, const Distreams::TDIBinaryReader* Reader, const void * Extra);
	void __fastcall writeNode(const PDITreeNode PNode, const Distreams::TDIBinaryWriter* Writer, const void * Extra);
	void __fastcall WriteNodeChildren(const PDITreeNode PNode, const Distreams::TDIBinaryWriter* Writer, const void * Extra);
	
protected:
	TDITreeNode FRootNode;
	void __fastcall InternalConnectNodeAfter(const PDITreeNode PNode, const PDITreeNode PDestinationNode);
	void __fastcall InternalConnectNodeBefore(const PDITreeNode PNode, const PDITreeNode PDestinationNode);
	void __fastcall InternalConnectNodeChildFirst(const PDITreeNode PNode, const PDITreeNode PDestinationNode);
	void __fastcall InternalConnectNodeChildLast(const PDITreeNode PNode, const PDITreeNode PDestinationNode);
	void __fastcall InternalDeleteNodeChildren(const PDITreeNode PNode);
	void __fastcall InternalDeleteNodeChildrenNoFree(const PDITreeNode PNode);
	void __fastcall InternalDisconnectNode(const PDITreeNode PNode);
	__classmethod PDITreeNode __fastcall InternalGetLastGrandChildNode(const PDITreeNode ANode);
	__classmethod PDITreeNode __fastcall InternalGetNextNode(const PDITreeNode ANode);
	__classmethod int __fastcall InternalGetNodeChildCount(const PDITreeNode PNode);
	__classmethod PDITreeNode __fastcall InternalGetPreviousNode(const PDITreeNode ANode);
	PDITreeNode __fastcall InternalInsertNodeChildLast(const PDITreeNode PNode);
	void __fastcall InternalSort(const PDITreeNode PNode, const TDICompareItemsFunc CompareItems)/* overload */;
	void __fastcall InternalSort(const PDITreeNode PNode, const TDICompareItemsExFunc CompareItems, const void * Extra)/* overload */;
	
public:
	__fastcall virtual ~TDITree(void);
	virtual void __fastcall Assign(Classes::TPersistent* Source);
	void __fastcall AssignDIList(const TDIList* Source);
	void __fastcall AssignDITree(const TDITree* Source);
	void __fastcall AssignDIVector(const TDIVector* Source);
	virtual void __fastcall Clear(void);
	void __fastcall Delete(const void * PItem);
	void __fastcall DeleteNoFree(const void * PItem);
	void __fastcall DeleteChildren(const void * PItem);
	void __fastcall DeleteChildrenNoFree(const void * PItem);
	virtual bool __fastcall Exists(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall Exists(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	virtual bool __fastcall ExistsBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall ExistsBack(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	HIDESBASE bool __fastcall ExistAll(void * const *PItems, const int PItems_Size, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	HIDESBASE bool __fastcall ExistsAny(void * const *PItems, const int PItems_Size, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	TDITreeEnumerator* __fastcall GetEnumerator(void);
	bool __fastcall HasChildren(const void * PItem);
	__classmethod bool __fastcall HasAsParent(const void * PItem, const void * PPotentialParentItem);
	virtual void * __fastcall InsertItemFirst(void);
	virtual void * __fastcall InsertItemFirstNoInit(void);
	virtual void * __fastcall InsertItemLast(void);
	virtual void * __fastcall InsertItemLastNoInit(void);
	void * __fastcall InsertItemAfter(const void * PItem);
	void * __fastcall InsertItemAfterNoInit(const void * PItem);
	void * __fastcall InsertItemBefore(const void * PItem);
	void * __fastcall InsertItemBeforeNoInit(const void * PItem);
	void * __fastcall InsertItemChildFirst(const void * PItem);
	void * __fastcall InsertItemChildFirstNoInit(const void * PItem);
	void * __fastcall InsertItemChildLast(const void * PItem);
	void * __fastcall InsertItemChildLastNoInit(const void * PItem);
	virtual void __fastcall Iterate(const TDIIterateItemProc AItemCallback)/* overload */;
	HIDESBASE bool __fastcall Iterate(const TDIIterateItemExFunc AItemCallback, const void * AStartItemPtr, const void * AExtraPtr = (void *)(0x0))/* overload */;
	__classmethod int __fastcall Level(const void * PItem);
	void __fastcall MoveAfter(const void * PSourceItem, const void * PTargetItem);
	void __fastcall MoveBefore(const void * PSourceItem, const void * PTargetItem);
	void __fastcall MoveChildFirst(const void * PSourceItem, const void * PTargetItem);
	void __fastcall MoveChildLast(const void * PSourceItem, const void * PTargetItem);
	virtual void * __fastcall PFirstItem(void);
	void * __fastcall PFirstChildItem(const void * PItem);
	void * __fastcall PFirstGrandChildItem(const void * PItem);
	__classmethod void * __fastcall PFirstSiblingItem(const void * PItem);
	virtual void * __fastcall PItemOf(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOf(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	virtual void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsExFunc SameItems, const void * PStartItem, const void * Extra = (void *)(0x0))/* overload */;
	void * __fastcall PItemOfChildren(const void * PItem, const TDISameItemsFunc SameItems, const void * PStartItem = (void *)(0x0));
	void * __fastcall PItemOfChildrenBack(const void * PItem, const TDISameItemsFunc SameItems, const void * PStartItem = (void *)(0x0));
	void * __fastcall PItemOfSiblings(const void * PItem, const TDISameItemsFunc SameItems, const void * PStartItem = (void *)(0x0));
	virtual void * __fastcall plastitem(void);
	void * __fastcall PLastChildItem(const void * PItem);
	void * __fastcall PLastGrandChildItem(const void * PItem);
	__classmethod void * __fastcall PLastSiblingItem(const void * PItem);
	__classmethod void * __fastcall PNextItem(const void * PItem);
	__classmethod void * __fastcall PNextSiblingItem(const void * PItem);
	__classmethod void * __fastcall PParentItem(const void * PItem);
	__classmethod void * __fastcall PPreviousItem(const void * PItem);
	__classmethod void * __fastcall PPreviousSiblingItem(const void * PItem);
	virtual void __fastcall ReadContainer(const Distreams::TDIBinaryReader* Reader, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	void __fastcall Sort(const void * PItem, const TDICompareItemsFunc CompareItems)/* overload */;
	void __fastcall Sort(const void * PItem, const TDICompareItemsExFunc CompareItems, const void * Extra)/* overload */;
	void __fastcall SortRecurse(const void * PItem, const TDICompareItemsFunc CompareItems)/* overload */;
	void __fastcall SortRecurse(const void * PItem, const TDICompareItemsExFunc CompareItems, const void * Extra)/* overload */;
	virtual void __fastcall WriteContainer(const Distreams::TDIBinaryWriter* Writer, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
public:
	/* TDIContainer.Create */ inline __fastcall TDITree(const TDIItemHandler* AItemHandler) : TDILinearContainer(AItemHandler) { }
	
};


class DELPHICLASS TDIVectorEnumerator;
class PASCALIMPLEMENTATION TDIVectorEnumerator : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	int FindEx;
	TDIVector* FVector;
	
public:
	__fastcall TDIVectorEnumerator(const TDIVector* AVector);
	void * __fastcall GetCurrent(void);
	bool __fastcall MoveNext(void);
	__property void * Current = {read=GetCurrent};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TDIVectorEnumerator(void) { }
	
};


class PASCALIMPLEMENTATION TDIVector : public TDILinearContainer
{
	typedef TDILinearContainer inherited;
	
protected:
	void *FVector;
	void __fastcall GrowCapacity(const int NewCapacity);
	void __fastcall ShrinkCapacity(const int NewCapacity);
	void __fastcall ExchangeItems(const void * Item1, const void * Item2);
	void __fastcall SetCount(const int NewCount);
	void __fastcall SetCountNoInit(const int NewCount);
	
public:
	__fastcall virtual ~TDIVector(void);
	void __fastcall AppendDIVector(const TDIVector* ASource);
	virtual void __fastcall Assign(Classes::TPersistent* Source);
	void __fastcall AssignDIList(const TDIList* Source);
	void __fastcall AssignDITree(const TDITree* Source);
	void __fastcall AssignDIVector(const TDIVector* Source);
	virtual void __fastcall Clear(void);
	void __fastcall ClearNoFree(void);
	void __fastcall Delete(const void * PItem);
	void __fastcall DeleteAt(const int Index);
	void __fastcall DeleteAtNoFree(const int Index);
	void __fastcall DeleteFirst(void);
	void __fastcall DeleteLast(void);
	void __fastcall Exchange(const int Index1, const int Index2);
	virtual bool __fastcall Exists(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall Exists(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	virtual bool __fastcall ExistsBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE bool __fastcall ExistsBack(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	HIDESBASE bool __fastcall ExistsAny(void * const *PItems, const int PItems_Size, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	HIDESBASE bool __fastcall ExistAll(void * const *PItems, const int PItems_Size, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	bool __fastcall Find(const void * PItem, const TDICompareItemsFunc CompareItems, /* out */ int &Index)/* overload */;
	bool __fastcall Find(const void * PItem, const TDICompareItemsExFunc CompareItems, /* out */ int &Index, const void * Extra)/* overload */;
	bool __fastcall FindDesc(const void * PItem, const TDICompareItemsFunc CompareItems, /* out */ int &Index)/* overload */;
	bool __fastcall FindDesc(const void * PItem, const TDICompareItemsExFunc CompareItems, /* out */ int &Index, const void * Extra)/* overload */;
	TDIVectorEnumerator* __fastcall GetEnumerator(void);
	int __fastcall IndexOf(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	int __fastcall IndexOf(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	int __fastcall IndexOfBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	int __fastcall IndexOfBack(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	virtual void * __fastcall InsertItemFirst(void);
	virtual void * __fastcall InsertItemFirstNoInit(void);
	virtual void * __fastcall InsertItemLast(void);
	virtual void * __fastcall InsertItemLastNoInit(void);
	void * __fastcall InsertItemAt(const int Index);
	void * __fastcall InsertItemAtNoInit(const int Index);
	virtual void __fastcall Iterate(const TDIIterateItemProc IterateItem)/* overload */;
	HIDESBASE bool __fastcall Iterate(const TDIIterateItemExFunc IterateItem, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	virtual void * __fastcall PFirstItem(void);
	void * __fastcall PItemAt(const int Index);
	virtual void * __fastcall PItemOf(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOf(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	virtual void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	HIDESBASE void * __fastcall PItemOfBack(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	virtual void * __fastcall plastitem(void);
	void __fastcall Move(const int CurrentIndex, const int NewIndex);
	virtual void __fastcall ReadContainer(const Distreams::TDIBinaryReader* Reader, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	virtual void __fastcall WriteContainer(const Distreams::TDIBinaryWriter* Writer, const TDIStreamInfos StreamInfos = (TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	void __fastcall RemoveAll(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	void __fastcall RemoveAll(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra = (void *)(0x0))/* overload */;
	void __fastcall RemoveAllBack(const void * PItem, const TDISameItemsFunc SameItems)/* overload */;
	void __fastcall RemoveAllBack(const void * PItem, const TDISameItemsExFunc SameItems, const int StartIndex, const void * Extra)/* overload */;
	void __fastcall Sort(const TDICompareItemsFunc CompareItems)/* overload */;
	void __fastcall Sort(const TDICompareItemsExFunc CompareItems, const void * Extra)/* overload */;
	void __fastcall SortDesc(const TDICompareItemsFunc CompareItems)/* overload */;
	void __fastcall SortDesc(const TDICompareItemsExFunc CompareItems, const void * Extra)/* overload */;
	__property int Count = {read=FCount, write=SetCount, nodefault};
	__property void * Vector = {read=FVector};
public:
	/* TDIContainer.Create */ inline __fastcall TDIVector(const TDIItemHandler* AItemHandler) : TDILinearContainer(AItemHandler) { }
	
};


//-- var, const, procedure ---------------------------------------------------
#define DEFAULT_STREAM_INFOS (Set<TDIStreamInfo, siMagic, siHeader> () << siMagic << siVersion << siHeader )
extern PACKAGE bool __fastcall IsNilOrEmpty(const TDIContainer* AContainer);
extern PACKAGE void __fastcall FreeAndNilIfEmpty(void *AContainer);
extern PACKAGE TDIInitItemProc __fastcall OptimizedZeroItemProc(const unsigned ItemSize);
extern PACKAGE void __fastcall InitItem_00z01(const void * PItem);
extern PACKAGE void __fastcall InitItem_00z02(const void * PItem);
extern PACKAGE void __fastcall InitItem_00z04(const void * PItem);
extern PACKAGE void __fastcall InitItem_00z08(const void * PItem);
extern PACKAGE void __fastcall InitItem_00z12(const void * PItem);
extern PACKAGE void __fastcall InitItem_00z16(const void * PItem);
extern PACKAGE void __fastcall InitItem_00z20(const void * PItem);
extern PACKAGE void __fastcall InitItem_00z24(const void * PItem);
extern PACKAGE void __fastcall FreeItem_AnsiName(const void * PItem);
extern PACKAGE void __fastcall FreeItem_WideName(const void * PItem);
extern PACKAGE void __fastcall FreeItem_UnicodeName(const void * PItem);
extern PACKAGE void __fastcall FreeItem_AnsiValue(const void * PItem);
extern PACKAGE void __fastcall FreeItem_WideValue(const void * PItem);
extern PACKAGE void __fastcall FreeItem_UnicodeValue(const void * PItem);
extern PACKAGE void __fastcall FreeItem_AnsiName_04o04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_WideName_04o04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_UnicodeName_04o04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_AnsiName_04p04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_WideName_04p04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_UnicodeName_04p04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_AnsiName_AnsiValue(const void * PItem);
extern PACKAGE void __fastcall FreeItem_WideName_WideValue(const void * PItem);
extern PACKAGE void __fastcall FreeItem_UnicodeName_UnicodeValue(const void * PItem);
extern PACKAGE void __fastcall FreeItem_00o04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_04o04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_00p04(const void * PItem);
extern PACKAGE void __fastcall FreeItem_04p04(const void * PItem);
extern PACKAGE int __fastcall CompareItems_00s04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_00u04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_04s04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_04u04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_08s04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_08u04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiNameCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiNameCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_WideNameCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_WideNameCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_UnicodeNameCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_UnicodeNameCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_WideValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_UnicodeValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_WideValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_UnicodeValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_00s04_04s04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_00u04_04u04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_04s04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_04u04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_08s04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_08u04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_AnsiValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_WideValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_UnicodeValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_AnsiValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_WideValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItemElement_UnicodeValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiNameCS_AnsiValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiNameCI_AnsiValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiNameCS_AnsiValueCS_08u04(const void * PItem1, const void * PItem2);
extern PACKAGE int __fastcall CompareItems_AnsiNameCI_AnsiValueCI_08u04(const void * PItem1, const void * PItem2);
extern PACKAGE void __fastcall CopyItem_01(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_02(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_04(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_08(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_12(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_AnsiName(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_WideName(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_UnicodeName(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_AnsiName_04(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_WideName_04(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_UnicodeName_04(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_AnsiName_08(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_AnsiName_AnsiValue(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_WideName_WideValue(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_UnicodeName_UnicodeValue(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_AnsiName_AnsiValue_04(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_WideName_WideValue_04(const void * PSourceItem, const void * PDestItem);
extern PACKAGE void __fastcall CopyItem_UnicodeName_UnicodeValue_04(const void * PSourceItem, const void * PDestItem);
extern PACKAGE unsigned __fastcall HashKey_02(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_04(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_AnsiStringCS(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_WideStringCS(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_UnicodeStringCS(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_AnsiBufferCS(const void * PKey, const unsigned KeySize);
extern PACKAGE unsigned __fastcall HashKey_WideBufferCS(const void * PKey, const unsigned KeySize);
extern PACKAGE unsigned __fastcall HashKey_AnsiStringCI(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_WideStringCI(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_UnicodeStringCI(const void * PKey);
extern PACKAGE unsigned __fastcall HashKey_AnsiBufferCI(const void * PKey, const unsigned KeySize);
extern PACKAGE unsigned __fastcall HashKey_WideBufferCI(const void * PKey, const unsigned KeySize);
extern PACKAGE void __fastcall StoreKey_01(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_02(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_04(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_08(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_12(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_AnsiString(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_WideString(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_UnicodeString(const void * PSourceKey, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_AnsiBuffer(const void * PSourceKey, const unsigned SourceSize, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_WideBuffer(const void * PSourceKey, const unsigned SourceSize, const void * PDestKey);
extern PACKAGE void __fastcall StoreKey_UnicodeBuffer(const void * PSourceKey, const unsigned SourceSize, const void * PDestKey);
extern PACKAGE void __fastcall FreeKey_AnsiString(const void * PKey);
extern PACKAGE void __fastcall FreeKey_WideString(const void * PKey);
extern PACKAGE void __fastcall FreeKey_UnicodeString(const void * PKey);
extern PACKAGE bool __fastcall SameItems_00c04(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_04c04(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_00u04(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_00u08(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_04u04(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_08u04(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_AnsiNameCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_AnsiNameCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_WideNameCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_UnicodeNameCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemsEx_WideNameCS(const TDIContainer* Sender, const void * PItem1, const void * PItem2, const void * Extra);
extern PACKAGE bool __fastcall SameItems_WideNameCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_UnicodeNameCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemsEx_WideNameCI(const TDIContainer* Sender, const void * PItem1, const void * PItem2, const void * Extra);
extern PACKAGE bool __fastcall SameItems_WideValueCS(const TDIContainer* Sender, const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemsEx_WideValueCS(const TDIContainer* Sender, const void * PItem1, const void * PItem2, const void * Extra);
extern PACKAGE bool __fastcall SameItems_WideValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_AnsiNameCS_AnsiValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_WideNameCS_WideValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_UnicodeNameCS_UnicodeValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_AnsiNameCI_AnsiValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_WideNameCI_WideValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItems_UnicodeNameCI_UnicodeValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemsEx_WideNameCS_WideValueCS(const void * PItem1, const void * PItem2, const void * Extra);
extern PACKAGE bool __fastcall SameItemsEx_WideNameCI_WideValueCI(const void * PItem1, const void * PItem2, const void * Extra);
extern PACKAGE bool __fastcall SameItemElement_04u04(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemElement_08u04(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemElementEx_08u04(const TDIContainer* Sender, const void * PItem1, const void * PItem2, const void * Extra);
extern PACKAGE bool __fastcall SameItemElement_AnsiValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemElement_WideValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemElement_UnicodeValueCS(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemElement_AnsiValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemElement_WideValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameItemElement_UnicodeValueCI(const void * PItem1, const void * PItem2);
extern PACKAGE bool __fastcall SameKeys_02(const void * PKey1, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_04(const void * PKey1, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_AnsiStringCS(const void * PKey1, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_AnsiStringCI(const void * PKey1, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_AnsiBufferCS(const void * PKey1, const unsigned Key1Size, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_WideBufferCS(const void * PKey1, const unsigned Key1Size, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_UnicodeBufferCS(const void * PKey1, const unsigned Key1Size, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_AnsiBufferCI(const void * PKey1, const unsigned Key1Size, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_WideBufferCI(const void * PKey1, const unsigned Key1Size, const void * PKey2);
extern PACKAGE bool __fastcall SameKeys_UnicodeBufferCI(const void * PKey1, const unsigned Key1Size, const void * PKey2);
extern PACKAGE void __fastcall ReadHeader_ItemSize(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * Extra);
extern PACKAGE void __fastcall ReadItem(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_Null(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_AnsiString(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_WideString(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_UnicodeString(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_AnsiString_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_AnsiString_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_WideString_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_UnicodeString_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_AnsiString_Integer2(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_AnsiString2(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_AnsiString2_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_WideString2_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_UnicodeString2_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_Byte(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_Cardinal2(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_Integer2(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_WideString_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_UnicodeString_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_WideString2(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_UnicodeString2(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadItem_Word(const TDIContainer* Sender, const Distreams::TDIBinaryReader* Reader, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall ReadKey_AnsiString(const TDIHash* Sender, const Distreams::TDIBinaryReader* Reader, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall ReadKey_WideString(const TDIHash* Sender, const Distreams::TDIBinaryReader* Reader, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall ReadKey_UnicodeString(const TDIHash* Sender, const Distreams::TDIBinaryReader* Reader, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall ReadKey_Cardinal(const TDIHash* Sender, const Distreams::TDIBinaryReader* Reader, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall ReadKey_Word(const TDIHash* Sender, const Distreams::TDIBinaryReader* Reader, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall WriteHeader_ItemSize(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * Extra);
extern PACKAGE void __fastcall WriteItem(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_Null(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_AnsiString(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_WideString(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_UnicodeString(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_AnsiString_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_AnsiString_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_WideString_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_UnicodeString_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_AnsiString_Integer2(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_AnsiString2(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_WideString2(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_UnicodeString2(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_AnsiString2_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_WideString2_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_UnicodeString2_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_Byte(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_Cardinal2(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_Integer(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_Integer2(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_WideString_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_UnicodeString_Cardinal(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteItem_Word(const TDIContainer* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PItem, const void * Extra);
extern PACKAGE void __fastcall WriteKey_AnsiString(const TDIHash* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall WriteKey_WideString(const TDIHash* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall WriteKey_UnicodeString(const TDIHash* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall WriteKey_Cardinal(const TDIHash* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PKey, const void * Extra);
extern PACKAGE void __fastcall WriteKey_Word(const TDIHash* Sender, const Distreams::TDIBinaryWriter* Writer, const void * PKey, const void * Extra);

}	/* namespace Dicontainers */
using namespace Dicontainers;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DicontainersHPP
