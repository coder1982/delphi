// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diunicodestring2vector.pas' rev: 20.00

#ifndef Diunicodestring2vectorHPP
#define Diunicodestring2vectorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit
#include <Diunicodestringvector.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diunicodestring2vector
{
//-- type declarations -------------------------------------------------------
typedef System::UnicodeString *PDIValueType;

typedef System::UnicodeString TDIValueType;

typedef Ditypes::TUnicodeString2 *PDIItemType;

typedef Ditypes::TUnicodeString2 TDIItemType;

class DELPHICLASS TDIUnicodeString2Vector;
class PASCALIMPLEMENTATION TDIUnicodeString2Vector : public Diunicodestringvector::TDIUnicodeStringVector
{
	typedef Diunicodestringvector::TDIUnicodeStringVector inherited;
	
protected:
	unsigned __fastcall GetMaxValueLength(void);
	void __fastcall SetMaxValueLength(const unsigned NewLength);
	System::UnicodeString __fastcall GetFirstValue();
	void __fastcall SetFirstValue(const System::UnicodeString AValue);
	System::UnicodeString __fastcall GetLastValue();
	void __fastcall SetLastValue(const System::UnicodeString Value);
	System::UnicodeString __fastcall GetValue(void * PItem);
	System::UnicodeString __fastcall GetValueAt(const int Index);
	System::UnicodeString __fastcall GetValueOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems);
	void __fastcall SetValueOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems, const System::UnicodeString Value);
	System::UnicodeString __fastcall GetNameOfValueCS(const System::UnicodeString AValue);
	void __fastcall SetNameOfValueCS(const System::UnicodeString AValue, const System::UnicodeString AName);
	System::UnicodeString __fastcall GetNameOfValueCI(const System::UnicodeString AValue);
	void __fastcall SetNameOfValueCI(const System::UnicodeString AValue, const System::UnicodeString AName);
	System::UnicodeString __fastcall GetNameOfValueCSBack(const System::UnicodeString AValue);
	void __fastcall SetNameOfValueCSBack(const System::UnicodeString AValue, const System::UnicodeString AName);
	System::UnicodeString __fastcall GetNameOfValueCIBack(const System::UnicodeString AValue);
	void __fastcall SetNameOfValueCIBack(const System::UnicodeString AValue, const System::UnicodeString AName);
	System::UnicodeString __fastcall GetValueOfNameCS(const System::UnicodeString AName);
	void __fastcall SetValueOfNameCS(const System::UnicodeString AName, const System::UnicodeString AValue);
	System::UnicodeString __fastcall GetValueOfNameCI(const System::UnicodeString AName);
	void __fastcall SetValueOfNameCI(const System::UnicodeString AName, const System::UnicodeString AValue);
	System::UnicodeString __fastcall GetValueOfNameCSBack(const System::UnicodeString AName);
	void __fastcall SetValueOfNameCSBack(const System::UnicodeString AName, const System::UnicodeString AValue);
	System::UnicodeString __fastcall GetValueOfNameCIBack(const System::UnicodeString AName);
	void __fastcall SetValueOfNameCIBack(const System::UnicodeString AName, const System::UnicodeString AValue);
	void __fastcall SetValue(void * PItem, const System::UnicodeString Value);
	void __fastcall SetValueAt(const int Index, const System::UnicodeString Value);
	
public:
	__property System::UnicodeString Value[void * PItem] = {read=GetValue, write=SetValue};
	__property System::UnicodeString ValueAt[const int Index] = {read=GetValueAt, write=SetValueAt};
	__property System::UnicodeString ValueOf[void * PItem][const Dicontainers::TDISameItemsFunc SameItems] = {read=GetValueOf, write=SetValueOf};
	__property System::UnicodeString FirstValue = {read=GetFirstValue, write=SetFirstValue};
	__property System::UnicodeString LastValue = {read=GetLastValue, write=SetLastValue};
	int __fastcall InsertValueSortedCS(const System::UnicodeString AValue);
	int __fastcall InsertValueSortedCI(const System::UnicodeString Value);
	int __fastcall InsertValueSortedCSDesc(const System::UnicodeString AValue);
	int __fastcall InsertValueSortedCIDesc(const System::UnicodeString AValue);
	bool __fastcall ExistsValueCS(const System::UnicodeString AValue);
	bool __fastcall ExistsValueCI(const System::UnicodeString AValue);
	bool __fastcall ExistsNameCSValueCS(const System::UnicodeString AName, const System::UnicodeString AValue, const int StartIndex = 0x0);
	bool __fastcall ExistsNameCIValueCI(const System::UnicodeString AName, const System::UnicodeString AValue, const int StartIndex = 0x0);
	bool __fastcall ExistsValueCSBack(const System::UnicodeString AValue);
	bool __fastcall ExistsValueCIBack(const System::UnicodeString AValue);
	bool __fastcall ExistsValueCSSorted(const System::UnicodeString Value);
	bool __fastcall ExistsValueCISorted(const System::UnicodeString Value);
	bool __fastcall ExistAllValuesCS(System::UnicodeString const *AValues, const int AValues_Size);
	bool __fastcall ExistAllValuesCI(System::UnicodeString const *AValues, const int AValues_Size);
	bool __fastcall ExistsAnyValueCS(System::UnicodeString const *AValues, const int AValues_Size);
	bool __fastcall ExistsAnyValueCI(System::UnicodeString const *AValues, const int AValues_Size);
	bool __fastcall FindValueCS(const System::UnicodeString Value, /* out */ int &Index);
	bool __fastcall FindValueCI(const System::UnicodeString Value, /* out */ int &Index);
	bool __fastcall FindValueCSDesc(const System::UnicodeString AValue, /* out */ int &Index);
	bool __fastcall FindValueCIDesc(const System::UnicodeString AValue, /* out */ int &Index);
	int __fastcall IndexOfValueCS(const System::UnicodeString AValue);
	int __fastcall IndexOfValueCI(const System::UnicodeString AValue);
	int __fastcall IndexOfValueCSBack(const System::UnicodeString AValue);
	int __fastcall IndexOfValueCIBack(const System::UnicodeString AValue);
	void * __fastcall InsertValueAt(const int Index, const System::UnicodeString AValue);
	void * __fastcall InsertValueFirst(const System::UnicodeString AValue);
	void * __fastcall InsertValueLast(const System::UnicodeString AValue);
	void __fastcall InsertValuesLast(System::UnicodeString const *AValues, const int AValues_Size);
	void * __fastcall InsertNameValueFirst(const System::UnicodeString AName, const System::UnicodeString AValue);
	void * __fastcall insertnamevaluelast(const System::UnicodeString AName, const System::UnicodeString AValue);
	__property unsigned MaxValueLength = {read=GetMaxValueLength, write=SetMaxValueLength, nodefault};
	void __fastcall RemoveAllValuesCS(const System::UnicodeString AValue);
	void __fastcall RemoveAllValuesCI(const System::UnicodeString AValue);
	void __fastcall RemoveAllValuesCSBack(const System::UnicodeString AValue, const int StartIndex = 0xffffffff);
	void __fastcall RemoveAllValuesCIBack(const System::UnicodeString AValue, const int StartIndex = 0xffffffff);
	void * __fastcall PItemOfValueCS(const System::UnicodeString Value, const int StartIndex = 0x0);
	void * __fastcall PItemOfValueCI(const System::UnicodeString Value, const int StartIndex = 0x0);
	void * __fastcall PItemOfValueCSBack(const System::UnicodeString Value);
	void * __fastcall PItemOfValueCIBack(const System::UnicodeString Value);
	void __fastcall SortByValueCS(void);
	void __fastcall SortByValueCI(void);
	void __fastcall SortByValueCSDesc(void);
	void __fastcall SortByValueCIDesc(void);
	__property System::UnicodeString NameOfValueCS[const System::UnicodeString Value] = {read=GetNameOfValueCS, write=SetNameOfValueCS};
	__property System::UnicodeString NameOfValueCSBack[const System::UnicodeString Value] = {read=GetNameOfValueCSBack, write=SetNameOfValueCSBack};
	__property System::UnicodeString NameOfValueCI[const System::UnicodeString Value] = {read=GetNameOfValueCI, write=SetNameOfValueCI};
	__property System::UnicodeString NameOfValueCIBack[const System::UnicodeString Value] = {read=GetNameOfValueCIBack, write=SetNameOfValueCIBack};
	__property System::UnicodeString ValueOfNameCS[const System::UnicodeString Name] = {read=GetValueOfNameCS, write=SetValueOfNameCS};
	__property System::UnicodeString ValueOfNameCSBack[const System::UnicodeString Name] = {read=GetValueOfNameCSBack, write=SetValueOfNameCSBack};
	__property System::UnicodeString ValueOfNameCI[const System::UnicodeString Name] = {read=GetValueOfNameCI, write=SetValueOfNameCI};
	__property System::UnicodeString ValueOfNameCIBack[const System::UnicodeString Name] = {read=GetValueOfNameCIBack, write=SetValueOfNameCIBack};
	void __fastcall SaveValuesToTextFile(const System::UnicodeString FileName);
	void __fastcall SaveValuesToTextStream(const Classes::TStream* Stream);
	void __fastcall ValuesToUpperCase(void);
	void __fastcall ValuesToLowerCase(void);
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDIUnicodeString2Vector(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDIUnicodeString2Vector(const Dicontainers::TDIItemHandler* AItemHandler) : Diunicodestringvector::TDIUnicodeStringVector(AItemHandler) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_VALUE_CS;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_VALUE_CI;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_VALUE_CS;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_VALUE_CI;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_VALUE_CS;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_VALUE_CI;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENTS_NAME_CS_VALUE_CS;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENTS_NAME_CI_VALUE_CI;
extern PACKAGE TDIUnicodeString2Vector* __fastcall NewDIUnicodeString2Vector(void);

}	/* namespace Diunicodestring2vector */
using namespace Diunicodestring2vector;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Diunicodestring2vectorHPP
