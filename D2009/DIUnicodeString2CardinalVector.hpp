// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diunicodestring2cardinalvector.pas' rev: 20.00

#ifndef Diunicodestring2cardinalvectorHPP
#define Diunicodestring2cardinalvectorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit
#include <Diunicodestringvector.hpp>	// Pascal unit
#include <Diunicodestring2vector.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diunicodestring2cardinalvector
{
//-- type declarations -------------------------------------------------------
typedef unsigned *PDINumberType;

typedef unsigned TDINumberType;

typedef Ditypes::TUnicodeString2Cardinal *PDIItemType;

typedef Ditypes::TUnicodeString2Cardinal TDIItemType;

class DELPHICLASS TDIUnicodeString2CardinalVector;
class PASCALIMPLEMENTATION TDIUnicodeString2CardinalVector : public Diunicodestring2vector::TDIUnicodeString2Vector
{
	typedef Diunicodestring2vector::TDIUnicodeString2Vector inherited;
	
protected:
	unsigned __fastcall GetFirstNumber(void);
	void __fastcall SetFirstNumber(const unsigned ANumber);
	unsigned __fastcall GetLastNumber(void);
	void __fastcall SetLastNumber(const unsigned Number);
	unsigned __fastcall GetNumber(void * PItem);
	unsigned __fastcall GetNumberAt(const int Index);
	unsigned __fastcall GetNumberOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems);
	void __fastcall SetNumberOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems, const unsigned Number);
	System::UnicodeString __fastcall GetNameOfNumber(const unsigned ANumber);
	void __fastcall SetNameOfNumber(const unsigned ANumber, const System::UnicodeString AName);
	System::UnicodeString __fastcall GetNameOfNumberBack(const unsigned ANumber);
	void __fastcall SetNameOfNumberBack(const unsigned ANumber, const System::UnicodeString AName);
	System::UnicodeString __fastcall GetValueOfNumber(const unsigned ANumber);
	System::UnicodeString __fastcall GetValueOfNumberEx(const unsigned ANumber);
	void __fastcall SetValueOfNumber(const unsigned ANumber, const System::UnicodeString AValue);
	void __fastcall SetValueOfNumberEx(const unsigned ANumber, const System::UnicodeString AValue);
	System::UnicodeString __fastcall GetValueOfNumberBack(const unsigned ANumber);
	void __fastcall SetValueOfNumberBack(const unsigned ANumber, const System::UnicodeString AValue);
	void __fastcall SetNumber(void * PItem, const unsigned Number);
	void __fastcall SetNumberAt(const int Index, const unsigned Number);
	
public:
	System::Extended __fastcall ArithmeticMeanOfNumber(void);
	System::Extended __fastcall SumOfNumber(void);
	__property unsigned Number[void * PItem] = {read=GetNumber, write=SetNumber};
	__property unsigned NumberAt[const int Index] = {read=GetNumberAt, write=SetNumberAt};
	__property unsigned NumberOf[void * PItem][const Dicontainers::TDISameItemsFunc SameItems] = {read=GetNumberOf, write=SetNumberOf};
	unsigned __fastcall MaxNumber(void);
	unsigned __fastcall MinNumber(void);
	__property unsigned FirstNumber = {read=GetFirstNumber, write=SetFirstNumber, nodefault};
	__property unsigned LastNumber = {read=GetLastNumber, write=SetLastNumber, nodefault};
	int __fastcall InsertNameNumberSortedByNameCS(const System::UnicodeString AName, const unsigned ANumber);
	int __fastcall InsertNameNumberSortedByNameCI(const System::UnicodeString AName, const unsigned ANumber);
	void * __fastcall InsertNameValueNumberLast(const System::UnicodeString AName, const System::UnicodeString AValue, const unsigned ANumber);
	int __fastcall InsertNumberSorted(const unsigned ANumber);
	int __fastcall InsertNumberSortedDesc(const unsigned ANumber);
	bool __fastcall ExistsNumber(const unsigned ANumber);
	bool __fastcall ExistsNumberBack(const unsigned ANumber);
	bool __fastcall ExistsNumberSorted(const unsigned Number);
	bool __fastcall ExistAllNumbers(unsigned const *ANumbers, const int ANumbers_Size);
	bool __fastcall ExistsAnyNumber(unsigned const *ANumbers, const int ANumbers_Size);
	bool __fastcall FindNumber(const unsigned Number, /* out */ int &Index);
	bool __fastcall FindNumberDesc(const unsigned ANumber, /* out */ int &Index);
	int __fastcall IndexOfNumber(const unsigned ANumber);
	int __fastcall IndexOfNumberBack(const unsigned ANumber);
	void * __fastcall InsertNumberAt(const int Index, const unsigned ANumber);
	void * __fastcall InsertNumberFirst(const unsigned ANumber);
	void * __fastcall InsertNumberLast(const unsigned ANumber);
	void __fastcall InsertNumbersLast(unsigned const *ANumbers, const int ANumbers_Size);
	void * __fastcall InsertNameNumberFirst(const System::UnicodeString AName, const unsigned ANumber);
	void * __fastcall InsertNameNumberLast(const System::UnicodeString AName, const unsigned ANumber);
	void __fastcall RemoveAllNumbers(const unsigned ANumber);
	void __fastcall RemoveAllNumbersBack(const unsigned ANumber, const int StartIndex = 0xffffffff);
	void * __fastcall PItemOfNumber(const unsigned Number)/* overload */;
	void * __fastcall PItemOfNumber(const unsigned Number, const int StartIndex)/* overload */;
	void * __fastcall PItemOfNumberBack(const unsigned Number);
	void __fastcall SortByNumber(void);
	void __fastcall SortByNumberDesc(void);
	__property System::UnicodeString NameOfNumber[const unsigned Number] = {read=GetNameOfNumber, write=SetNameOfNumber};
	__property System::UnicodeString NameOfNumberBack[const unsigned Number] = {read=GetNameOfNumberBack, write=SetNameOfNumberBack};
	__property System::UnicodeString ValueOfNumber[const unsigned Number] = {read=GetValueOfNumber, write=SetValueOfNumber};
	__property System::UnicodeString ValueOfNumberEx[const unsigned Number] = {read=GetValueOfNumberEx, write=SetValueOfNumberEx};
	__property System::UnicodeString ValueOfNumberBack[const unsigned Number] = {read=GetValueOfNumberBack, write=SetValueOfNumberBack};
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDIUnicodeString2CardinalVector(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDIUnicodeString2CardinalVector(const Dicontainers::TDIItemHandler* AItemHandler) : Diunicodestring2vector::TDIUnicodeString2Vector(AItemHandler) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_NUMBER;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_NUMBER;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_NUMBER;
extern PACKAGE TDIUnicodeString2CardinalVector* __fastcall NewDIUnicodeString2CardinalVector(void);

}	/* namespace Diunicodestring2cardinalvector */
using namespace Diunicodestring2cardinalvector;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Diunicodestring2cardinalvectorHPP
