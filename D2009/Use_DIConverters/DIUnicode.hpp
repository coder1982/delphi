// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diunicode.pas' rev: 20.00

#ifndef DiunicodeHPP
#define DiunicodeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Diconverters.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Diutils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diunicode
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TDIBOM { bomUnknown, bomUtf8, bomutf16be, bomutf16le, bomutf32be, bomutf32le };
#pragma option pop

class DELPHICLASS TDIUnicodeBase;
class PASCALIMPLEMENTATION TDIUnicodeBase : public Classes::TComponent
{
	typedef Classes::TComponent inherited;
	
protected:
	__classmethod void __fastcall Error(const System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size);
public:
	/* TComponent.Create */ inline __fastcall virtual TDIUnicodeBase(Classes::TComponent* AOwner) : Classes::TComponent(AOwner) { }
	/* TComponent.Destroy */ inline __fastcall virtual ~TDIUnicodeBase(void) { }
	
};


struct TDIUnicodeReadMethods
{
	
public:
	Diconverters::xxx_mbtowc mbtowc;
	Diconverters::xxx_flushwc flushwc;
};


typedef TDIUnicodeReadMethods *PDIUnicodeReadMethods;

struct TUnicodeReaderSource;
typedef TUnicodeReaderSource *PUnicodeReaderSource;

struct TUnicodeReaderSource
{
	
public:
	TUnicodeReaderSource *Next;
	unsigned CharPos;
	unsigned CharLine;
	unsigned CharCol;
	TDIUnicodeReadMethods ReadMethods;
	void *SourceBuffer;
	unsigned SourceBufferSize;
	unsigned SourceBufferPosition;
	System::UnicodeString SourceFile;
	Classes::TStream* SourceStream;
	void *UserData;
};


#pragma option push -b-
enum TAutoFreeSourceStreams { afNone, afAllButFirst, afAll };
#pragma option pop

class DELPHICLASS TDIUnicodeReader;
class PASCALIMPLEMENTATION TDIUnicodeReader : public TDIUnicodeBase
{
	typedef TDIUnicodeBase inherited;
	
private:
	bool FCharOK;
	System::WideChar FChar;
	System::WideChar FLastChar;
	unsigned FCharPos;
	unsigned FCharLine;
	unsigned FCharCol;
	void *FSourceBuffer;
	unsigned FSourceBufferSize;
	unsigned FSourceBufferPosition;
	System::WideChar *FPeekedChars;
	unsigned FPeekedCount;
	unsigned FPeekedCapacity;
	System::UnicodeString FSourceFile;
	TUnicodeReaderSource *FSourceStack;
	TDIUnicodeReadMethods FReadMethods;
	Diconverters::conv_struct FState;
	Classes::TStream* FSourceStream;
	TAutoFreeSourceStreams FAutoFreeSourceStreams;
	void __fastcall AdjustDataCapacity(const unsigned MinCapacity);
	void __fastcall SetDataSize(const unsigned NewDataSize);
	System::UnicodeString __fastcall GetSourceBufferAsStr();
	System::RawByteString __fastcall GetSourceBufferAsStrA();
	System::UnicodeString __fastcall GetSourceBufAsStrW();
	void __fastcall SetSourceBufferAsStrProp(const System::UnicodeString ASourceStr);
	void __fastcall SetSourceBufferAsStrAProp(const System::RawByteString ASourceStr);
	void __fastcall SetSourceBufferAsStrWProp(const System::UnicodeString ASourceStr);
	void __fastcall SetSourceBufferPosition(const unsigned NewPosition);
	void __fastcall SetSourceBufferSize(const unsigned NewSize);
	void __fastcall SetSourceFile(const System::UnicodeString AFileName);
	void __fastcall SetSourceStreamProp(const Classes::TStream* AStream);
	bool __fastcall ReadOneChar(/* out */ System::WideChar &c);
	void __fastcall SetReadMethods(const TDIUnicodeReadMethods &Value);
	
protected:
	System::WideChar *FData;
	unsigned FDataSize;
	unsigned FDataCapacity;
	__property TAutoFreeSourceStreams AutoFreeSourceStreams = {read=FAutoFreeSourceStreams, write=FAutoFreeSourceStreams, default=0};
	__property PUnicodeReaderSource SourceStack = {read=FSourceStack};
	void __fastcall TrimmedDataStartAndLength(Diutils::TDIValidateWideCharFunc Validate, /* out */ System::WideChar * &Start, /* out */ unsigned &Length);
	void __fastcall TrimmedLeftDataStartAndLength(Diutils::TDIValidateWideCharFunc Validate, /* out */ System::WideChar * &Start, /* out */ unsigned &Length);
	unsigned __fastcall TrimmedRightDataLength(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	
public:
	__fastcall virtual TDIUnicodeReader(Classes::TComponent* AOwner);
	__fastcall virtual ~TDIUnicodeReader(void);
	void __fastcall AddChar(void)/* overload */;
	void __fastcall AddChar(const System::WideChar c)/* overload */;
	void __fastcall AddCharCompact(const System::WideChar c);
	void __fastcall AddCrLf(void);
	void __fastcall AddSpace(void);
	void __fastcall AddSpaceCompact(void);
	void __fastcall AddStrW(const System::UnicodeString s);
	void __fastcall AddStrCompactW(const System::UnicodeString Value);
	bool __fastcall AddCharReadChar(void);
	__property System::WideChar Char = {read=FChar, nodefault};
	__property bool CharOK = {read=FCharOK, write=FCharOK, nodefault};
	__property unsigned CharPos = {read=FCharPos, nodefault};
	__property unsigned CharLine = {read=FCharLine, nodefault};
	__property unsigned CharCol = {read=FCharCol, nodefault};
	void __fastcall ClearData(void);
	void __fastcall ClearPeekedChars(void);
	void __fastcall ClearSourceStack(void);
	__property System::WideChar * Data = {read=FData};
	__property System::WideChar * PeekedChars = {read=FPeekedChars};
	__property void * SourceBuffer = {read=FSourceBuffer};
	__property TDIUnicodeReadMethods ReadMethods = {read=FReadMethods, write=SetReadMethods};
	System::UnicodeString __fastcall DataAsStr();
	System::AnsiString __fastcall DataAsStrA(const unsigned CodePage = (unsigned)(0x0));
	System::UnicodeString __fastcall DataAsStrW();
	System::UTF8String __fastcall DataAsStr8();
	System::AnsiString __fastcall DataAsStrTrimA(const unsigned CodePage = (unsigned)(0x0), const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	System::AnsiString __fastcall DataToStrA();
	System::AnsiString __fastcall DataToStrTrimA(const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	System::UnicodeString __fastcall DataAsStrTrimW(const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	System::UnicodeString __fastcall DataAsStrTrimLeftW(const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	bool __fastcall DataHasChars(const Diutils::TDIValidateWideCharFunc Validate);
	bool __fastcall DataIsEmpty(void);
	bool __fastcall DataIsChars(const Diutils::TDIValidateWideCharFunc Validate);
	bool __fastcall DataIsNotEmpty(void);
	bool __fastcall DataIsStrW(const System::UnicodeString s);
	bool __fastcall DataIsStrIW(const System::UnicodeString s);
	__property unsigned DataSize = {read=FDataSize, write=SetDataSize, nodefault};
	unsigned __fastcall FillSourceBuffer(const bool MoveEndToFront = false);
	System::UnicodeString __fastcall FirstSourceFile();
	void __fastcall FreeSourceStream(void);
	void __fastcall IncSourceBufferPosition(void)/* overload */;
	void __fastcall IncSourceBufferPosition(const unsigned n)/* overload */;
	bool __fastcall PeekChars(const unsigned Count = (unsigned)(0x1));
	__property unsigned PeekedCount = {read=FPeekedCount, nodefault};
	void __fastcall PushSource(const bool StoreReadMethods);
	bool __fastcall PopSource(void);
	TDIBOM __fastcall ReadBOM(void);
	bool __fastcall ReadChar(void);
	void __fastcall ReadChars(const System::WideChar c)/* overload */;
	void __fastcall ReadChars(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall ReadChars(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall ReadCharsTill(const System::WideChar c)/* overload */;
	void __fastcall ReadCharsTill(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall ReadCharsTill(const System::WideChar c, const System::WideChar c2, const System::WideChar c3)/* overload */;
	void __fastcall ReadCharsTill(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall ReadCharsTillWhiteSpace(void);
	void __fastcall ReadDigits(void);
	void __fastcall SkipDigits(void);
	void __fastcall ReadHexDigits(void);
	void __fastcall SkipHexDigits(void);
	bool __fastcall ReadLine(void);
	bool __fastcall ReadTillEndOfLine(void);
	void __fastcall ReadWhiteSpace(void);
	void __fastcall ResetCharLocation(void);
	void __fastcall SetSourceBufferAsStr(const System::UnicodeString ASourceStr, const System::UnicodeString AFileName = L"");
	void __fastcall SetSourceBufferAsStrA(const System::RawByteString ASourceStr, const System::UnicodeString AFileName = L"");
	void __fastcall SetSourceBufferAsStrW(const System::UnicodeString ASourceStr, const System::UnicodeString AFileName = L"");
	void __fastcall SetSourceStream(const Classes::TStream* AStream, const System::UnicodeString AFileName = L"");
	void __fastcall SkipChars(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall SkipChars(const System::WideChar c)/* overload */;
	void __fastcall SkipChars(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall SkipCharsTill(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall SkipCharsTill(const System::WideChar c)/* overload */;
	void __fastcall SkipCharsTill(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall SkipCharsTill(const System::WideChar c, const System::WideChar c2, const System::WideChar c3)/* overload */;
	bool __fastcall SkipEmptyLines(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	bool __fastcall SkipLine(void);
	bool __fastcall SkipTillEndOfLine(void);
	void __fastcall SkipWhiteSpace(void);
	void __fastcall RewindSource(void);
	void __fastcall SetSourceBuffer(const void *Buffer, const unsigned BufferSize, const System::UnicodeString AFileName = L"");
	void __fastcall SaveDataToFile(const System::UnicodeString FileName, const bool WriteByteOrderMark = false);
	void __fastcall SaveDataToStream(const Classes::TStream* Stream, const bool WriteByteOrderMark = false);
	__property System::UnicodeString SourceBufferAsStr = {read=GetSourceBufferAsStr, write=SetSourceBufferAsStrProp};
	__property System::RawByteString SourceBufferAsStrA = {read=GetSourceBufferAsStrA, write=SetSourceBufferAsStrAProp};
	void __fastcall SourceStreamModified(void);
	unsigned __fastcall TrimmedDataLength(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	__property System::UnicodeString SourceBufferAsStrW = {read=GetSourceBufAsStrW, write=SetSourceBufferAsStrWProp};
	__property unsigned SourceBufferSize = {read=FSourceBufferSize, write=SetSourceBufferSize, nodefault};
	__property unsigned SourceBufferPosition = {read=FSourceBufferPosition, write=SetSourceBufferPosition, nodefault};
	__property System::UnicodeString SourceFile = {read=FSourceFile, write=SetSourceFile};
	__property Classes::TStream* SourceStream = {read=FSourceStream, write=SetSourceStreamProp};
	void __fastcall TrimDataRight(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	void __fastcall ZeroDataSize(void);
};


struct TDIUnicodeWriteMethods
{
	
public:
	Diconverters::xxx_wctomb wctomb;
	Diconverters::xxx_reset Reset;
};


typedef TDIUnicodeWriteMethods *PDIUnicodeWriteMethods;

#pragma option push -b-
enum TDIInvalidCharAction { icWrite, icOwnerWrite, icAbort };
#pragma option pop

class DELPHICLASS TDIUnicodeWriter;
typedef void __fastcall (__closure *TDIInvalidCharEvent)(const TDIUnicodeWriter* Sender, const System::WideChar InvalidChar, System::WideChar &ReplacementChar, const unsigned Attempt, TDIInvalidCharAction &Action);

class PASCALIMPLEMENTATION TDIUnicodeWriter : public TDIUnicodeBase
{
	typedef TDIUnicodeBase inherited;
	
private:
	void *FData;
	TDIUnicodeWriteMethods FWriteMethods;
	Diconverters::conv_struct FState;
	unsigned FDataCapacity;
	unsigned FDataSize;
	Classes::TStream* FDataStream;
	System::WideChar FReplacementChar;
	TDIInvalidCharEvent FOnInvalidChar;
	void __fastcall SetWriteMethods(const TDIUnicodeWriteMethods &Value);
	void __fastcall SetDataStream(const Classes::TStream* AStream);
	
protected:
	void __fastcall RequestData(void);
	void __fastcall SetDataSize(const unsigned NewDataSize);
	bool __fastcall WriteInvalidChar(const System::WideChar InvalidChar);
	bool __fastcall WriteOneChar(const System::WideChar c);
	void __fastcall Flush(void);
	
public:
	__fastcall virtual TDIUnicodeWriter(Classes::TComponent* AOwner);
	__fastcall virtual ~TDIUnicodeWriter(void);
	__property void * Data = {read=FData};
	__property unsigned DataSize = {read=FDataSize, write=SetDataSize, nodefault};
	__property unsigned DataCapacity = {read=FDataCapacity, nodefault};
	__property Classes::TStream* DataStream = {read=FDataStream, write=SetDataStream};
	System::UnicodeString __fastcall DataToStr();
	System::AnsiString __fastcall DataToStrA();
	System::RawByteString __fastcall DataToStrRaw();
	System::UnicodeString __fastcall DataToStrW();
	void __fastcall Clear(void);
	void __fastcall FlushDataBuffer(void);
	void __fastcall FreeDataStream(void);
	void __fastcall IncDataSize(void);
	void __fastcall SaveDataToFile(const System::UnicodeString FileName);
	void __fastcall SaveDataToStream(const Classes::TStream* Stream);
	void __fastcall WriteByte(const System::Byte b);
	bool __fastcall WriteDecimalCodePoint(const System::WideChar c);
	bool __fastcall WriteDecimalNumber(unsigned Number, unsigned StartDiv)/* overload */;
	bool __fastcall WriteDecimalNumber(unsigned Number)/* overload */;
	bool __fastcall WriteDecimalNumber(int Number)/* overload */;
	bool __fastcall WriteDecimalNumber(__int64 Number)/* overload */;
	bool __fastcall WriteFile(const System::UnicodeString AFileName, const TDIUnicodeReadMethods &AReadMethods);
	bool __fastcall WriteHexCodePoint(const System::WideChar c);
	void __fastcall WriteLineBreak(void);
	void __fastcall WriteBufA(const char * Buffer, const unsigned CharCount, const unsigned CodePage = (unsigned)(0x0));
	void __fastcall WriteStrA(const System::AnsiString s, const unsigned CodePage = (unsigned)(0x0));
	bool __fastcall WriteStream(const Classes::TStream* AStream, const TDIUnicodeReadMethods &AReadMethods);
	void __fastcall WriteStrLineBreakA(const System::AnsiString s, const unsigned CodePage = (unsigned)(0x0));
	void __fastcall WriteBufDirect(const void *Buffer, const unsigned Size);
	bool __fastcall WriteBufW(System::WideChar * Buffer, unsigned CharCount);
	bool __fastcall WriteCharW(const System::WideChar c);
	bool __fastcall WriteCharsW(const System::WideChar c, const unsigned Count);
	void __fastcall WriteStrRaw(const System::AnsiString s);
	bool __fastcall WriteStrW(const System::UnicodeString s);
	void __fastcall WriteStrLineBreakW(const System::UnicodeString s);
	bool __fastcall TryWriteCharW(const System::WideChar c);
	__property TDIUnicodeWriteMethods WriteMethods = {read=FWriteMethods, write=SetWriteMethods};
	
__published:
	__property System::WideChar ReplacementChar = {read=FReplacementChar, write=FReplacementChar, default=63};
	__property TDIInvalidCharEvent OnInvalidChar = {read=FOnInvalidChar, write=FOnInvalidChar};
};


//-- var, const, procedure ---------------------------------------------------
static const WideChar DEFAULT_REPLACEMENT_CHAR = (WideChar)(0x3f);
extern PACKAGE TDIUnicodeReadMethods Read_us_ascii;
extern PACKAGE TDIUnicodeWriteMethods Write_us_ascii;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_8;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_8;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_2;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_2;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_2_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_2_BE;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_2_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_2_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_4;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_4;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_4_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_4_BE;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_4_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_4_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_16;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_16;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_16_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_16_BE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_16_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_16_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_32;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_32;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_32_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_32_BE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_32_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_32_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_7;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_7;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_2_Internal;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_2_Internal;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_2_Swapped;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_2_Swapped;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_4_Internal;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_4_Internal;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_4_Swapped;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_4_Swapped;
extern PACKAGE TDIUnicodeReadMethods Read_C99;
extern PACKAGE TDIUnicodeWriteMethods Write_C99;
extern PACKAGE TDIUnicodeReadMethods Read_Java;
extern PACKAGE TDIUnicodeWriteMethods Write_Java;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_1;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_1;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_2;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_2;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_3;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_3;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_4;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_4;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_5;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_5;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_6;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_6;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_7;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_7;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_8;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_8;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_9;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_9;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_10;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_10;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_11;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_11;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_13;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_13;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_14;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_14;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_15;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_15;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_16;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_16;
extern PACKAGE TDIUnicodeReadMethods Read_KOI8_R;
extern PACKAGE TDIUnicodeWriteMethods Write_KOI8_R;
extern PACKAGE TDIUnicodeReadMethods Read_KOI8_U;
extern PACKAGE TDIUnicodeWriteMethods Write_KOI8_U;
extern PACKAGE TDIUnicodeReadMethods Read_KOI8_RU;
extern PACKAGE TDIUnicodeWriteMethods Write_KOI8_RU;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1250;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1250;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1251;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1251;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1252;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1252;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1253;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1253;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1254;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1254;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1255;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1255;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1256;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1256;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1257;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1257;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1258;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1258;
extern PACKAGE TDIUnicodeReadMethods Read_CP_850;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_850;
extern PACKAGE TDIUnicodeReadMethods Read_CP_862;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_862;
extern PACKAGE TDIUnicodeReadMethods Read_CP_866;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_866;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Roman;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Roman;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_CentralEurope;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_CentralEurope;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Iceland;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Iceland;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Croatian;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Croatian;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Romania;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Romania;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Cyrillic;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Cyrillic;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Ukraine;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Ukraine;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Greek;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Greek;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Turkish;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Turkish;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Hebrew;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Hebrew;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Arabic;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Arabic;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Thai;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Thai;
extern PACKAGE TDIUnicodeReadMethods Read_HP_Roman_8;
extern PACKAGE TDIUnicodeWriteMethods Write_HP_Roman_8;
extern PACKAGE TDIUnicodeReadMethods Read_NextStep;
extern PACKAGE TDIUnicodeWriteMethods Write_NextStep;
extern PACKAGE TDIUnicodeReadMethods Read_ARMSCII_8;
extern PACKAGE TDIUnicodeWriteMethods Write_ARMSCII_8;
extern PACKAGE TDIUnicodeReadMethods Read_Georgian_Academy;
extern PACKAGE TDIUnicodeWriteMethods Write_Georgian_Academy;
extern PACKAGE TDIUnicodeReadMethods Read_Georgian_PS;
extern PACKAGE TDIUnicodeWriteMethods Write_Georgian_PS;
extern PACKAGE TDIUnicodeReadMethods Read_KOI8_T;
extern PACKAGE TDIUnicodeWriteMethods Write_KOI8_T;
extern PACKAGE TDIUnicodeReadMethods Read_PT_154;
extern PACKAGE TDIUnicodeWriteMethods Write_PT_154;
extern PACKAGE TDIUnicodeReadMethods Read_Mulelao_1;
extern PACKAGE TDIUnicodeWriteMethods Write_Mulelao_1;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1133;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1133;
extern PACKAGE TDIUnicodeReadMethods Read_TIS_620;
extern PACKAGE TDIUnicodeWriteMethods Write_TIS_620;
extern PACKAGE TDIUnicodeReadMethods Read_CP_874;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_874;
extern PACKAGE TDIUnicodeReadMethods Read_viscii;
extern PACKAGE TDIUnicodeWriteMethods Write_viscii;
extern PACKAGE TDIUnicodeReadMethods Read_tcvn;
extern PACKAGE TDIUnicodeWriteMethods Write_tcvn;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_646_JP;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_646_JP;
extern PACKAGE TDIUnicodeReadMethods Read_jis_x0201;
extern PACKAGE TDIUnicodeWriteMethods Write_jis_x0201;
extern PACKAGE TDIUnicodeReadMethods Read_jis_x0208;
extern PACKAGE TDIUnicodeWriteMethods Write_jis_x0208;
extern PACKAGE TDIUnicodeReadMethods Read_jis_x0212;
extern PACKAGE TDIUnicodeWriteMethods Write_jis_x0212;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_646_CN;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_646_CN;
extern PACKAGE TDIUnicodeReadMethods Read_GB_2312_80;
extern PACKAGE TDIUnicodeWriteMethods Write_GB_2312_80;
extern PACKAGE TDIUnicodeReadMethods Read_iso_ir_165;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_ir_165;
extern PACKAGE TDIUnicodeReadMethods Read_gbk;
extern PACKAGE TDIUnicodeWriteMethods Write_gbk;
extern PACKAGE TDIUnicodeReadMethods Read_ksc_5601;
extern PACKAGE TDIUnicodeWriteMethods Write_ksc_5601;
extern PACKAGE TDIUnicodeReadMethods Read_euc_jp;
extern PACKAGE TDIUnicodeWriteMethods Write_euc_jp;
extern PACKAGE TDIUnicodeReadMethods Read_shift_jis;
extern PACKAGE TDIUnicodeWriteMethods Write_shift_jis;
extern PACKAGE TDIUnicodeReadMethods Read_CP_932;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_932;
extern PACKAGE TDIUnicodeReadMethods Read_iso_2022_jp;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_2022_jp;
extern PACKAGE TDIUnicodeReadMethods Read_iso_2022_jp_1;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_2022_jp_1;
extern PACKAGE TDIUnicodeReadMethods Read_iso_2022_jp_2;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_2022_jp_2;
extern PACKAGE TDIUnicodeReadMethods Read_GB_2312;
extern PACKAGE TDIUnicodeWriteMethods Write_GB_2312;
extern PACKAGE TDIUnicodeReadMethods Read_ces_gbk;
extern PACKAGE TDIUnicodeWriteMethods Write_ces_gbk;
extern PACKAGE TDIUnicodeReadMethods Read_CP_936;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_936;
extern PACKAGE TDIUnicodeReadMethods Read_GB_18030;
extern PACKAGE TDIUnicodeWriteMethods Write_GB_18030;
extern PACKAGE TDIUnicodeReadMethods Read_iso_2022_cn;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_2022_cn;
extern PACKAGE TDIUnicodeReadMethods Read_iso_2022_cn_ext;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_2022_cn_ext;
extern PACKAGE TDIUnicodeReadMethods Read_hz_gb_2312;
extern PACKAGE TDIUnicodeWriteMethods Write_hz_gb_2312;
extern PACKAGE TDIUnicodeReadMethods Read_euc_tw;
extern PACKAGE TDIUnicodeWriteMethods Write_euc_tw;
extern PACKAGE TDIUnicodeReadMethods Read_big5;
extern PACKAGE TDIUnicodeWriteMethods Write_big5;
extern PACKAGE TDIUnicodeReadMethods Read_CP_950;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_950;
extern PACKAGE TDIUnicodeReadMethods Read_big5_hkscs_1999;
extern PACKAGE TDIUnicodeWriteMethods Write_big5_hkscs_1999;
extern PACKAGE TDIUnicodeReadMethods Read_big5_hkscs_2001;
extern PACKAGE TDIUnicodeWriteMethods Write_big5_hkscs_2001;
extern PACKAGE TDIUnicodeReadMethods Read_big5_hkscs_2004;
extern PACKAGE TDIUnicodeWriteMethods Write_big5_hkscs_2004;
extern PACKAGE TDIUnicodeReadMethods Read_euc_kr;
extern PACKAGE TDIUnicodeWriteMethods Write_euc_kr;
extern PACKAGE TDIUnicodeReadMethods Read_CP_949;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_949;
extern PACKAGE TDIUnicodeReadMethods Read_johab;
extern PACKAGE TDIUnicodeWriteMethods Write_johab;
extern PACKAGE TDIUnicodeReadMethods Read_iso_2022_kr;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_2022_kr;
extern PACKAGE TDIUnicodeReadMethods Read_CP_856;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_856;
extern PACKAGE TDIUnicodeReadMethods Read_CP_922;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_922;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1046;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1046;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1124;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1124;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1129;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1129;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1161;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1161;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1162;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1162;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1163;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1163;
extern PACKAGE TDIUnicodeReadMethods Read_dec_kanji;
extern PACKAGE TDIUnicodeWriteMethods Write_dec_kanji;
extern PACKAGE TDIUnicodeReadMethods Read_dec_hanyu;
extern PACKAGE TDIUnicodeWriteMethods Write_dec_hanyu;
extern PACKAGE TDIUnicodeReadMethods Read_CP_437;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_437;
extern PACKAGE TDIUnicodeReadMethods Read_CP_737;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_737;
extern PACKAGE TDIUnicodeReadMethods Read_CP_775;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_775;
extern PACKAGE TDIUnicodeReadMethods Read_CP_852;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_852;
extern PACKAGE TDIUnicodeReadMethods Read_CP_853;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_853;
extern PACKAGE TDIUnicodeReadMethods Read_CP_855;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_855;
extern PACKAGE TDIUnicodeReadMethods Read_CP_857;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_857;
extern PACKAGE TDIUnicodeReadMethods Read_CP_858;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_858;
extern PACKAGE TDIUnicodeReadMethods Read_CP_860;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_860;
extern PACKAGE TDIUnicodeReadMethods Read_CP_861;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_861;
extern PACKAGE TDIUnicodeReadMethods Read_CP_863;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_863;
extern PACKAGE TDIUnicodeReadMethods Read_CP_864;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_864;
extern PACKAGE TDIUnicodeReadMethods Read_CP_865;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_865;
extern PACKAGE TDIUnicodeReadMethods Read_CP_869;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_869;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1125;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1125;
extern PACKAGE TDIUnicodeReadMethods Read_euc_jisx0213;
extern PACKAGE TDIUnicodeWriteMethods Write_euc_jisx0213;
extern PACKAGE TDIUnicodeReadMethods Read_shift_jisx0213;
extern PACKAGE TDIUnicodeWriteMethods Write_shift_jisx0213;
extern PACKAGE TDIUnicodeReadMethods Read_iso_2022_jp_3;
extern PACKAGE TDIUnicodeWriteMethods Write_iso_2022_jp_3;
extern PACKAGE TDIUnicodeReadMethods Read_big5_2003;
extern PACKAGE TDIUnicodeWriteMethods Write_big5_2003;
extern PACKAGE TDIUnicodeReadMethods Read_TDS_565;
extern PACKAGE TDIUnicodeWriteMethods Write_TDS_565;
extern PACKAGE TDIUnicodeReadMethods Read_AtariST;
extern PACKAGE TDIUnicodeWriteMethods Write_AtariST;
extern PACKAGE TDIUnicodeReadMethods Read_riscos_latin1;
extern PACKAGE TDIUnicodeWriteMethods Write_riscos_latin1;
extern PACKAGE unsigned __fastcall CodePageFromLocale(const unsigned Language);

}	/* namespace Diunicode */
using namespace Diunicode;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiunicodeHPP
