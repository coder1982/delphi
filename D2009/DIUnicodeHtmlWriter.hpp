// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diunicodehtmlwriter.pas' rev: 20.00

#ifndef DiunicodehtmlwriterHPP
#define DiunicodehtmlwriterHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Dihtmlmisc.hpp>	// Pascal unit
#include <Diunicode.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diunicodehtmlwriter
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TDIPredefinedEntities { peLt, peGt, peAmp, peQuot, peApos, peAposNum, peNbsp };
#pragma option pop

typedef Set<TDIPredefinedEntities, peLt, peNbsp>  TDIPredefinedEntitiesSet;

class DELPHICLASS TDIUnicodeHtmlWriter;
class PASCALIMPLEMENTATION TDIUnicodeHtmlWriter : public Diunicode::TDIUnicodeWriter
{
	typedef Diunicode::TDIUnicodeWriter inherited;
	
private:
	TDIPredefinedEntitiesSet FPredefinedEntities;
	Dihtmlmisc::TDIQuoteTags FQuoteCustomTags;
	Dihtmlmisc::TDIQuoteTags FQuoteHtmlTags;
	Dihtmlmisc::TDIQuoteTags FQuoteSsiTags;
	bool FEmptyCustomAttribValues;
	bool FEmptyHtmlAttribValues;
	bool FEmptySsiAttribValues;
	
public:
	__fastcall virtual TDIUnicodeHtmlWriter(Classes::TComponent* AOwner);
	void __fastcall WriteCustomStartTagAttribs(const System::UnicodeString TagName, System::UnicodeString const *Attribs, const int Attribs_Size, const System::WideChar CustomTagStartChar = (System::WideChar)(0x23));
	bool __fastcall WriteCustomTagAttribValue(const System::UnicodeString AValue);
	bool __fastcall WriteHtmlTagAttribValue(const System::UnicodeString AValue);
	void __fastcall WriteHtmlAnchorEnd(void);
	void __fastcall WriteHtmlBoldEnd(void);
	void __fastcall WriteHtmlBoldStart(void);
	void __fastcall WriteHtmlLineBreak(void);
	void __fastcall WriteHtmlBufW(System::WideChar * Buffer, unsigned CharCount);
	void __fastcall WriteHtmlCharSetInfo(const System::UnicodeString CharSetName, const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1));
	void __fastcall WriteHtmlCharEncodeW(const System::WideChar c);
	void __fastcall WriteHtmlCharW(const System::WideChar c);
	void __fastcall WriteHtmlCodeEnd(void);
	void __fastcall WriteHtmlCodeStart(void);
	void __fastcall WriteHtmlDocType(const System::UnicodeString Text);
	void __fastcall WriteHtmlEndTag(const System::UnicodeString TagName);
	void __fastcall WriteHtmlNoBreakSpace(void);
	void __fastcall WriteHtmlParagraph(const System::UnicodeString Text);
	void __fastcall WriteHtmlParagraphAttribs(System::UnicodeString const *Attribs, const int Attribs_Size, const System::UnicodeString Text);
	void __fastcall WriteHtmlParagraphEnd(void);
	void __fastcall WriteHtmlParagraphStart(void);
	void __fastcall WriteHtmlStartTag(const System::UnicodeString TagName);
	void __fastcall WriteHtmlStartTagAttribs(const System::UnicodeString TagName, System::UnicodeString const *Attribs, const int Attribs_Size);
	void __fastcall WriteHtmlEmptyElementTagAttribs(const System::UnicodeString TagName, System::UnicodeString const *Attribs, const int Attribs_Size);
	void __fastcall WriteHtmlStrW(const System::UnicodeString Text);
	void __fastcall WriteHtmlStrBoldW(const System::UnicodeString Text);
	void __fastcall WriteHtmlStrCodeW(const System::UnicodeString Text);
	void __fastcall WriteHtmlStrThW(const System::UnicodeString Text);
	void __fastcall WriteHtmlStrTagW(const System::UnicodeString TagName, const System::UnicodeString Text);
	void __fastcall WriteHtmlTableEnd(void);
	void __fastcall WriteHtmlTdEnd(void);
	void __fastcall WriteHtmlTdStart(void);
	void __fastcall WriteHtmlThEnd(void);
	void __fastcall WriteHtmlThStart(void);
	void __fastcall WriteHtmlTitle(const System::UnicodeString Text);
	void __fastcall WriteHtmlTitleEnd(void);
	void __fastcall WriteHtmlTitleStart(void);
	void __fastcall WriteHtmlTrStart(void);
	void __fastcall WriteHtmlTrEnd(void);
	void __fastcall WriteSsiStartTagAttribs(const System::UnicodeString TagName, System::UnicodeString const *Attribs, const int Attribs_Size);
	bool __fastcall WriteSsiTagAttribValue(const System::UnicodeString AValue);
	
__published:
	__property TDIPredefinedEntitiesSet PredefinedEntities = {read=FPredefinedEntities, write=FPredefinedEntities, default=15};
	__property Dihtmlmisc::TDIQuoteTags QuoteCustomTags = {read=FQuoteCustomTags, write=FQuoteCustomTags, default=1};
	__property Dihtmlmisc::TDIQuoteTags QuoteHtmlTags = {read=FQuoteHtmlTags, write=FQuoteHtmlTags, default=1};
	__property Dihtmlmisc::TDIQuoteTags QuoteSsiTags = {read=FQuoteSsiTags, write=FQuoteSsiTags, default=1};
	__property bool EmptyCustomAttribValues = {read=FEmptyCustomAttribValues, write=FEmptyCustomAttribValues, default=0};
	__property bool EmptyHtmlAttribValues = {read=FEmptyHtmlAttribValues, write=FEmptyHtmlAttribValues, default=0};
	__property bool EmptySsiAttribValues = {read=FEmptySsiAttribValues, write=FEmptySsiAttribValues, default=0};
public:
	/* TDIUnicodeWriter.Destroy */ inline __fastcall virtual ~TDIUnicodeHtmlWriter(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
#define DEFAULT_PREDEFINED_ENTITIES (Set<TDIPredefinedEntities, peLt, peNbsp> () << peLt << peGt << peAmp << peQuot )
extern PACKAGE void __fastcall RegisterEncodingEntity(const System::WideChar EntityChar, const System::UnicodeString EntityName);
extern PACKAGE void __fastcall RegisterEncodingEntities(Dihtmlmisc::TDIHtmlEntity const *Entities, const int Entities_Size);
extern PACKAGE void __fastcall RegisterHtmlEncodingEntities(void);
extern PACKAGE void __fastcall UnRegisterEncodingEntity(const System::WideChar EntityChar);
extern PACKAGE void __fastcall ClearEncodingEntities(void);

}	/* namespace Diunicodehtmlwriter */
using namespace Diunicodehtmlwriter;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiunicodehtmlwriterHPP
