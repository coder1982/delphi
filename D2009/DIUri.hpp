// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diuri.pas' rev: 20.00

#ifndef DiuriHPP
#define DiuriHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diuri
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TDIUri;
class PASCALIMPLEMENTATION TDIUri : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::UnicodeString FFragment;
	System::UnicodeString FHost;
	System::UnicodeString FPassword;
	System::UnicodeString FPath;
	System::UnicodeString FPort;
	System::UnicodeString FQuery;
	System::UnicodeString FScheme;
	System::UnicodeString FUser;
	bool FHasFragment;
	bool FHasHost;
	bool FHasPassword;
	bool FHasPort;
	bool FHasQuery;
	bool FHasScheme;
	bool FHasUser;
	System::UnicodeString __fastcall GetAuthority();
	System::UnicodeString __fastcall GetReference();
	void __fastcall SetFragment(const System::UnicodeString Value);
	
public:
	void __fastcall Assign(const TDIUri* Source);
	void __fastcall AssignAuthority(const TDIUri* Source);
	void __fastcall AssignFragment(const TDIUri* Source);
	void __fastcall AssignQuery(const TDIUri* Source);
	void __fastcall AssignScheme(const TDIUri* Source);
	void __fastcall Clear(void);
	void __fastcall ClearAuthority(void);
	void __fastcall ClearFragment(void);
	void __fastcall ClearHost(void);
	void __fastcall ClearHostPort(void);
	void __fastcall ClearPassword(void);
	void __fastcall ClearPath(void);
	void __fastcall ClearPort(void);
	void __fastcall ClearQuery(void);
	void __fastcall ClearScheme(void);
	void __fastcall ClearUser(void);
	void __fastcall ClearUserPassword(void);
	__property bool HasAuthority = {read=FHasHost, nodefault};
	__property bool HasFragment = {read=FHasFragment, nodefault};
	__property bool HasHost = {read=FHasHost, nodefault};
	__property bool HasPassword = {read=FHasPassword, nodefault};
	__property bool HasPort = {read=FHasPort, nodefault};
	__property bool HasQuery = {read=FHasQuery, nodefault};
	__property bool HasScheme = {read=FHasScheme, nodefault};
	__property bool HasUser = {read=FHasUser, nodefault};
	bool __fastcall SetAuthority(const System::UnicodeString Value);
	bool __fastcall SetHost(const System::UnicodeString Value);
	bool __fastcall SetHostPort(const System::UnicodeString Value);
	bool __fastcall SetPassword(const System::UnicodeString Value);
	bool __fastcall SetPath(const System::UnicodeString Value);
	bool __fastcall SetPort(const System::UnicodeString Value);
	bool __fastcall SetQuery(const System::UnicodeString Value);
	bool __fastcall SetReference(const System::UnicodeString Value);
	bool __fastcall SetScheme(const System::UnicodeString Value);
	bool __fastcall SetUser(const System::UnicodeString Value);
	bool __fastcall SetUserPassword(const System::UnicodeString Value);
	__property System::UnicodeString Authority = {read=GetAuthority};
	__property System::UnicodeString Fragment = {read=FFragment, write=SetFragment};
	__property System::UnicodeString Host = {read=FHost};
	__property System::UnicodeString Password = {read=FPassword};
	__property System::UnicodeString Path = {read=FPath};
	__property System::UnicodeString Port = {read=FPort};
	__property System::UnicodeString Query = {read=FQuery};
	__property System::UnicodeString Reference = {read=GetReference};
	__property System::UnicodeString Scheme = {read=FScheme};
	__property System::UnicodeString User = {read=FUser};
public:
	/* TObject.Create */ inline __fastcall TDIUri(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TDIUri(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::UnicodeString __fastcall StrEscapeUriW(const System::UnicodeString Uri);
extern PACKAGE bool __fastcall FileNameToUriW(const System::UnicodeString FileName, /* out */ System::UnicodeString &Uri);
extern PACKAGE bool __fastcall UriToFileName(const TDIUri* AUri, /* out */ System::UnicodeString &AFileName);
extern PACKAGE bool __fastcall UriToFileNameW(const System::UnicodeString AUri, /* out */ System::UnicodeString &AFileName);
extern PACKAGE bool __fastcall ResolveRelativeUriW(const System::UnicodeString BaseUri, const System::UnicodeString RelativeUri, /* out */ System::UnicodeString &ResultUri);
extern PACKAGE bool __fastcall StrIsAbsoluteUriW(const System::UnicodeString Value);

}	/* namespace Diuri */
using namespace Diuri;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiuriHPP
