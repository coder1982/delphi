// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dicardinalhash.pas' rev: 20.00

#ifndef DicardinalhashHPP
#define DicardinalhashHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dicardinalhash
{
//-- type declarations -------------------------------------------------------
typedef unsigned TDINumberType;

typedef unsigned *PDINumberType;

#pragma pack(push,1)
struct TDIItemType
{
	
public:
	unsigned Number;
};
#pragma pack(pop)


typedef TDIItemType *PDIItemType;

class DELPHICLASS TDICardinalHash;
class PASCALIMPLEMENTATION TDICardinalHash : public Dicontainers::TDIHash
{
	typedef Dicontainers::TDIHash inherited;
	
protected:
	unsigned __fastcall GetNumberOfKey(const void *AKey);
	unsigned __fastcall GetNumberOfKeyBuf(const void *Buffer, const unsigned BufferSize);
	PDINumberType __fastcall GetPNumberOfKey(const void *AKey);
	PDINumberType __fastcall GetPNumberOfKeyBuf(const void *Buffer, const unsigned BufferSize);
	void __fastcall SetNumberOfKey(const void *AKey, const unsigned ANumber);
	void __fastcall SetNumberOfKeyBuf(const void *Buffer, const unsigned BufferSize, const unsigned ANumber);
	void __fastcall SetPNumberOfKey(const void *AKey, const PDINumberType ANumber);
	void __fastcall SetPNumberOfKeyBuf(const void *Buffer, const unsigned BufferSize, const PDINumberType ANumber);
	unsigned __fastcall GetNumberAt(const int Index);
	
public:
	__property unsigned NumberAt[const int Index] = {read=GetNumberAt};
	__property unsigned NumberOfKey[void AKey] = {read=GetNumberOfKey, write=SetNumberOfKey};
	__property unsigned NumberOfKeyBuf[void Buffer][const unsigned BufferSize] = {read=GetNumberOfKeyBuf, write=SetNumberOfKeyBuf};
	__property PDINumberType PNumberOfKey[void AKey] = {read=GetPNumberOfKey, write=SetPNumberOfKey};
	__property PDINumberType PNumberOfKeyBuf[void Buffer][const unsigned BufferSize] = {read=GetPNumberOfKeyBuf, write=SetPNumberOfKeyBuf};
	void * __fastcall InsertNumberByKey(const void *AKey, const unsigned ANumber);
public:
	/* TDIHash.Create */ inline __fastcall TDICardinalHash(const Dicontainers::TDIItemHandler* AItemHandler, const Dicontainers::TDIKeyHandler* AKeyHandler) : Dicontainers::TDIHash(AItemHandler, AKeyHandler) { }
	/* TDIHash.Destroy */ inline __fastcall virtual ~TDICardinalHash(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_NUMBER;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_NUMBER;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_NUMBER;
extern PACKAGE TDICardinalHash* __fastcall NewDICardinalHash(const Dicontainers::TDIKeyHandler* KeyHandler);

}	/* namespace Dicardinalhash */
using namespace Dicardinalhash;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DicardinalhashHPP
