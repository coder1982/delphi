// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diobjectvector.pas' rev: 20.00

#ifndef DiobjectvectorHPP
#define DiobjectvectorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diobjectvector
{
//-- type declarations -------------------------------------------------------
typedef System::TObject* *PDIObjectType;

typedef System::TObject* TDIObjectType;

struct TDIItemType;
typedef TDIItemType *PDIItemType;

#pragma pack(push,1)
struct TDIItemType
{
	
public:
	System::TObject* Obj;
};
#pragma pack(pop)


class DELPHICLASS TDIObjectVector;
class PASCALIMPLEMENTATION TDIObjectVector : public Dicontainers::TDIVector
{
	typedef Dicontainers::TDIVector inherited;
	
protected:
	System::TObject* __fastcall GetFirstObject(void);
	void __fastcall SetFirstObject(const System::TObject* AObj);
	System::TObject* __fastcall GetLastObject(void);
	void __fastcall SetLastObject(const System::TObject* Obj);
	System::TObject* __fastcall GetObject(void * PItem);
	System::TObject* __fastcall GetObjectAt(const int Index);
	System::TObject* __fastcall GetObjectOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems);
	void __fastcall SetObjectOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems, const System::TObject* Obj);
	void __fastcall SetObject(void * PItem, const System::TObject* Obj);
	void __fastcall SetObjectAt(const int Index, const System::TObject* Obj);
	
public:
	__property System::TObject* Obj[void * PItem] = {read=GetObject, write=SetObject};
	__property System::TObject* ObjectAt[const int Index] = {read=GetObjectAt, write=SetObjectAt};
	__property System::TObject* ObjectOf[void * PItem][const Dicontainers::TDISameItemsFunc SameItems] = {read=GetObjectOf, write=SetObjectOf};
	__property System::TObject* FirstObject = {read=GetFirstObject, write=SetFirstObject};
	__property System::TObject* LastObject = {read=GetLastObject, write=SetLastObject};
	void __fastcall RemoveAllClasses(const System::TClass AClass, const int StartIndex = 0x0);
	int __fastcall InsertObjectSorted(const System::TObject* AObj);
	int __fastcall InsertObjectSortedDesc(const System::TObject* AObj);
	bool __fastcall ExistsObject(const System::TObject* AObj);
	bool __fastcall ExistsObjectBack(const System::TObject* AObj);
	bool __fastcall ExistsObjectSorted(const System::TObject* Obj);
	bool __fastcall ExistAllObjects(System::TObject* const *AObjs, const int AObjs_Size);
	bool __fastcall ExistsAnyObject(System::TObject* const *AObjs, const int AObjs_Size);
	bool __fastcall FindObject(const System::TObject* Obj, /* out */ int &Index);
	bool __fastcall FindObjectDesc(const System::TObject* AObj, /* out */ int &Index);
	int __fastcall IndexOfObject(const System::TObject* AObj);
	int __fastcall IndexOfObjectBack(const System::TObject* AObj);
	void * __fastcall InsertObjectAt(const int Index, const System::TObject* AObj);
	void * __fastcall InsertObjectFirst(const System::TObject* AObj);
	void * __fastcall InsertObjectLast(const System::TObject* AObj);
	void __fastcall InsertObjectsLast(System::TObject* const *AObjects, const int AObjects_Size);
	void __fastcall RemoveAllObjects(const System::TObject* AObj);
	void __fastcall RemoveAllObjectsBack(const System::TObject* AObj, const int StartIndex = 0xffffffff);
	void * __fastcall PItemOfObject(const System::TObject* Obj, const int StartIndex = 0x0);
	void * __fastcall PItemOfObjectBack(const System::TObject* Obj);
	void __fastcall SortByObject(void);
	void __fastcall SortByObjectDesc(void);
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDIObjectVector(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDIObjectVector(const Dicontainers::TDIItemHandler* AItemHandler) : Dicontainers::TDIVector(AItemHandler) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_OBJECT;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_OBJECT;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_OBJECT;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_CLASS;
extern PACKAGE TDIObjectVector* __fastcall NewDIObjectVector(void);

}	/* namespace Diobjectvector */
using namespace Diobjectvector;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiobjectvectorHPP
