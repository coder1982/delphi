// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dicardinaltree.pas' rev: 20.00

#ifndef DicardinaltreeHPP
#define DicardinaltreeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dicardinaltree
{
//-- type declarations -------------------------------------------------------
typedef unsigned *PDINumberType;

typedef unsigned TDINumberType;

struct TDIItemType;
typedef TDIItemType *PDIItemType;

#pragma pack(push,1)
struct TDIItemType
{
	
public:
	unsigned Number;
};
#pragma pack(pop)


class DELPHICLASS TDICardinalTree;
class PASCALIMPLEMENTATION TDICardinalTree : public Dicontainers::TDITree
{
	typedef Dicontainers::TDITree inherited;
	
protected:
	unsigned __fastcall GetFirstNumber(void);
	void __fastcall SetFirstNumber(const unsigned ANumber);
	unsigned __fastcall GetLastNumber(void);
	void __fastcall SetLastNumber(const unsigned Number);
	unsigned __fastcall GetFirstChildNumber(void * PItem);
	unsigned __fastcall GetLastChildNumber(void * PItem);
	unsigned __fastcall GetNumber(void * PItem);
	unsigned __fastcall GetNumberOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems);
	void __fastcall SetNumberOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems, const unsigned Number);
	void __fastcall SetFirstChildNumber(void * PItem, const unsigned Number);
	void __fastcall SetLastChildNumber(void * PItem, const unsigned Number);
	void __fastcall SetNumber(void * PItem, const unsigned Number);
	
public:
	__property unsigned Number[void * PItem] = {read=GetNumber, write=SetNumber};
	__property unsigned NumberOf[void * PItem][const Dicontainers::TDISameItemsFunc SameItems] = {read=GetNumberOf, write=SetNumberOf};
	__property unsigned FirstNumber = {read=GetFirstNumber, write=SetFirstNumber, nodefault};
	__property unsigned LastNumber = {read=GetLastNumber, write=SetLastNumber, nodefault};
	__property unsigned FirstChildNumber[void * PItem] = {read=GetFirstChildNumber, write=SetFirstChildNumber};
	__property unsigned LastChildNumber[void * PItem] = {read=GetLastChildNumber, write=SetLastChildNumber};
	bool __fastcall ExistsNumber(const unsigned ANumber);
	bool __fastcall ExistsNumberBack(const unsigned ANumber);
	bool __fastcall ExistAllNumbers(unsigned const *ANumbers, const int ANumbers_Size);
	bool __fastcall ExistsAnyNumber(unsigned const *ANumbers, const int ANumbers_Size);
	void * __fastcall InsertNumberAfter(const void * PItem, const unsigned Number);
	void * __fastcall InsertNumberBefore(const void * PItem, const unsigned Number);
	void * __fastcall InsertNumberChildFirst(const void * PItem, const unsigned Number);
	void * __fastcall InsertNumberChildLast(const void * PItem, const unsigned Number);
	void * __fastcall InsertNumberFirst(const unsigned ANumber);
	void * __fastcall InsertNumberLast(const unsigned ANumber);
	void __fastcall InsertNumbersLast(unsigned const *ANumbers, const int ANumbers_Size);
	void * __fastcall PChildItemOfNumber(const unsigned ANumber, const void * StartItem);
	void * __fastcall PChildItemBackOfNumber(const unsigned ANumber, const void * StartItem);
	void * __fastcall PItemOfNumber(const unsigned Number)/* overload */;
	void * __fastcall PItemOfNumber(const unsigned Number, const void * PStartItem)/* overload */;
	void * __fastcall PItemOfNumberBack(const unsigned Number);
	void * __fastcall PSiblingItemOfNumber(const unsigned Number, const void * PStartItem = (void *)(0x0));
	void __fastcall SortByNumber(const void * PItem);
	void __fastcall SortRecurseByNumber(const void * PItem = (void *)(0x0));
public:
	/* TDITree.Destroy */ inline __fastcall virtual ~TDICardinalTree(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDICardinalTree(const Dicontainers::TDIItemHandler* AItemHandler) : Dicontainers::TDITree(AItemHandler) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_NUMBER;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_NUMBER;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_NUMBER;
extern PACKAGE TDICardinalTree* __fastcall NewDICardinalTree(void);

}	/* namespace Dicardinaltree */
using namespace Dicardinaltree;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DicardinaltreeHPP
