// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diunicode.pas' rev: 20.00

#ifndef DiunicodeHPP
#define DiunicodeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Diutils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diunicode
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TDIBOM { bomUnknown, bomUtf8, bomutf16be, bomutf16le, bomutf32be, bomutf32le };
#pragma option pop

class DELPHICLASS TDIUnicodeBase;
class PASCALIMPLEMENTATION TDIUnicodeBase : public Classes::TComponent
{
	typedef Classes::TComponent inherited;
	
protected:
	__classmethod void __fastcall Error(const System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size);
public:
	/* TComponent.Create */ inline __fastcall virtual TDIUnicodeBase(Classes::TComponent* AOwner) : Classes::TComponent(AOwner) { }
	/* TComponent.Destroy */ inline __fastcall virtual ~TDIUnicodeBase(void) { }
	
};


class DELPHICLASS TDIUnicodeReader;
typedef void __fastcall (*TDIUnicodeReadInit)(const TDIUnicodeReader* UnicodeReader);

typedef bool __fastcall (*TDIUnicodeReadChar)(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);

typedef TDIUnicodeReadChar *PDIUnicodeReadChar;

typedef void __fastcall (*TDIUnicodeReadFinalize)(const TDIUnicodeReader* UnicodeReader);

struct TDIUnicodeReadMethods
{
	
public:
	TDIUnicodeReadInit Init;
	TDIUnicodeReadChar ReadChar;
	TDIUnicodeReadFinalize Finalize;
};


typedef TDIUnicodeReadMethods *PDIUnicodeReadMethods;

struct TUnicodeReaderSource;
typedef TUnicodeReaderSource *PUnicodeReaderSource;

struct TUnicodeReaderSource
{
	
public:
	TUnicodeReaderSource *Next;
	unsigned CharPos;
	unsigned CharLine;
	unsigned CharCol;
	TDIUnicodeReadMethods ReadMethods;
	void *SourceBuffer;
	unsigned SourceBufferSize;
	unsigned SourceBufferPosition;
	System::UnicodeString SourceFile;
	Classes::TStream* SourceStream;
	void *UserData;
};


#pragma option push -b-
enum TAutoFreeSourceStreams { afNone, afAllButFirst, afAll };
#pragma option pop

class PASCALIMPLEMENTATION TDIUnicodeReader : public TDIUnicodeBase
{
	typedef TDIUnicodeBase inherited;
	
private:
	bool FCharOK;
	System::WideChar FChar;
	System::WideChar FLastChar;
	unsigned FCharPos;
	unsigned FCharLine;
	unsigned FCharCol;
	void *FSourceBuffer;
	unsigned FSourceBufferSize;
	unsigned FSourceBufferPosition;
	System::WideChar *FPeekedChars;
	unsigned FPeekedCount;
	unsigned FPeekedCapacity;
	System::UnicodeString FSourceFile;
	TUnicodeReaderSource *FSourceStack;
	TDIUnicodeReadMethods FReadMethods;
	void *FState;
	Classes::TStream* FSourceStream;
	TAutoFreeSourceStreams FAutoFreeSourceStreams;
	void __fastcall AdjustDataCapacity(const unsigned MinCapacity);
	void __fastcall SetDataSize(const unsigned NewDataSize);
	System::UnicodeString __fastcall GetSourceBufferAsStr();
	System::RawByteString __fastcall GetSourceBufferAsStrA();
	System::UnicodeString __fastcall GetSourceBufAsStrW();
	void __fastcall SetSourceBufferAsStrProp(const System::UnicodeString ASourceStr);
	void __fastcall SetSourceBufferAsStrAProp(const System::RawByteString ASourceStr);
	void __fastcall SetSourceBufferAsStrWProp(const System::UnicodeString ASourceStr);
	void __fastcall SetSourceBufferPosition(const unsigned NewPosition);
	void __fastcall SetSourceBufferSize(const unsigned NewSize);
	void __fastcall SetSourceFile(const System::UnicodeString AFileName);
	void __fastcall SetSourceStreamProp(const Classes::TStream* AStream);
	void __fastcall SetReadMethods(const TDIUnicodeReadMethods &Value);
	
protected:
	System::WideChar *FData;
	unsigned FDataSize;
	unsigned FDataCapacity;
	__property TAutoFreeSourceStreams AutoFreeSourceStreams = {read=FAutoFreeSourceStreams, write=FAutoFreeSourceStreams, default=0};
	__property PUnicodeReaderSource SourceStack = {read=FSourceStack};
	void __fastcall TrimmedDataStartAndLength(Diutils::TDIValidateWideCharFunc Validate, /* out */ System::WideChar * &Start, /* out */ unsigned &Length);
	void __fastcall TrimmedLeftDataStartAndLength(Diutils::TDIValidateWideCharFunc Validate, /* out */ System::WideChar * &Start, /* out */ unsigned &Length);
	unsigned __fastcall TrimmedRightDataLength(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	
public:
	__fastcall virtual TDIUnicodeReader(Classes::TComponent* AOwner);
	__fastcall virtual ~TDIUnicodeReader(void);
	void __fastcall AddChar(void)/* overload */;
	void __fastcall AddChar(const System::WideChar c)/* overload */;
	void __fastcall AddCharCompact(const System::WideChar c);
	void __fastcall AddCrLf(void);
	void __fastcall AddSpace(void);
	void __fastcall AddSpaceCompact(void);
	void __fastcall AddStrW(const System::UnicodeString s);
	void __fastcall AddStrCompactW(const System::UnicodeString Value);
	bool __fastcall AddCharReadChar(void);
	__property System::WideChar Char = {read=FChar, nodefault};
	__property bool CharOK = {read=FCharOK, write=FCharOK, nodefault};
	__property unsigned CharPos = {read=FCharPos, nodefault};
	__property unsigned CharLine = {read=FCharLine, nodefault};
	__property unsigned CharCol = {read=FCharCol, nodefault};
	void __fastcall ClearData(void);
	void __fastcall ClearPeekedChars(void);
	void __fastcall ClearSourceStack(void);
	__property System::WideChar * Data = {read=FData};
	__property System::WideChar * PeekedChars = {read=FPeekedChars};
	__property void * SourceBuffer = {read=FSourceBuffer};
	__property TDIUnicodeReadMethods ReadMethods = {read=FReadMethods, write=SetReadMethods};
	System::UnicodeString __fastcall DataAsStr();
	System::AnsiString __fastcall DataAsStrA(const unsigned CodePage = (unsigned)(0x0));
	System::UnicodeString __fastcall DataAsStrW();
	System::UTF8String __fastcall DataAsStr8();
	System::AnsiString __fastcall DataAsStrTrimA(const unsigned CodePage = (unsigned)(0x0), const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	System::AnsiString __fastcall DataToStrA();
	System::AnsiString __fastcall DataToStrTrimA(const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	System::UnicodeString __fastcall DataAsStrTrimW(const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	System::UnicodeString __fastcall DataAsStrTrimLeftW(const Diutils::TDIValidateWideCharFunc Validate = 0x0);
	bool __fastcall DataHasChars(const Diutils::TDIValidateWideCharFunc Validate);
	bool __fastcall DataIsEmpty(void);
	bool __fastcall DataIsChars(const Diutils::TDIValidateWideCharFunc Validate);
	bool __fastcall DataIsNotEmpty(void);
	bool __fastcall DataIsStrW(const System::UnicodeString s);
	bool __fastcall DataIsStrIW(const System::UnicodeString s);
	__property unsigned DataSize = {read=FDataSize, write=SetDataSize, nodefault};
	unsigned __fastcall FillSourceBuffer(const bool MoveEndToFront = false);
	System::UnicodeString __fastcall FirstSourceFile();
	void __fastcall FreeSourceStream(void);
	void __fastcall IncSourceBufferPosition(void)/* overload */;
	void __fastcall IncSourceBufferPosition(const unsigned n)/* overload */;
	bool __fastcall PeekChars(const unsigned Count = (unsigned)(0x1));
	__property unsigned PeekedCount = {read=FPeekedCount, nodefault};
	void __fastcall PushSource(const bool StoreReadMethods);
	bool __fastcall PopSource(void);
	TDIBOM __fastcall ReadBOM(void);
	bool __fastcall ReadChar(void);
	void __fastcall ReadChars(const System::WideChar c)/* overload */;
	void __fastcall ReadChars(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall ReadChars(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall ReadCharsTill(const System::WideChar c)/* overload */;
	void __fastcall ReadCharsTill(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall ReadCharsTill(const System::WideChar c, const System::WideChar c2, const System::WideChar c3)/* overload */;
	void __fastcall ReadCharsTill(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall ReadCharsTillWhiteSpace(void);
	void __fastcall ReadDigits(void);
	void __fastcall SkipDigits(void);
	void __fastcall ReadHexDigits(void);
	void __fastcall SkipHexDigits(void);
	bool __fastcall ReadLine(void);
	bool __fastcall ReadTillEndOfLine(void);
	void __fastcall ReadWhiteSpace(void);
	void __fastcall ResetCharLocation(void);
	void __fastcall SetSourceBufferAsStr(const System::UnicodeString ASourceStr, const System::UnicodeString AFileName = L"");
	void __fastcall SetSourceBufferAsStrA(const System::RawByteString ASourceStr, const System::UnicodeString AFileName = L"");
	void __fastcall SetSourceBufferAsStrW(const System::UnicodeString ASourceStr, const System::UnicodeString AFileName = L"");
	void __fastcall SetSourceStream(const Classes::TStream* AStream, const System::UnicodeString AFileName = L"");
	void __fastcall SkipChars(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall SkipChars(const System::WideChar c)/* overload */;
	void __fastcall SkipChars(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall SkipCharsTill(const Diutils::TDIValidateWideCharFunc Validate)/* overload */;
	void __fastcall SkipCharsTill(const System::WideChar c)/* overload */;
	void __fastcall SkipCharsTill(const System::WideChar c, const System::WideChar c2)/* overload */;
	void __fastcall SkipCharsTill(const System::WideChar c, const System::WideChar c2, const System::WideChar c3)/* overload */;
	bool __fastcall SkipEmptyLines(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	bool __fastcall SkipLine(void);
	bool __fastcall SkipTillEndOfLine(void);
	void __fastcall SkipWhiteSpace(void);
	void __fastcall RewindSource(void);
	void __fastcall SetSourceBuffer(const void *Buffer, const unsigned BufferSize, const System::UnicodeString AFileName = L"");
	void __fastcall SaveDataToFile(const System::UnicodeString FileName, const bool WriteByteOrderMark = false);
	void __fastcall SaveDataToStream(const Classes::TStream* Stream, const bool WriteByteOrderMark = false);
	__property System::UnicodeString SourceBufferAsStr = {read=GetSourceBufferAsStr, write=SetSourceBufferAsStrProp};
	__property System::RawByteString SourceBufferAsStrA = {read=GetSourceBufferAsStrA, write=SetSourceBufferAsStrAProp};
	void __fastcall SourceStreamModified(void);
	unsigned __fastcall TrimmedDataLength(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	__property System::UnicodeString SourceBufferAsStrW = {read=GetSourceBufAsStrW, write=SetSourceBufferAsStrWProp};
	__property unsigned SourceBufferSize = {read=FSourceBufferSize, write=SetSourceBufferSize, nodefault};
	__property unsigned SourceBufferPosition = {read=FSourceBufferPosition, write=SetSourceBufferPosition, nodefault};
	__property System::UnicodeString SourceFile = {read=FSourceFile, write=SetSourceFile};
	__property Classes::TStream* SourceStream = {read=FSourceStream, write=SetSourceStreamProp};
	void __fastcall TrimDataRight(Diutils::TDIValidateWideCharFunc Validate = 0x0);
	void __fastcall ZeroDataSize(void);
	__property void * State = {read=FState, write=FState};
};


class DELPHICLASS TDIUnicodeWriter;
typedef void __fastcall (*TDIUnicodeWriteInit)(const TDIUnicodeWriter* UnicodeWriter);

typedef bool __fastcall (*TDIUnicodeWriteChar)(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar Char);

typedef void __fastcall (*TDIUnicodeWriteFlush)(const TDIUnicodeWriter* UnicodeWriter);

typedef void __fastcall (*TDIUnicodeWriteFinalize)(const TDIUnicodeWriter* UnicodeWriter);

struct TDIUnicodeWriteMethods
{
	
public:
	TDIUnicodeWriteInit Init;
	TDIUnicodeWriteChar WriteChar;
	TDIUnicodeWriteFlush Flush;
	TDIUnicodeWriteFinalize Finalize;
};


typedef TDIUnicodeWriteMethods *PDIUnicodeWriteMethods;

#pragma option push -b-
enum TDIInvalidCharAction { icWrite, icOwnerWrite, icAbort };
#pragma option pop

typedef void __fastcall (__closure *TDIInvalidCharEvent)(const TDIUnicodeWriter* Sender, const System::WideChar InvalidChar, System::WideChar &ReplacementChar, const unsigned Attempt, TDIInvalidCharAction &Action);

class PASCALIMPLEMENTATION TDIUnicodeWriter : public TDIUnicodeBase
{
	typedef TDIUnicodeBase inherited;
	
private:
	void *FData;
	TDIUnicodeWriteMethods FWriteMethods;
	void *FState;
	unsigned FDataCapacity;
	unsigned FDataSize;
	Classes::TStream* FDataStream;
	System::WideChar FReplacementChar;
	TDIInvalidCharEvent FOnInvalidChar;
	void __fastcall SetWriteMethods(const TDIUnicodeWriteMethods &Value);
	void __fastcall SetDataStream(const Classes::TStream* AStream);
	
protected:
	void __fastcall RequestData(void);
	void __fastcall SetDataSize(const unsigned NewDataSize);
	bool __fastcall WriteInvalidChar(const System::WideChar InvalidChar);
	
public:
	__fastcall virtual TDIUnicodeWriter(Classes::TComponent* AOwner);
	__fastcall virtual ~TDIUnicodeWriter(void);
	__property void * Data = {read=FData};
	__property unsigned DataSize = {read=FDataSize, write=SetDataSize, nodefault};
	__property unsigned DataCapacity = {read=FDataCapacity, nodefault};
	__property Classes::TStream* DataStream = {read=FDataStream, write=SetDataStream};
	System::UnicodeString __fastcall DataToStr();
	System::AnsiString __fastcall DataToStrA();
	System::RawByteString __fastcall DataToStrRaw();
	System::UnicodeString __fastcall DataToStrW();
	void __fastcall Clear(void);
	void __fastcall FlushDataBuffer(void);
	void __fastcall FreeDataStream(void);
	void __fastcall IncDataSize(void);
	void __fastcall SaveDataToFile(const System::UnicodeString FileName);
	void __fastcall SaveDataToStream(const Classes::TStream* Stream);
	void __fastcall WriteByte(const System::Byte b);
	bool __fastcall WriteDecimalCodePoint(const System::WideChar c);
	bool __fastcall WriteDecimalNumber(unsigned Number, unsigned StartDiv)/* overload */;
	bool __fastcall WriteDecimalNumber(unsigned Number)/* overload */;
	bool __fastcall WriteDecimalNumber(int Number)/* overload */;
	bool __fastcall WriteDecimalNumber(__int64 Number)/* overload */;
	bool __fastcall WriteFile(const System::UnicodeString AFileName, const TDIUnicodeReadMethods &AReadMethods);
	bool __fastcall WriteHexCodePoint(const System::WideChar c);
	void __fastcall WriteLineBreak(void);
	void __fastcall WriteBufA(const char * Buffer, const unsigned CharCount, const unsigned CodePage = (unsigned)(0x0));
	void __fastcall WriteStrA(const System::AnsiString s, const unsigned CodePage = (unsigned)(0x0));
	bool __fastcall WriteStream(const Classes::TStream* AStream, const TDIUnicodeReadMethods &AReadMethods);
	void __fastcall WriteStrLineBreakA(const System::AnsiString s, const unsigned CodePage = (unsigned)(0x0));
	void __fastcall WriteBufDirect(const void *Buffer, const unsigned Size);
	bool __fastcall WriteBufW(System::WideChar * Buffer, unsigned CharCount);
	bool __fastcall WriteCharW(const System::WideChar c);
	bool __fastcall WriteCharsW(const System::WideChar c, const unsigned Count);
	void __fastcall WriteStrRaw(const System::AnsiString s);
	bool __fastcall WriteStrW(const System::UnicodeString s);
	void __fastcall WriteStrLineBreakW(const System::UnicodeString s);
	bool __fastcall TryWriteCharW(const System::WideChar c);
	__property TDIUnicodeWriteMethods WriteMethods = {read=FWriteMethods, write=SetWriteMethods};
	__property void * State = {read=FState, write=FState};
	
__published:
	__property System::WideChar ReplacementChar = {read=FReplacementChar, write=FReplacementChar, default=63};
	__property TDIInvalidCharEvent OnInvalidChar = {read=FOnInvalidChar, write=FOnInvalidChar};
};


//-- var, const, procedure ---------------------------------------------------
static const WideChar DEFAULT_REPLACEMENT_CHAR = (WideChar)(0x3f);
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_2;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_2;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_3;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_3;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_4;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_4;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_5;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_5;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_6;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_6;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_7;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_7;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_8;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_8;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_9;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_9;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_10;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_10;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_13;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_13;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_14;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_14;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_15;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_15;
extern PACKAGE TDIUnicodeReadMethods Read_Iso_8859_16;
extern PACKAGE TDIUnicodeWriteMethods Write_Iso_8859_16;
extern PACKAGE TDIUnicodeReadMethods Read_Koi8_R;
extern PACKAGE TDIUnicodeWriteMethods Write_Koi8_R;
extern PACKAGE TDIUnicodeReadMethods Read_Koi8_RU;
extern PACKAGE TDIUnicodeWriteMethods Write_Koi8_RU;
extern PACKAGE TDIUnicodeReadMethods Read_Koi8_U;
extern PACKAGE TDIUnicodeWriteMethods Write_Koi8_U;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1250;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1250;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1251;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1251;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1252;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1252;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1253;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1253;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1254;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1254;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1255;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1255;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1256;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1256;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1257;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1257;
extern PACKAGE TDIUnicodeReadMethods Read_Windows_1258;
extern PACKAGE TDIUnicodeWriteMethods Write_Windows_1258;
extern PACKAGE TDIUnicodeReadMethods Read_CP_850;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_850;
extern PACKAGE TDIUnicodeReadMethods Read_CP_862;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_862;
extern PACKAGE TDIUnicodeReadMethods Read_CP_866;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_866;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Roman;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Roman;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_CentralEurope;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_CentralEurope;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Dingbats;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Dingbats;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Iceland;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Iceland;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Croatian;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Croatian;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Romania;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Romania;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Cyrillic;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Cyrillic;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Greek;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Greek;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Turkish;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Turkish;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Hebrew;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Hebrew;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Arabic;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Arabic;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Thai;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Thai;
extern PACKAGE TDIUnicodeReadMethods Read_Mac_Farsi;
extern PACKAGE TDIUnicodeWriteMethods Write_Mac_Farsi;
extern PACKAGE TDIUnicodeReadMethods Read_CP_424;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_424;
extern PACKAGE TDIUnicodeReadMethods Read_CP_437;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_437;
extern PACKAGE TDIUnicodeReadMethods Read_CP_737;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_737;
extern PACKAGE TDIUnicodeReadMethods Read_CP_775;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_775;
extern PACKAGE TDIUnicodeReadMethods Read_CP_852;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_852;
extern PACKAGE TDIUnicodeReadMethods Read_CP_855;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_855;
extern PACKAGE TDIUnicodeReadMethods Read_CP_856;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_856;
extern PACKAGE TDIUnicodeReadMethods Read_CP_857;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_857;
extern PACKAGE TDIUnicodeReadMethods Read_CP_860;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_860;
extern PACKAGE TDIUnicodeReadMethods Read_CP_861;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_861;
extern PACKAGE TDIUnicodeReadMethods Read_CP_863;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_863;
extern PACKAGE TDIUnicodeReadMethods Read_CP_864;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_864;
extern PACKAGE TDIUnicodeReadMethods Read_CP_865;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_865;
extern PACKAGE TDIUnicodeReadMethods Read_CP_869;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_869;
extern PACKAGE TDIUnicodeReadMethods Read_CP_874;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_874;
extern PACKAGE TDIUnicodeReadMethods Read_CP_1006;
extern PACKAGE TDIUnicodeWriteMethods Write_CP_1006;
extern PACKAGE TDIUnicodeReadMethods Read_NextStep;
extern PACKAGE TDIUnicodeWriteMethods Write_NextStep;
extern PACKAGE TDIUnicodeReadMethods Read_US_ASCII;
extern PACKAGE TDIUnicodeWriteMethods Write_US_ASCII;
extern PACKAGE TDIUnicodeReadMethods Read_ISO_8859_1;
extern PACKAGE TDIUnicodeWriteMethods Write_ISO_8859_1;
extern PACKAGE TDIUnicodeReadMethods Read_JIS_X0201;
extern PACKAGE TDIUnicodeWriteMethods Write_JIS_X0201;
extern PACKAGE TDIUnicodeReadMethods Read_GB_18030;
extern PACKAGE TDIUnicodeReadMethods Read_TIS_620;
extern PACKAGE TDIUnicodeWriteMethods Write_TIS_620;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_2_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_2_BE;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_2_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_2_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_4_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_4_BE;
extern PACKAGE TDIUnicodeReadMethods Read_UCS_4_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UCS_4_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_7;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_7;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_7_ODC;
extern PACKAGE TDIUnicodeReadMethods Read_utf_8;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_8;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_16_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_16_BE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_16_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_16_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_32_LE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_32_LE;
extern PACKAGE TDIUnicodeReadMethods Read_UTF_32_BE;
extern PACKAGE TDIUnicodeWriteMethods Write_UTF_32_BE;
extern PACKAGE bool __fastcall Read_Iso_8859_2_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_2_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_3_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_3_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_4_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_4_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_5_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_5_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_6_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_6_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_7_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_7_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_8_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_8_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_9_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_9_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_10_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_10_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_13_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_13_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_14_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_14_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_15_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_15_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Iso_8859_16_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_16_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Koi8_R_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Koi8_R_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Koi8_RU_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Koi8_RU_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Koi8_U_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Koi8_U_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1250_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1250_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1251_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1251_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1252_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1252_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1253_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1253_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1254_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1254_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1255_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1255_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1256_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1256_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1257_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1257_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Windows_1258_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Windows_1258_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_850_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_850_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_862_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_862_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_866_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_866_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Roman_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Roman_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_CentralEurope_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_CentralEurope_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Dingbats_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Dingbats_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Iceland_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Iceland_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Croatian_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Croatian_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Romania_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Romania_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Cyrillic_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Cyrillic_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Greek_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Greek_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Turkish_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Turkish_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Hebrew_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Hebrew_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Arabic_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Arabic_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Thai_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Thai_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_Mac_Farsi_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Mac_Farsi_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_424_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_424_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_437_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_437_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_737_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_737_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_775_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_775_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_852_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_852_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_855_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_855_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_856_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_856_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_857_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_857_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_860_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_860_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_861_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_861_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_863_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_863_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_864_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_864_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_865_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_865_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_869_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_869_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_874_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_874_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_CP_1006_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_CP_1006_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_NextStep_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_NextStep_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_US_ASCII_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_US_ASCII_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar Char);
extern PACKAGE bool __fastcall Read_Iso_8859_1_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_Iso_8859_1_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar Char);
extern PACKAGE bool __fastcall Read_JIS_X0201_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_JIS_X0201_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar Char);
extern PACKAGE bool __fastcall Read_GB_18030_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Read_TIS_620_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_TIS_620_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UCS_2_BE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UCS_2_BE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UCS_2_LE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UCS_2_LE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UCS_4_BE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UCS_4_BE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UCS_4_LE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UCS_4_LE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE void __fastcall Read_UTF_7_Init(const TDIUnicodeReader* UnicodeReader);
extern PACKAGE void __fastcall Read_UTF_7_Finalize(const TDIUnicodeReader* UnicodeReader);
extern PACKAGE bool __fastcall Read_UTF_7_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE void __fastcall Write_UTF_7_Init(const TDIUnicodeWriter* UnicodeWriter);
extern PACKAGE void __fastcall Write_UTF_7_Flush(const TDIUnicodeWriter* UnicodeWriter);
extern PACKAGE void __fastcall Write_UTF_7_Finalize(const TDIUnicodeWriter* UnicodeWriter);
extern PACKAGE bool __fastcall Write_UTF_7_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Write_UTF_7_ODC_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UTF_8_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UTF_8_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UTF_16_BE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UTF_16_BE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UTF_16_LE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UTF_16_LE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UTF_32_BE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UTF_32_LE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE bool __fastcall Read_UTF_32_LE_ReadChar(const TDIUnicodeReader* UnicodeReader, /* out */ System::WideChar &c);
extern PACKAGE bool __fastcall Write_UTF_32_BE_WriteChar(const TDIUnicodeWriter* UnicodeWriter, const System::WideChar c);
extern PACKAGE unsigned __fastcall CodePageFromLocale(const unsigned Language);

}	/* namespace Diunicode */
using namespace Diunicode;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiunicodeHPP
