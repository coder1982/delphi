unit untGetProc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, untGetprocCommon, ImgList;

type
  TfrmGetProc = class(TForm)
    PageControl: TPageControl;
    tsProgram: TTabSheet;
    tsProcess: TTabSheet;
    lvProgram: TListView;
    Button1: TButton;
    lvProcess: TListView;
    btnRefresh: TButton;
    ImageList: TImageList;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtAddress: TEdit;
    cbbType: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    edtValue: TEdit;
    Label4: TLabel;
    edtWrite: TEdit;
    btnWrite: TButton;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    edtName: TEdit;
    edtProcId: TEdit;
    Label7: TLabel;
    edtMaxRange: TEdit;
    GroupBox3: TGroupBox;
    edtSechValue: TEdit;
    Label8: TLabel;
    btnSechBegin: TButton;
    lblSechResult: TLabel;
    btnClearSech: TButton;
    lvSechResult: TListView;
    lblTime: TLabel;
    btnSelTarget: TButton;
    GroupBox4: TGroupBox;
    lvFoundAddress: TListView;
    GroupBox5: TGroupBox;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    edtDataValue: TEdit;
    Label13: TLabel;
    cbxDataType: TComboBox;
    edtDataAdd: TEdit;
    ckbDataLock: TCheckBox;
    btnAddNew: TButton;
    cbxAddressName: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure lvProcessData(Sender: TObject; Item: TListItem);
    procedure FormDestroy(Sender: TObject);
    procedure lvProgramData(Sender: TObject; Item: TListItem);
    procedure lvProgramDrawItem(Sender: TCustomListView; Item: TListItem;
      Rect: TRect; State: TOwnerDrawState);
    procedure btnRefreshClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure lvProgramSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure cbbTypeChange(Sender: TObject);
    procedure edtAddressKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnWriteClick(Sender: TObject);
    procedure btnSechBeginClick(Sender: TObject);
    procedure btnClearSechClick(Sender: TObject);
    procedure lvSechResultData(Sender: TObject; Item: TListItem);
    procedure lvSechResultSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure btnSelTargetClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lvFoundAddressData(Sender: TObject; Item: TListItem);
    procedure btnAddNewClick(Sender: TObject);
    procedure edtDataAddKeyPress(Sender: TObject; var Key: Char);
    procedure edtDataValueKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    u_DType   : TDataType;
    u_Buf     : TRWBuffer;
    u_FirstSearch: Boolean;
    u_SechList: TList;
    u_FoundAdds: TList;
    u_Count : Integer;
    u_SearchCount: Integer;
    //procedure GetIcons;
    procedure FreeLists;
    //进行第一次搜索
    procedure FirstSearch(AValue: DWord; AProcMem: TProcessMemory;
      var AList: TList);
    //进行第n次搜索,n>1
    procedure SearchAgain(Avalue: DWord; AProcMem: TProcessMemory;
      var AList: TList);

    procedure WmSechFind(var Msg: TMessage); message WM_SECHFIND;
    //设置lvSechResult中四列的宽度
    procedure SetColumnWidthOfSR;
    //选择lvSechResult中的某一项Item时
    procedure SelectAddressOfSR(AId: Integer);
    //判断地址是否在Found列表已经存在,如果存在则返回true,并将Id号返回到AId中
    function IsAddressExists(Adds: DWord; var AId: Integer): Boolean; overload;
    function IsAddressExists(Adds: DWord): Boolean; overload;
    //将指定的地址及其信息添加到Found列表中
    procedure AddRessAddToFoundList(Adds: DWord);
    //刷新Found列表
    procedure RefreshFoundList;
    //将指定的数据写入指定地址
    procedure WriteValueToAdds(AValue: DWord; Adds: DWord);
  public
    { Public declarations }
    u_ProcList: TList;
    u_ProgList: TList;
    u_ProcMem : TProcessMemory;
  end;

var
  frmGetProc: TfrmGetProc;

implementation
  uses untSelectTarget;

{$R *.DFM}

procedure TfrmGetProc.FormCreate(Sender: TObject);
begin
  u_ProcMem := TProcessMemory.Create;
  u_SechList := TList.Create;
  u_FoundAdds := TList.Create;
  //u_ProcList := GetSysProcess;
  //u_ProgList := GetRunProgram;
  cbbType.ItemIndex := 0;
  cbxDataType.ItemIndex := 0;

  //GetIcons;
  //lvProcess.Items.Count := u_ProcList.Count;
  //lvProgram.Items.Count := u_ProgList.Count;

  //lvProcess.Columns[0].Width := lvProcess.ClientWidth;
  //lvProgram.Columns[0].Width := lvProgram.ClientWidth;

  btnClearSechClick(Sender);
end;

procedure TfrmGetProc.lvProcessData(Sender: TObject; Item: TListItem);
var
  nId: Integer;
begin
  nId := Item.Index;
  Item.Caption := pProcessInfo(u_ProcList.Items[nId]).ExeFile;
end;

procedure TfrmGetProc.FormDestroy(Sender: TObject);
begin
  FreeLists;
  FreeAndNil(u_ProcMem);
  FreeAndNil(u_SechList);
end;

procedure TfrmGetProc.lvProgramData(Sender: TObject; Item: TListItem);
var
  nId: Integer;
begin
  nId := Item.Index;
  Item.Caption := pProgramInfo(u_ProgList.Items[nId]).WndCaption;
  Item.ImageIndex := nId;
end;

procedure TfrmGetProc.lvProgramDrawItem(Sender: TCustomListView;
  Item: TListItem; Rect: TRect; State: TOwnerDrawState);
var
  hWnd, hIcon: THandle;
  icon: TIcon;
  nId: Integer;
begin
  nId := Item.Index;
  hWnd := pProgramInfo(u_ProgList.Items[nId]).HdlWnd;
  hIcon := GetClassLong(hWnd, GCL_HICONSM);
  icon := TIcon.Create;
  icon.Handle := hIcon;
  lvProgram.Canvas.Draw(Rect.Left, Rect.Top, icon);
  icon.Free;
end;

{procedure TfrmGetProc.GetIcons;
var
  i: Integer;
  hWnd, hIcon: THandle;
  icon: TIcon;
begin
  ImageList.Clear;
  for i:= 0 to u_ProgList.Count - 1 do
  begin
    hWnd := pProgramInfo(u_ProgList.Items[i]).HdlWnd;
    hIcon := GetClassLong(hWnd, GCL_HICONSM);
    icon := TIcon.Create;
    icon.Handle := hIcon;
    ImageList.AddIcon(icon);
    icon.Free;
  end;
end;}

procedure TfrmGetProc.btnRefreshClick(Sender: TObject);
begin
  FreeLists;

  lvProcess.Items.Count := 0;
  lvProgram.Items.Count := 0;

  u_ProcList := GetSysProcess;
  u_ProgList := GetRunProgram;

  lvProcess.Items.Count := u_ProcList.Count;
  lvProgram.Items.Count := u_ProgList.Count;
end;

procedure TfrmGetProc.FreeLists;
var
  pProcInfo: pProcessInfo;
  pProg : pProgramInfo;
  pAddInfo: pFoundAddInfo;
  i: Integer;
begin
  for i:= u_ProcList.Count - 1 downto 0 do
  begin
    pProcInfo := u_ProcList.Items[i];
    Dispose(pProcInfo);
    u_ProcList.Delete(i);
  end;
  FreeAndNil(u_ProcList);

  for i := u_ProgList.Count - 1 downto 0 do
  begin
    pProg := u_ProgList.Items[i];
    Dispose(pProg);
    u_ProgList.Delete(i);
  end;
  FreeAndNil(u_ProgList);

  for i:= u_SechList.Count - 1 downto 0 do
    u_SechList.Delete(i);

  for i:= u_FoundAdds.Count - 1 downto 0 do
  begin
    pAddInfo := u_FoundAdds.Items[i];
    Dispose(pAddInfo);
    u_FoundAdds.Delete(i);
  end;
  FreeAndNil(u_FoundAdds);

  FreeAndNil(u_SechList);
end;

procedure TfrmGetProc.Button1Click(Sender: TObject);
var
  i, nProgId: Integer;
  dwProcid: DWord;
begin
  //nProgId := 0;
  //dwProcid := 0;
  if lvProgram.SelCount > 0 then
  begin
    nProgId := lvProgram.Selected.Index;
    dwProcId := pProgramInfo(u_ProgList.Items[nProgId]).ProcessId;
    for i:= 0 to u_ProcList.Count - 1 do
      if pProcessInfo(u_ProcList.Items[i]).ProcessId = dwProcId then
      begin
        PageControl.ActivePageIndex := 1;
        lvProcess.Items[i].Selected := True;
        Break;
      end;
  end;
end;

procedure TfrmGetProc.lvProgramSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  nId: Integer;
begin
  //nId := 0;
  if Selected then
  begin
    nId := Item.Index;
    edtName.Text := pProgramInfo(u_ProgList.Items[nId]).WndCaption;
    edtProcId.Text := IntToHex(pProgramInfo(u_ProgList.Items[nId]).ProcessId, 8);
    u_ProcMem.ProcId := pProgramInfo(u_ProgList.Items[nId]).ProcessId;
    edtMaxRange.Text := IntToHex(GetMaxRangeOfProc(u_ProcMem), 8);
  end;
end;

procedure TfrmGetProc.cbbTypeChange(Sender: TObject);
begin
  Case cbbType.ItemIndex of
    0: u_DType := dtByte;
    1: u_DType := dtWord;
    2: u_DType := dtDWord;
    3: u_DType := dtSingle;
    4: u_DType := dtDouble;
  end;
end;

procedure TfrmGetProc.edtAddressKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nValue: DWord;
  nDbValue: Double;
begin
  nValue := 0;
  if Key = 13 then
  begin
    if ReadAddress(StrToInt('$' + edtAddress.Text), u_DType, u_ProcMem, u_Buf) then
    begin
      Case u_DType of
        dtByte   : nValue := Byte(pByte(@u_Buf)^);
        dtWord   : nValue := Word(pWord(@u_Buf)^);
        dtDWord  : nValue := DWord(pDWord(@u_Buf)^);
        dtSingle : nDbValue := Single(pSingle(@u_Buf)^);
        dtDouble : nDbValue := Double(pDouble(@u_Buf)^);
      end;
      edtValue.Text := IntToStr(nValue);
    end;
  end;
end;

procedure TfrmGetProc.btnWriteClick(Sender: TObject);
var
  mBuf: TRWBuffer;
  nValue: DWord;
begin
  nValue := StrToInt(edtWrite.Text);
  Case u_DType of
    dtByte  : Byte(pByte(@mBuf)^) := nValue;
    dtWord  : Word(pWord(@mBuf)^) := nValue;
    dtDWord : DWord(pDWord(@mBuf)^) := nValue;
  end;
  WriteAddress(StrToInt('$' + edtAddress.Text), u_DType, u_ProcMem, mBuf);
end;

procedure TfrmGetProc.btnSechBeginClick(Sender: TObject);
var
  tS1, tS2: DWord;
begin
  u_Count := 0;
  Inc(u_SearchCount);
  if u_FirstSearch then
  begin
    u_SechList.Clear;
    tS1 := GetTickCount;
    FirstSearch(StrToInt(edtSechValue.Text), u_ProcMem, u_SechList);
    tS2 := GetTickCount;

    if u_FirstSearch then
      u_FirstSearch := False;
  end
  else
  begin
    tS1 := GetTickCount;
    SearchAgain(StrToInt(edtSechValue.Text), u_ProcMem, u_SechList);
    tS2 := GetTickCount;
  end;
  u_Count := u_SechList.Count;
  lvSechResult.Items.Count := u_Count;
  lblSechResult.Caption := Format(Str_SechResult, [u_SearchCount, u_Count, tS2-tS1]);
  SetColumnWidthOfSR;
end;

procedure TfrmGetProc.btnClearSechClick(Sender: TObject);
begin
  u_FirstSearch := True;
  u_SearchCount := 0;
  u_SechList.Clear;
  lblSechResult.Caption := Format(Str_SechResult, [0, 0, 0]);
  lvSechResult.Items.Count := 0;
  lvSechResult.Refresh;
end;

procedure TfrmGetProc.FirstSearch(AValue: DWord; AProcMem: TProcessMemory;
  var AList: TList);
var
  mReadBuf: TReadBlock;
  mBuf: TRWBuffer;
  //mDt: TDataType;
  //nLength: Integer; 2003-02-25被注释掉 Liujh
begin
  if (not Assigned(AProcMem)) or (not Assigned(AList)) or (AProcMem.ProcId = 0) then
    Exit;

  AList.Clear;

  AProcMem.Position := Addr_Begin;
  //mDt := GetSechValueDT(AValue);
  //nLength := GetRWLength(mDt); 2003-02-25被注释掉 Liujh
  TransDataToRWBuffer(AValue, mBuf);

  While AProcMem.Position <= StrToInt('$' + edtMaxRange.Text) do
  begin
    Application.ProcessMessages;
    AProcMem.Read(mReadBuf, $1000);
    ScanBlockMem(AValue, AProcMem.Position, @mReadBuf, $1000, AList);
    {if AProcMem.Read(mReadBuf, nLength) = nLength then
    begin
      nFailCount := 0;
      if CompareMem(@mReadBuf, @mBuf, nLength) then
      begin
        AList.Add(Pointer(AProcMem.Position));
        PostMessage(Handle, WM_SECHFIND, 0, 0);
      end;
    end
    else
      Inc(nFailCount);}
    //if (nFailCount > 512 / nLength) or (AProcMem.Position > $006BB000) then
      //Break;
    AProcMem.Position := AProcMem.Position + $1000;//nLength;
  end;
end;

procedure TfrmGetProc.WmSechFind(var Msg: TMessage);
begin
  Inc(u_Count);
  lvSechResult.Items.Count := u_Count;
  //lvSech
  lblSechResult.Caption := Format(Str_SechResult, [u_Count]);
  lblSechResult.Refresh;
end;

procedure TfrmGetProc.lvSechResultData(Sender: TObject; Item: TListItem);
begin
  Item.Caption := IntToHex(DWord(u_SechList.Items[Item.Index]), 8);

  Item.SubItems.Add(
    IntToStr(ReadAddrData(DWord(u_SechList.Items[Item.Index]), u_ProcMem, dtByte)));

  Item.SubItems.Add(
    IntToStr(ReadAddrData(DWord(u_SechList.Items[Item.Index]), u_ProcMem, dtWord)));

  Item.SubItems.Add(
    IntToStr(ReadAddrData(DWord(u_SechList.Items[Item.Index]), u_ProcMem, dtDWord)));
end;

procedure TfrmGetProc.SearchAgain(Avalue: DWord; AProcMem: TProcessMemory;
  var AList: TList);
var
  mList: TList;
  i, nLength: Integer;
  mDt: TDataType;
  mBuf, mReadBuf: TRWBuffer;
begin
  if (not Assigned(AProcMem)) or (not Assigned(AList)) or (AProcMem.ProcId = 0) then
    Exit;
  lvSechResult.Items.Count := 0;
  mList := TList.Create;
  mList.Clear;
  for i:= 0 to AList.Count - 1 do
    mList.Add(AList.Items[i]);
  AList.Clear;

  mDt:= GetSechValueDT(AValue);
  nLength := GetRWLength(mDt);
  //AProcMem.Position := DWord(mList.Items[0]);
  TransDataToRWBuffer(AValue, mBuf);

  for i:= 0 to mList.Count - 1 do
  begin
    Application.ProcessMessages;
    AProcMem.Position := DWord( mList.Items[i]);
    AProcMem.Read(mReadBuf, nLength);
    if CompareMem(@mReadBuf, @mBuf, nLength) then
      AList.Add(mList.Items[i]);
  end;

  lvSechResult.Items.Count := AList.Count;
  for i:= mList.Count - 1 downto 0 do
    mList.Delete(i);
  FreeAndNil(mList);
end;

procedure TfrmGetProc.lvSechResultSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  if Selected then
  begin
    edtAddress.Text := Item.Caption;
    edtValue.Text := IntToStr(ReadAddrData(StrToInt('$' + Item.Caption),
      u_ProcMem, u_DType));

    SelectAddressOfSR(Item.Index);
  end;
end;

procedure TfrmGetProc.btnSelTargetClick(Sender: TObject);
var
  nId: Integer;
  nUseTime: DWord;
begin
  if not Assigned(frmTargetProc) then
    frmTargetProc := TfrmTargetProc.Create(Application);

  frmTargetProc.ShowModal;
  if frmTargetProc.ModalResult = mrOk then
    if frmTargetProc.lvProgram.SelCount > 0 then
    begin
      nId := frmTargetProc.lvProgram.Selected.Index;
      edtName.Text := pProgramInfo(u_ProgList.Items[nId]).WndCaption;
      edtProcId.Text := IntToHex(pProgramInfo(u_ProgList.Items[nId]).ProcessId, 8);
      u_ProcMem.ProcId := pProgramInfo(u_ProgList.Items[nId]).ProcessId;
      //edtMaxRange.Text := IntToHex(GetMaxRangeOfProc(u_ProcMem), 8);
      edtMaxRange.Text := IntToHex(GetMaxRangeOfProc(u_ProcMem, nUseTime), 8);
      ShowMessage(IntToStr(nUseTime));
    end;

  FreeAndNil(frmTargetProc);
end;

procedure TfrmGetProc.FormShow(Sender: TObject);
begin
  SetColumnWidthOfSR;
end;

procedure TfrmGetProc.SetColumnWidthOfSR;
var
  i, nHScroll: Integer;
begin
  nHScroll := GetSystemMetrics(SM_CXVSCROLL);

  lvSechResult.Columns[0].Width := 70;
  lvSechResult.Columns[0].MaxWidth := 70;
  lvSechResult.Columns[0].MinWidth := 70;

  for i:= 1 to 3 do
  begin
    lvSechResult.Columns[i].Width := Trunc((lvSechResult.Width - nHScroll -74)/3);
    lvSechResult.Columns[i].MaxWidth := Trunc((lvSechResult.Width - nHScroll -74)/3);
    lvSechResult.Columns[i].MinWidth := Trunc((lvSechResult.Width - nHScroll -74)/3);
  end;
end;

procedure TfrmGetProc.SelectAddressOfSR(AId: Integer);
var
  mdt: TDataType;
begin
  edtDataAdd.Text := lvSechResult.Items[AId].Caption;
  case cbxDataType.ItemIndex of
    0: mdt := dtByte;
    1: mdt := dtWord;
    2: mdt := dtDWord;
  end;
  //edtDataValue.Text := lvSechResult.Items[AId].SubItems[cbxDataType.ItemIndex];
  edtDataValue.Text := IntToStr(ReadAddrData(StrToInt('$' + edtDataAdd.Text),
      u_ProcMem, mdt));
end;

function TfrmGetProc.IsAddRessExists(Adds: DWord; var AId: Integer): Boolean;
var
  pAddInfo: pFoundAddInfo;
  i: Integer;
begin
  Result := False;
  for i:= 0 to u_FoundAdds.Count - 1 do
  begin
    pAddInfo := u_FoundAdds.Items[i];
    if pAddInfo.Address = Adds then
    begin
      Result := True;
      AId := i;
      Break;
    end;
  end;
end;

procedure TfrmGetProc.AddRessAddToFoundList(Adds: DWord);
var
  pAddInfo: pFoundAddInfo;
begin
  if not IsAddressExists(Adds) then
  begin
    New(pAddInfo);
    pAddInfo.Address := Adds;
    pAddInfo.DType := TDataType(cbxDataType.ItemIndex);
    pAddInfo.Name := cbxAddressName.Text;
    pAddInfo.Value := StrToInt(edtDataValue.Text);
    pAddInfo.Locked := ckbDataLock.Checked;
    u_FoundAdds.Add(pAddInfo);

    WriteValueToAdds(StrToInt(edtDataValue.Text),
      StrToInt('$' + edtDataAdd.Text));
  end;
end;

function TfrmGetProc.IsAddressExists(Adds: DWord): Boolean;
var
  pAddInfo: pFoundAddInfo;
  i: Integer;
begin
  Result := False;
  for i:= 0 to u_FoundAdds.Count - 1 do
  begin
    pAddInfo := u_FoundAdds.Items[i];
    if pAddInfo.Address = Adds then
    begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TfrmGetProc.RefreshFoundList;
begin
  lvFoundAddress.Items.Count := 0;
  lvFoundAddress.Items.Count := u_FoundAdds.Count;
end;

procedure TfrmGetProc.lvFoundAddressData(Sender: TObject; Item: TListItem);
var
  pAddInfo: pFoundAddInfo;
begin
  pAddInfo := u_FoundAdds.Items[Item.Index];
  Item.Caption := IntToHex(pAddInfo.Address, 8);
  Item.SubItems.Add(pAddInfo.Name);
  Item.SubItems.Add(IntToStr(pAddInfo.Value));
end;

procedure TfrmGetProc.btnAddNewClick(Sender: TObject);
begin
  AddressAddToFoundList(StrToInt('$' + edtDataAdd.Text));
  RefreshFoundList;
end;

procedure TfrmGetProc.edtDataAddKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9', Char(VK_BACK), Char(VK_RETURN)]) then
    Key := #0;
end;

procedure TfrmGetProc.edtDataValueKeyPress(Sender: TObject; var Key: Char);
var
  nItemId: Integer;
  pAddInfo: pFoundAddInfo;
begin
  if not (Key in ['0'..'9', Char(VK_BACK), Char(VK_RETURN)]) then
    Key := #0
  else
    if (Key = Char(VK_RETURN)) and IsAddressExists(StrToInt('$' + edtDataAdd.Text), nItemId) then
    begin
      pAddInfo := u_FoundAdds.Items[nItemId];
      WriteValueToAdds(StrToInt(edtDataValue.Text),
        StrToInt('$' + edtDataAdd.Text));
      pAddInfo.Value := StrToInt(edtDataValue.Text);
      lvFoundAddress.UpdateItems(nItemId, nItemId);
    end;
end;

procedure TfrmGetProc.WriteValueToAdds(AValue, Adds: DWord);
var
  mBuf: TRWBuffer;
  //nValue: DWord;
begin
  //nValue := StrToInt(edtWrite.Text);
  Case TDataType(cbxDataType.ItemIndex) of
    dtByte  : Byte(pByte(@mBuf)^) := AValue;
    dtWord  : Word(pWord(@mBuf)^) := AValue;
    dtDWord : DWord(pDWord(@mBuf)^) := AValue;
  end;
  WriteAddress(Adds, TDataType(cbxDataType.ItemIndex), u_ProcMem, mBuf);
end;

end.
