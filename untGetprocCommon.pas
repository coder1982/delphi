unit untGetprocCommon;

interface

uses
  Windows, classes, TLHelp32, Sysutils, Messages;

const
  Addr_Begin = $00400000;
  Addr_End   = $7FFFFFFF;

  WM_SECHFIND = WM_USER + $001;

resourcestring
  Str_SechResult = '第%d次查找，共找到%d个地址，用去时间%d毫秒';
  Str_SechOver   = '查找结束，找到%d个结果';
  Str_Time       = '用去时间%d毫秒';

type
  TProcessInfo = packed Record
    ExeFile   : string;
    ProcessId : DWord;
  end;

  TProgramInfo = packed Record
    WndName    : string;
    ClsName    : string;
    WndCaption : string;
    HdlWnd     : THandle;
    ProcessId  : DWord;
  end;

  TDataType = (dtByte, dtWord, dtDWord, dtSingle, dtDouble);

  TFoundAddInfo = packed Record
    Address    : DWord;
    Name       : string;
    Value      : Integer;
    DType      : TDataType;
    Locked     : Boolean;
  end;

  pFoundAddInfo = ^TFoundAddInfo;

  pProcessInfo = ^TProcessInfo;
  pProgramInfo = ^TProgramInfo;

  TProcessMemory = class(TMemoryStream)
  private
    FProcId: DWord;
    FSize, FPosition: LongInt;
    procedure SetProcId(const Value: DWord);
  public
    property ProcId: DWord Read FProcId Write SetProcId;
    function Read(var Buffer; Count: LongInt): LongInt; override;
    function Write(const Buffer; Count: LongInt): LongInt; override;
    function Seek(Offset: Longint; Origin: Word): LongInt; override;
  end;

  TRWBuffer = array[0..7] of Byte;
  TReadBlock = array[0..$FFF] of Byte;
  pReadBlock = ^TReadBlock;

//获得系统进程信息
function GetSysProcess: TList;

//获得正在运行的应用程序
function GetRunProgram: TList;

//读取指定的地址
function ReadAddress(DWAddr: DWord; AType: TDataType;
  AProcMem: TProcessMemory; var ABuf: TRWBuffer): Boolean;

//向指定的地址写入指定的数
function WriteAddress(DWAddr: DWord; AType: TDataType;
  AProcMem: TProcessMemory; ABuf: TRWBuffer): Boolean;

//进程是否处于激活
function IsProcessActive(AProcId: DWord): Boolean;

//得到搜索数据的数据类型,其中DT是DataType的缩写
function GetSechValueDT(AValue: Integer): TDataType;

function GetRWLength(AType: TDataType): Integer;

procedure TransDataToRWBuffer(AValue: DWord; var ABuf: TRWBuffer);

//在一个内存区域内查找一个数
procedure ScanBlockMem(AValue: DWord; MemAddr: DWord; Addr: Pointer; ALength: Integer; var AList: TList);

//确定要搜索的最大范围
function GetMaxRangeOfProc(AProcMem: TProcessMemory): DWord; overload;
function GetMaxRangeOfProc(AProcMem: TProcessMemory; var AUseTime: Dword): DWord; overload;

//读出指定地址中指定类型的数
function ReadAddrData(DWAddr: DWord; AProcMem: TProcessMemory; ADType: TDataType): DWord;

implementation

function GetSysProcess: TList;
var
  mList: TList;
  hSnapShot: THandle;
  mProcessEntry32: TProcessEntry32;
  pProcInfo: pProcessInfo;
  bFound: Boolean;
begin
  Result := nil;
  mList := TList.Create;
  mList.Clear;
  hSnapShot := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
  mProcessEntry32.dwSize := Sizeof(mProcessEntry32);
  bFound := Process32First(hSnapShot, mProcessEntry32);
  while bFound do
  begin
    New(pProcInfo);
    pProcInfo.ExeFile := mProcessEntry32.szExeFile;
    pProcInfo.ProcessId := mProcessEntry32.th32ProcessID;
    mList.Add(pProcInfo);
    bFound := Process32Next(hSnapShot, mProcessEntry32);
  end;
  Result := mList;
end;

function GetRunProgram: TList;
var
  mList: TList;
  pProg: pProgramInfo;
  hFound: THandle;
  aryCaption: array[0..255] of Char;
  nProcId: DWord;
begin
  Result := nil;
  mList := TList.Create;
  mList.Clear;

  FillChar(aryCaption, 256, #0);
  hFound := FindWindowEx(0, 0, nil, nil);
  while hFound <> 0 do
  begin
    if GetWindow(hFound, GW_OWNER) = 0 then
      if IsWindowVisible(hFound) then
        if GetWindowText(hFound, aryCaption, 255) > 0 then
          if (AnsiCompareText(StrPas(aryCaption), 'Program Manager') <> 0) and
            (AnsiCompareText(StrPas(aryCaption), 'ProcessInfo') <> 0) then
          begin
            New(pProg);
            pProg.HdlWnd := hFound;//窗口句柄

            FillChar(aryCaption, 256, #0);
            GetWindowText(hFound, aryCaption, 255);
            pProg.WndCaption := StrPas(aryCaption);//窗口caption

            FillChar(aryCaption, 256, #0);
            GetClassName(hFound, aryCaption, 256);
            pProg.ClsName := StrPas(aryCaption);//窗口类名

            nProcId := 0;
            GetWindowThreadProcessId(hFound, @nProcId);
            pProg.ProcessId := nProcId;//窗口进程Id

            mList.Add(pProg);
          end;
    hFound := FindWindowEx(0, hFound, nil, nil);
  end;
  Result := mList;
end;

{ TProcessMemory }

function TProcessMemory.Read(var Buffer; Count: Integer): LongInt;
var
  hProcess: THandle;
  nCount: DWord;
begin
  nCount := 0;
  hProcess := OpenProcess(PROCESS_VM_READ, True, FProcId);
  ReadProcessMemory(hProcess, Pointer(Position), @Buffer, Count, nCount);
  Result := nCount;
  CloseHandle(hProcess);
end;

function TProcessMemory.Seek(Offset: Integer; Origin: Word): LongInt;
begin
  case Origin of
    soFromBeginning: FPosition := Offset;
    soFromCurrent: Inc(FPosition, Offset);
    soFromEnd: FPosition := FSize + Offset;
  end;
  Result := FPosition;
end;

procedure TProcessMemory.SetProcId(const Value: DWord);
begin
  FProcId := Value;
end;

function TProcessMemory.Write(const Buffer; Count: Integer): LongInt;
var
  hProcess, hProcInfo: THandle;
  nCount: DWord;
  Buf: _MEMORY_BASIC_INFORMATION;
begin
  hProcess := OpenProcess(PROCESS_ALL_ACCESS, True, FProcId);
  hProcInfo := OpenProcess(PROCESS_QUERY_INFORMATION, True, FProcId);
  VirtualQueryEx(hProcInfo, Pointer(Position), Buf, Sizeof(Buf));

  if Buf.Protect in [PAGE_READWRITE, PAGE_WRITECOPY, PAGE_EXECUTE_READWRITE,
                     PAGE_EXECUTE_WRITECOPY] then
  begin
    WriteProcessMemory(hProcess, Pointer(Position), @Buffer, Count, nCount);
    Result:= nCount;
  end
  else Result:= -1;

  CloseHandle(hProcess);
  CloseHandle(hProcInfo);
end;

function GetRWLength(AType: TDataType): Integer;
begin
  Result := 0;
  Case AType of
    dtByte   : Result := 1;
    dtWord   : Result := 2;
    dtDWord  : Result := 4;
    dtSingle : Result := 4;
    dtDouble : Result := 8;
  end;
end;

function ReadAddress(DWAddr: DWord; AType: TDataType;
  AProcMem: TProcessMemory; var ABuf: TRWBuffer): Boolean;
var
  nLength: Integer;
begin
  Result := False;
  if not Assigned(AProcMem) then
   Exit;
  nLength := GetRWLength(AType);
  AProcMem.Position := DWAddr;
  if AProcMem.Read(ABuf, nLength) = nLength then
    Result := True;
end;

function WriteAddress(DWAddr: DWord; AType: TDataType;
  AProcMem: TProcessMemory; ABuf: TRWBuffer): Boolean;
var
  nLength: Integer;
begin
  Result := False;
  if not Assigned(AProcMem) then
    Exit;
  nLength := GetRWLength(AType);
  AProcMem.Position := DWAddr;
  if IsProcessActive(AProcMem.ProcId) then
    AProcMem.Write(ABuf, nLength)// = nLength
  else
    CopyMemory(Pointer(DWAddr), @ABuf, nLength);
    //Result := True;
end;

function IsProcessActive(AProcId: DWord): Boolean;
var
  hProcess: THandle;
  mExitCode: DWord;
begin
  hProcess := OpenProcess(PROCESS_ALL_ACCESS, True, AProcId);
  Result := GetExitCodeProcess(hProcess, mExitCode);
  Result := Result and (mExitCode = STILL_ACTIVE);
  CloseHandle(hProcess);
end;

function GetSechValueDT(AValue: Integer): TDataType;
begin
  Result:= dtByte;
  if (AValue < 256) and (AValue >= 0) then
    Result := dtByte
  else
    if (AValue < 65536) and (AValue > 0) then
      Result := dtWord
      else
        if AValue > 0 then
          Result := dtDWord;
end;

procedure TransDataToRWBuffer(AValue: DWord; var ABuf: TRWBuffer);
var
  mDt: TDataType;
begin
  FillChar(ABuf, 8, #0);
  mDt := GetSechValueDT(AValue);
  Case mDt of
    dtByte  : Byte(pByte(@ABuf)^) := AValue;
    dtWord  : Word(pWord(@ABuf)^) := AValue;
    dtDWord : DWord(pDWord(@ABuf)^) := AValue;
  end;
end;

procedure ScanBlockMem(AValue: DWord; MemAddr: DWord; Addr: Pointer; ALength: Integer; var AList: TList);
var
  i, nLength, nTempValue: Integer;
  mDt: TDataType;
begin
  mDt := GetSechValueDT(AValue);
  nLength := GetRWLength(mDt);

  if not Assigned(AList) then
    Exit;

  i:= 0;
  while i <= ALength - 1 do
  begin
    Case nLength of
      1: nTempvalue := Byte(pByte(Addr)^);// p^);
      2: nTempvalue := Word(pWord(Addr)^);// p^);
      4: nTempvalue := DWord(pDWord(Addr)^);// p^);
    end;
    if nTempValue = AValue then
      AList.Add(Pointer(MemAddr + i));
    Inc(i, 1);
    DWord(Addr) := DWord(Addr) + 1;
  end;
end;

function GetMaxRangeOfProc(AProcMem: TProcessMemory): DWord;
const
  ChcekCount = 32;
  StepLength = $1000;
var
  i, nFailCount: Integer;
  mBuf: Byte;
begin
  Result := $00800000;
  if (not Assigned(AProcMem)) or (AProcMem.ProcId = 0) then
    Exit;

  AProcMem.Position := Addr_Begin;
  While AProcMem.Position < Addr_End do
  begin
    if AProcMem.Read(mBuf, 1) = 1 then
    begin
      nFailCount := 0;
      AProcMem.Position := AProcMem.Position + StepLength;
    end
    else
    begin
      Inc(nFailCount);
      AProcMem.Position := AProcMem.Position + 1;
      if nFailCount > 32 then
      begin
        Result := AProcMem.Position;
        Break;
      end;
    end;
  end;
end;

function GetMaxRangeOfProc(AProcMem: TProcessMemory; var AUseTime: Dword): DWord;
const
  StepLength = $10000;
var
  nTBegin: DWord;
  mBuf: Byte;
begin
  Result := $00800000;
  if (not Assigned(AProcMem)) or (AProcMem.ProcId = 0) then
    Exit;
  AProcMem.Position := 0;//Addr_Begin;
  nTBegin := GetTickCount;
  While AProcMem.Position <= $2FFEFFFF do
  begin
    if AProcMem.Read(mbuf, 1) = 1 then
      Result := AProcMem.Position;
    AProcMem.Position := AProcMem.Position + StepLength;
  end;
  AUseTime := GetTickCount - nTBegin;
end;

function ReadAddrData(DWAddr: DWord; AProcMem: TProcessMemory; ADType: TDataType): DWord;
var
  mBuf: TRWBuffer;
  nLength: Integer;
begin
  Result := 0;
  if not Assigned(AProcMem) then
    Exit;

  nLength := GetRWLength(ADType);
  AProcMem.Position := DWAddr;
  AProcMem.Read(mBuf, nLength);
  Case ADType of
    dtByte: Result := Byte(PByte(@mBuf)^);
    dtWord: Result := Word(PWord(@mBuf)^);
    dtDWord: Result := DWord(PDWord(@mBuf)^);
  end;
end;

end.
