unit untSelectTarget;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ImgList, Buttons;

type
  TfrmTargetProc = class(TForm)
    PageControl: TPageControl;
    tsProgram: TTabSheet;
    lvProgram: TListView;
    Button1: TButton;
    tsProcess: TTabSheet;
    lvProcess: TListView;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    ImageList: TImageList;
    procedure Button1Click(Sender: TObject);
    procedure lvProgramData(Sender: TObject; Item: TListItem);
    procedure lvProcessData(Sender: TObject; Item: TListItem);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure FreeList;
    procedure GetIcons;
  public
    { Public declarations }
  end;

var
  frmTargetProc: TfrmTargetProc;

implementation

  uses untGetProc, untGetProcCommon;

{$R *.DFM}

procedure TfrmTargetProc.Button1Click(Sender: TObject);
var
  i, nProgId: Integer;
  dwProcid: DWord;
begin
  nProgId := 0;
  dwProcid := 0;
  if lvProgram.SelCount > 0 then
  begin
    nProgId := lvProgram.Selected.Index;
    dwProcId := pProgramInfo(frmGetProc.u_ProgList.Items[nProgId]).ProcessId;
    for i:= 0 to frmGetProc.u_ProcList.Count - 1 do
      if pProcessInfo(frmGetProc.u_ProcList.Items[i]).ProcessId = dwProcId then
      begin
        PageControl.ActivePageIndex := 1;
        lvProcess.Items[i].Selected := True;
        Break;
      end;
  end;
end;

procedure TfrmTargetProc.lvProgramData(Sender: TObject; Item: TListItem);
begin
  Item.Caption := pProgramInfo(frmGetProc.u_ProgList.Items[Item.Index]).WndCaption;
  Item.ImageIndex := Item.Index;
end;

procedure TfrmTargetProc.lvProcessData(Sender: TObject; Item: TListItem);
begin
  Item.Caption := pProcessInfo(frmGetProc.u_ProcList.Items[Item.Index]).ExeFile;
end;

procedure TfrmTargetProc.btnOkClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmTargetProc.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmTargetProc.FormCreate(Sender: TObject);
begin
  if Assigned(frmGetProc.u_ProcList) then
    FreeList;

  frmGetProc.u_ProcList := GetSysProcess;
  frmGetProc.u_ProgList := GetRunProgram;
  GetIcons;  

  lvProcess.Items.Count := frmGetProc.u_ProcList.Count;
  lvProgram.Items.Count := frmGetProc.u_ProgList.Count;

  lvProcess.Columns[0].Width := lvProcess.ClientWidth;
  lvProgram.Columns[0].Width := lvProgram.ClientWidth;
end;

procedure TfrmTargetProc.FreeList;
var
  pProcInfo: pProcessInfo;
  pProg : pProgramInfo;
  i: Integer;
begin
  for i:= frmGetProc.u_ProcList.Count - 1 downto 0 do
  begin
    pProcInfo := frmGetProc.u_ProcList.Items[i];
    Dispose(pProcInfo);
    frmGetProc.u_ProcList.Delete(i);
  end;
  FreeAndNil(frmGetProc.u_ProcList);

  for i := frmGetProc.u_ProgList.Count - 1 downto 0 do
  begin
    pProg := frmGetProc.u_ProgList.Items[i];
    Dispose(pProg);
    frmGetProc.u_ProgList.Delete(i);
  end;
  FreeAndNil(frmGetProc.u_ProgList);
end;

procedure TfrmTargetProc.GetIcons;
var
  i: Integer;
  hWnd, hIcon: THandle;
  icon: TIcon;
begin
  ImageList.Clear;
  for i:= 0 to frmGetProc.u_ProgList.Count - 1 do
  begin
    hWnd := pProgramInfo(frmGetProc.u_ProgList.Items[i]).HdlWnd;
    hIcon := GetClassLong(hWnd, GCL_HICONSM);
    icon := TIcon.Create;
    icon.Handle := hIcon;
    ImageList.AddIcon(icon);
    icon.Free;
  end;
end;

end.
