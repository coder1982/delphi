program ProcessInfo;

uses
  Forms,
  untGetProc in 'untGetProc.pas' {frmGetProc},
  untGetprocCommon in 'untGetprocCommon.pas',
  untSelectTarget in 'untSelectTarget.pas' {frmTargetProc};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TfrmGetProc, frmGetProc);
  Application.CreateForm(TfrmTargetProc, frmTargetProc);
  Application.Run;
end.
