// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diunicodestringvector.pas' rev: 20.00

#ifndef DiunicodestringvectorHPP
#define DiunicodestringvectorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diunicodestringvector
{
//-- type declarations -------------------------------------------------------
typedef System::UnicodeString TDINameType;

typedef System::UnicodeString *PDINameType;

#pragma pack(push,1)
struct TDIItemType
{
	
public:
	System::UnicodeString Name;
};
#pragma pack(pop)


typedef TDIItemType *PDIItemType;

class DELPHICLASS TDIUnicodeStringVector;
class PASCALIMPLEMENTATION TDIUnicodeStringVector : public Dicontainers::TDIVector
{
	typedef Dicontainers::TDIVector inherited;
	
protected:
	unsigned __fastcall GetMaxNameLength(void);
	void __fastcall SetMaxNameLength(const unsigned NewLength);
	System::UnicodeString __fastcall GetFirstName();
	void __fastcall SetFirstName(const System::UnicodeString AName);
	System::UnicodeString __fastcall GetLastName();
	void __fastcall SetLastName(const System::UnicodeString Name);
	System::UnicodeString __fastcall GetName(void * PItem);
	System::UnicodeString __fastcall GetNameAt(const int Index);
	System::UnicodeString __fastcall GetNameOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems);
	void __fastcall SetNameOf(void * PItem, const Dicontainers::TDISameItemsFunc SameItems, const System::UnicodeString Name);
	void __fastcall SetName(void * PItem, const System::UnicodeString Name);
	void __fastcall SetNameAt(const int Index, const System::UnicodeString Name);
	
public:
	__property System::UnicodeString Name[void * PItem] = {read=GetName, write=SetName};
	__property System::UnicodeString NameAt[const int Index] = {read=GetNameAt, write=SetNameAt};
	__property System::UnicodeString NameOf[void * PItem][const Dicontainers::TDISameItemsFunc SameItems] = {read=GetNameOf, write=SetNameOf};
	__property System::UnicodeString FirstName = {read=GetFirstName, write=SetFirstName};
	__property System::UnicodeString LastName = {read=GetLastName, write=SetLastName};
	int __fastcall InsertNameSortedCS(const System::UnicodeString AName);
	int __fastcall InsertNameSortedCI(const System::UnicodeString Name);
	int __fastcall InsertNameSortedCSDesc(const System::UnicodeString AName);
	int __fastcall InsertNameSortedCIDesc(const System::UnicodeString AName);
	bool __fastcall ExistsNameCS(const System::UnicodeString AName);
	bool __fastcall ExistsNameCI(const System::UnicodeString AName);
	bool __fastcall ExistsNameCSBack(const System::UnicodeString AName);
	bool __fastcall ExistsNameCIBack(const System::UnicodeString AName);
	bool __fastcall ExistsNameCSSorted(const System::UnicodeString Name);
	bool __fastcall ExistsNameCISorted(const System::UnicodeString Name);
	bool __fastcall ExistAllNamesCS(System::UnicodeString const *ANames, const int ANames_Size);
	bool __fastcall ExistAllNamesCI(System::UnicodeString const *ANames, const int ANames_Size);
	bool __fastcall ExistsAnyNameCS(System::UnicodeString const *ANames, const int ANames_Size);
	bool __fastcall ExistsAnyNameCI(System::UnicodeString const *ANames, const int ANames_Size);
	bool __fastcall FindNameCS(const System::UnicodeString Name, /* out */ int &Index);
	bool __fastcall FindNameCI(const System::UnicodeString Name, /* out */ int &Index);
	bool __fastcall FindNameCSDesc(const System::UnicodeString AName, /* out */ int &Index);
	bool __fastcall FindNameCIDesc(const System::UnicodeString AName, /* out */ int &Index);
	int __fastcall IndexOfNameCS(const System::UnicodeString AName);
	int __fastcall IndexOfNameCI(const System::UnicodeString AName);
	int __fastcall IndexOfNameCSBack(const System::UnicodeString AName);
	int __fastcall IndexOfNameCIBack(const System::UnicodeString AName);
	void * __fastcall InsertNameAt(const int Index, const System::UnicodeString AName);
	void * __fastcall InsertNameFirst(const System::UnicodeString AName);
	void * __fastcall InsertNameLast(const System::UnicodeString AName);
	void __fastcall InsertNamesLast(System::UnicodeString const *ANames, const int ANames_Size);
	__property unsigned MaxNameLength = {read=GetMaxNameLength, write=SetMaxNameLength, nodefault};
	void __fastcall RemoveAllNamesCS(const System::UnicodeString AName);
	void __fastcall RemoveAllNamesCI(const System::UnicodeString AName);
	void __fastcall RemoveAllNamesCSBack(const System::UnicodeString AName, const int StartIndex = 0xffffffff);
	void __fastcall RemoveAllNamesCIBack(const System::UnicodeString AName, const int StartIndex = 0xffffffff);
	void * __fastcall PItemOfNameCS(const System::UnicodeString Name, const int StartIndex = 0x0);
	void * __fastcall PItemOfNameCI(const System::UnicodeString Name)/* overload */;
	void * __fastcall PItemOfNameCI(const System::UnicodeString Name, const int StartIndex)/* overload */;
	void * __fastcall PItemOfNameCSBack(const System::UnicodeString Name);
	void * __fastcall PItemOfNameCIBack(const System::UnicodeString Name);
	void __fastcall SortByNameCS(void);
	void __fastcall SortByNameCI(void);
	void __fastcall SortByNameCSDesc(void);
	void __fastcall SortByNameCIDesc(void);
	void __fastcall SaveNamesToTextFile(const System::UnicodeString FileName);
	void __fastcall SaveNamesToTextStream(const Classes::TStream* Stream);
	void __fastcall AssignNamesToStrings(const Classes::TStrings* Strings);
	void __fastcall AssignStringsToNames(const Classes::TStrings* Strings);
	void __fastcall NamesToUpperCase(void);
	void __fastcall NamesToLowerCase(void);
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDIUnicodeStringVector(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDIUnicodeStringVector(const Dicontainers::TDIItemHandler* AItemHandler) : Dicontainers::TDIVector(AItemHandler) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_NAME_CS;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_NAME_CI;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_NAME_CS;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_NAME_CI;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_NAME_CS;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_NAME_CI;
extern PACKAGE TDIUnicodeStringVector* __fastcall NewDIUnicodeStringVector(void);

}	/* namespace Diunicodestringvector */
using namespace Diunicodestringvector;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiunicodestringvectorHPP
