// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Distreams.pas' rev: 20.00

#ifndef DistreamsHPP
#define DistreamsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Distreams
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TDIFilerType { ftUnknown, ftNInt08, ftPInt08, ftUInt08, ftNInt16, ftPInt16, ftUInt16, ftNInt24, ftPInt24, ftUInt24, ftNInt32, ftPInt32, ftUInt32, ftNInt40, ftPInt40, ftUInt40, ftNInt48, ftPInt48, ftUInt48, ftNInt56, ftPInt56, ftUInt56, ftNInt64, ftPInt64, ftUInt64, ftSingle, ftDouble, ftExtended, ftCurrency, ftAnsiString00, ftAnsiString08, ftAnsiString16, ftAnsiString24, ftAnsiString32, ftWideString00, ftWideString08, ftWideString16, ftWideString24, ftWideString32, ftBinary00, ftBinary08, ftBinary16, ftBinary24, ftBinary32, ftNull, ftNil, ftFalse, ftTrue };
#pragma option pop

class DELPHICLASS TDIFiler;
class PASCALIMPLEMENTATION TDIFiler : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Classes::TStream* FStream;
	void *FBuffer;
	unsigned FBufferSize;
	unsigned FBufferPosition;
	unsigned FBufferEnd;
	
public:
	__fastcall TDIFiler(const Classes::TStream* AStream, const unsigned BufferSize);
	__fastcall virtual ~TDIFiler(void);
	__property Classes::TStream* Stream = {read=FStream};
};


class DELPHICLASS TDIReader;
class PASCALIMPLEMENTATION TDIReader : public TDIFiler
{
	typedef TDIFiler inherited;
	
private:
	int __fastcall GetPosition(void);
	void __fastcall SetPosition(const int Value);
	void __fastcall ReadBuffer(void);
	
public:
	__fastcall virtual ~TDIReader(void);
	void __fastcall FlushBuffer(void);
	__property int Position = {read=GetPosition, write=SetPosition, nodefault};
	void __fastcall Read(/* out */ void *Buffer, const unsigned Count);
	void __fastcall SkipBytes(const unsigned Count);
public:
	/* TDIFiler.Create */ inline __fastcall TDIReader(const Classes::TStream* AStream, const unsigned BufferSize) : TDIFiler(AStream, BufferSize) { }
	
};


class DELPHICLASS TDIBinaryReader;
class PASCALIMPLEMENTATION TDIBinaryReader : public TDIReader
{
	typedef TDIReader inherited;
	
private:
	__classmethod void __fastcall ElementTypeError();
	
public:
	void __fastcall SkipElement(void);
	System::AnsiString __fastcall ReadAnsiString();
	void __fastcall SkipAnsiString(void);
	bool __fastcall ReadBoolean(void);
	void __fastcall SkipBoolean(void);
	System::Byte __fastcall ReadByte(void);
	void __fastcall SkipByte(void);
	void __fastcall ReadBinary(/* out */ void *Buffer, const unsigned BufferSize);
	void __fastcall SkipBinary(void);
	unsigned __fastcall ReadCardinal(void);
	void __fastcall SkipCardinal(void);
	System::Currency __fastcall ReadCurrency(void);
	void __fastcall SkipCurrency(void);
	double __fastcall ReadDouble(void);
	void __fastcall SkipDouble(void);
	System::Extended __fastcall ReadExtended(void);
	void __fastcall SkipExtended(void);
	TDIFilerType __fastcall ReadFilerType(void);
	void __fastcall SkipFilerType(void);
	__int64 __fastcall ReadInt64(void);
	void __fastcall SkipInt64(void);
	int __fastcall ReadInteger(void);
	void __fastcall SkipInteger(void);
	System::ShortInt __fastcall ReadShortInt(void);
	void __fastcall SkipShortInt(void);
	float __fastcall ReadSingle(void);
	void __fastcall SkipSingle(void);
	short __fastcall ReadSmallInt(void);
	void __fastcall SkipSmallInt(void);
	System::Variant __fastcall ReadVariant();
	System::WideString __fastcall ReadWideString();
	void __fastcall SkipWideString(void);
	System::Word __fastcall ReadWord(void);
	void __fastcall SkipWord(void);
	System::UnicodeString __fastcall ReadUnicodeString();
	void __fastcall SkipUnicodeString(void);
public:
	/* TDIReader.Destroy */ inline __fastcall virtual ~TDIBinaryReader(void) { }
	
public:
	/* TDIFiler.Create */ inline __fastcall TDIBinaryReader(const Classes::TStream* AStream, const unsigned BufferSize) : TDIReader(AStream, BufferSize) { }
	
};


class DELPHICLASS TDIWriter;
class PASCALIMPLEMENTATION TDIWriter : public TDIFiler
{
	typedef TDIFiler inherited;
	
private:
	unsigned __fastcall GetPosition(void);
	void __fastcall SetPosition(const unsigned Value);
	
public:
	__fastcall virtual ~TDIWriter(void);
	__fastcall ~TDIWriter(const unsigned Dummy = (unsigned)(0x0));
	void __fastcall ClearBuffer(void);
	void __fastcall FlushBuffer(void);
	__property unsigned Position = {read=GetPosition, write=SetPosition, nodefault};
	void __fastcall Write(const void *Buffer, const unsigned Count);
public:
	/* TDIFiler.Create */ inline __fastcall TDIWriter(const Classes::TStream* AStream, const unsigned BufferSize) : TDIFiler(AStream, BufferSize) { }
	
};


class DELPHICLASS TDIBinaryWriter;
class PASCALIMPLEMENTATION TDIBinaryWriter : public TDIWriter
{
	typedef TDIWriter inherited;
	
public:
	void __fastcall WriteAnsiString(const System::AnsiString Value);
	void __fastcall WriteBoolean(const bool Value);
	void __fastcall WriteBinary(const void *Buffer, const unsigned BufferSize);
	void __fastcall WriteByte(const System::Byte Value);
	void __fastcall WriteCardinal(const unsigned Value);
	void __fastcall WriteCurrency(const System::Currency Value);
	void __fastcall WriteDouble(const double Value);
	void __fastcall WriteExtended(const System::Extended Value);
	void __fastcall WriteFilerType(const TDIFilerType Value);
	void __fastcall WriteInt64(const __int64 Value);
	void __fastcall WriteInteger(const int Value);
	void __fastcall WriteShortInt(const System::ShortInt Value);
	void __fastcall WriteSingle(const float Value);
	void __fastcall WriteSmallInt(const short Value);
	void __fastcall WriteVariant(const System::Variant &Value);
	void __fastcall WriteWideString(const System::WideString Value);
	void __fastcall WriteWord(const System::Word Value);
	void __fastcall WriteUnicodeString(const System::UnicodeString Value);
public:
	/* TDIWriter.Destroy */ inline __fastcall virtual ~TDIBinaryWriter(void) { }
	/* TDIWriter.DestroyNoFlushBuffer */ inline __fastcall ~TDIBinaryWriter(const unsigned Dummy = (unsigned)(0x0)) { }
	
public:
	/* TDIFiler.Create */ inline __fastcall TDIBinaryWriter(const Classes::TStream* AStream, const unsigned BufferSize) : TDIWriter(AStream, BufferSize) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const Word FILER_BUFFER_SIZE = 0x1000;

}	/* namespace Distreams */
using namespace Distreams;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DistreamsHPP
