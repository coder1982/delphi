// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dihtmlcolors.pas' rev: 20.00

#ifndef DihtmlcolorsHPP
#define DihtmlcolorsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dihtmlcolors
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern PACKAGE bool __fastcall ColorFromHtml(const System::UnicodeString s, /* out */ unsigned &Color);
extern PACKAGE void __fastcall RegisterColor(const System::UnicodeString ColorName, const unsigned Color);
extern PACKAGE void __fastcall UnregisterColor(const System::UnicodeString ColorName);
extern PACKAGE unsigned __fastcall RegisteredColorID(const System::UnicodeString ColorName);
extern PACKAGE void __fastcall ClearColorRegistry(void);
extern PACKAGE int __fastcall ColorRegistryCount(void);
extern PACKAGE void __fastcall RegisterHtmlColors(void);

}	/* namespace Dihtmlcolors */
using namespace Dihtmlcolors;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DihtmlcolorsHPP
