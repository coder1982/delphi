// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diunicodestringhash.pas' rev: 20.00

#ifndef DiunicodestringhashHPP
#define DiunicodestringhashHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diunicodestringhash
{
//-- type declarations -------------------------------------------------------
typedef System::UnicodeString TDINameType;

typedef System::UnicodeString *PDINameType;

#pragma pack(push,1)
struct TDIItemType
{
	
public:
	System::UnicodeString Name;
};
#pragma pack(pop)


typedef TDIItemType *PDIItemType;

class DELPHICLASS TDIUnicodeStringHash;
class PASCALIMPLEMENTATION TDIUnicodeStringHash : public Dicontainers::TDIHash
{
	typedef Dicontainers::TDIHash inherited;
	
protected:
	System::UnicodeString __fastcall GetNameOfKey(const void *AKey);
	System::UnicodeString __fastcall GetNameOfKeyBuf(const void *Buffer, const unsigned BufferSize);
	PDINameType __fastcall GetPNameOfKey(const void *AKey);
	PDINameType __fastcall GetPNameOfKeyBuf(const void *Buffer, const unsigned BufferSize);
	void __fastcall SetNameOfKey(const void *AKey, const System::UnicodeString AName);
	void __fastcall SetNameOfKeyBuf(const void *Buffer, const unsigned BufferSize, const System::UnicodeString AName);
	void __fastcall SetPNameOfKey(const void *AKey, const PDINameType AName);
	void __fastcall SetPNameOfKeyBuf(const void *Buffer, const unsigned BufferSize, const PDINameType AName);
	System::UnicodeString __fastcall GetNameAt(const int Index);
	
public:
	__property System::UnicodeString NameAt[const int Index] = {read=GetNameAt};
	__property System::UnicodeString NameOfKey[void AKey] = {read=GetNameOfKey, write=SetNameOfKey};
	__property System::UnicodeString NameOfKeyBuf[void Buffer][const unsigned BufferSize] = {read=GetNameOfKeyBuf, write=SetNameOfKeyBuf};
	__property PDINameType PNameOfKey[void AKey] = {read=GetPNameOfKey, write=SetPNameOfKey};
	__property PDINameType PNameOfKeyBuf[void Buffer][const unsigned BufferSize] = {read=GetPNameOfKeyBuf, write=SetPNameOfKeyBuf};
	void * __fastcall InsertNameByKey(const void *AKey, const System::UnicodeString AName);
public:
	/* TDIHash.Create */ inline __fastcall TDIUnicodeStringHash(const Dicontainers::TDIItemHandler* AItemHandler, const Dicontainers::TDIKeyHandler* AKeyHandler) : Dicontainers::TDIHash(AItemHandler, AKeyHandler) { }
	/* TDIHash.Destroy */ inline __fastcall virtual ~TDIUnicodeStringHash(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_NAME_CS;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ELEMENT_NAME_CI;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_NAME_CS;
extern PACKAGE Dicontainers::TDICompareItemsFunc COMPARE_ITEM_NAME_CI;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_NAME_CS;
extern PACKAGE Dicontainers::TDISameItemsFunc SAME_ELEMENT_NAME_CI;
extern PACKAGE TDIUnicodeStringHash* __fastcall NewDIUnicodeStringHash(const Dicontainers::TDIKeyHandler* KeyHandler);

}	/* namespace Diunicodestringhash */
using namespace Diunicodestringhash;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiunicodestringhashHPP
