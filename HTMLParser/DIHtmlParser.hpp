// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dihtmlparser.pas' rev: 20.00

#ifndef DihtmlparserHPP
#define DihtmlparserHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Disystemcompat.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Ditypes.hpp>	// Pascal unit
#include <Diutils.hpp>	// Pascal unit
#include <Disearchinterface.hpp>	// Pascal unit
#include <Dihtmlmisc.hpp>	// Pascal unit
#include <Distreams.hpp>	// Pascal unit
#include <Diunicode.hpp>	// Pascal unit
#include <Dicontainers.hpp>	// Pascal unit
#include <Diunicodestring2cardinalvector.hpp>	// Pascal unit
#include <Diobjectvector.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dihtmlparser
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TDITagType { ttStartTag, ttEndTag, ttEmptyElementTag };
#pragma option pop

#pragma option push -b-
enum TDIFilter { fiHide, fiShowHidden, fiShowShown, fiShowLocal, fiShow };
#pragma option pop

struct TDINotificationRec;
typedef TDINotificationRec *PDINotificationRec;

__interface IDITagFiltersTarget;
typedef System::DelphiInterface<IDITagFiltersTarget> _di_IDITagFiltersTarget;
struct TDINotificationRec
{
	
public:
	TDINotificationRec *Next;
	_di_IDITagFiltersTarget Target;
	unsigned RefCount;
};


class DELPHICLASS TDITagFilters;
class PASCALIMPLEMENTATION TDITagFilters : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Dicontainers::TDIHash* FTags;
	TDINotificationRec *FFreeNotifiers;
	
protected:
	void __fastcall FreeNotification(const _di_IDITagFiltersTarget n);
	void __fastcall RemoveFreeNotification(const _di_IDITagFiltersTarget n);
	void __fastcall UpdateNotifiers(const unsigned ATagID);
	
public:
	__fastcall TDITagFilters(void);
	__fastcall virtual ~TDITagFilters(void);
	void __fastcall Clear(void);
	void __fastcall DeleteStart(const unsigned TagID);
	void __fastcall DeleteEnd(const unsigned TagID);
	void __fastcall DeleteStartEnd(const unsigned TagID);
	bool __fastcall ExistsStart(const unsigned TagID);
	bool __fastcall ExistsEnd(const unsigned TagID);
	bool __fastcall ExistsStartEnd(const unsigned TagID);
	void __fastcall SetStart(const unsigned TagID, const TDIFilter Value)/* overload */;
	void __fastcall SetStart(unsigned const *TagIDs, const int TagIDs_Size, const TDIFilter Value)/* overload */;
	void __fastcall SetEnd(const unsigned TagID, const TDIFilter Value)/* overload */;
	void __fastcall SetEnd(unsigned const *TagIDs, const int TagIDs_Size, const TDIFilter Value)/* overload */;
	void __fastcall SetStartEnd(const unsigned TagID, const TDIFilter Value)/* overload */;
	void __fastcall SetStartEnd(unsigned const *TagIDs, const int TagIDs_Size, const TDIFilter Value)/* overload */;
	void __fastcall SetStartEnd(const unsigned TagID, const TDIFilter StartFilter, const TDIFilter EndFilter)/* overload */;
	void __fastcall SetStartEnd(unsigned const *TagIDs, const int TagIDs_Size, const TDIFilter StartFilter, const TDIFilter EndFilter)/* overload */;
	bool __fastcall TestStart(const unsigned TagID, const TDIFilter Value);
	bool __fastcall TestEnd(const unsigned TagID, const TDIFilter Value);
	bool __fastcall TestStartEnd(const unsigned TagID, const TDIFilter Value)/* overload */;
	bool __fastcall TestStartEnd(const unsigned TagID, const TDIFilter StartFilter, const TDIFilter EndFilter)/* overload */;
};


__interface IDITagFiltersTarget  : public System::IInterface 
{
	
public:
	virtual void __fastcall Notify(const TDITagFilters* ASender) = 0 ;
	virtual void __fastcall TagChange(const TDITagFilters* Sender, const unsigned ATagID) = 0 ;
};

typedef Ditypes::TUnicodeString2Cardinal TDITagAttrib;

typedef Ditypes::TUnicodeString2Cardinal *PDITagAttrib;

class DELPHICLASS TDITag;
class PASCALIMPLEMENTATION TDITag : public Diunicodestring2cardinalvector::TDIUnicodeString2CardinalVector
{
	typedef Diunicodestring2cardinalvector::TDIUnicodeString2CardinalVector inherited;
	
private:
	unsigned FTagID;
	System::UnicodeString FTagName;
	TDITagType FTagType;
	
public:
	virtual void __fastcall Assign(Classes::TPersistent* Source);
	void __fastcall AssignDITag(const TDITag* Source);
	virtual void __fastcall Clear(void);
	void __fastcall ClearAttribs(void);
	bool __fastcall CopyAttrib(const TDITag* Source, const unsigned AttribId);
	bool __fastcall ForceAttrib(const unsigned AttribId, const System::UnicodeString AttribName, const System::UnicodeString AttribValue = L"");
	virtual void __fastcall ReadContainer(const Distreams::TDIBinaryReader* Reader, const Dicontainers::TDIStreamInfos StreamInfos = (Dicontainers::TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	virtual void __fastcall WriteContainer(const Distreams::TDIBinaryWriter* Writer, const Dicontainers::TDIStreamInfos StreamInfos = (Dicontainers::TDIStreamInfos() << siMagic << siVersion << siHeader ), const void * Extra = (void *)(0x0));
	__property unsigned TagID = {read=FTagID, nodefault};
	__property System::UnicodeString TagName = {read=FTagName, write=FTagName};
	__property TDITagType TagType = {read=FTagType, write=FTagType, nodefault};
	void __fastcall UpperCaseTagName(void);
	void __fastcall LowerCaseTagName(void);
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDITag(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDITag(const Dicontainers::TDIItemHandler* AItemHandler) : Diunicodestring2cardinalvector::TDIUnicodeString2CardinalVector(AItemHandler) { }
	
};


class DELPHICLASS TDIHtmlTag;
class PASCALIMPLEMENTATION TDIHtmlTag : public TDITag
{
	typedef TDITag inherited;
	
private:
	System::UnicodeString __fastcall GetCodeProp();
	void __fastcall SetCodeProp(const System::UnicodeString AValue);
	
protected:
	virtual bool __fastcall ConCatValue(const System::UnicodeString AValue, System::UnicodeString &d, unsigned &InUse, const Dihtmlmisc::TDIQuoteTags QuoteTagValues);
	
public:
	__property System::UnicodeString Code = {read=GetCodeProp, write=SetCodeProp};
	System::UnicodeString __fastcall GetCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1));
	System::UnicodeString __fastcall GetStartCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1));
	System::UnicodeString __fastcall GetEmptyElementCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1));
	System::UnicodeString __fastcall GetEndCode();
	void __fastcall FontSizeToAbsolute(void);
	void __fastcall FontSizeToRelative(void);
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDIHtmlTag(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDIHtmlTag(const Dicontainers::TDIItemHandler* AItemHandler) : TDITag(AItemHandler) { }
	
};


class DELPHICLASS TDICustomTag;
class PASCALIMPLEMENTATION TDICustomTag : public TDITag
{
	typedef TDITag inherited;
	
private:
	System::UnicodeString __fastcall GetCodeProp();
	void __fastcall SetCodeProp(const System::UnicodeString AValue);
	
protected:
	virtual bool __fastcall ConCatValue(const System::UnicodeString AValue, System::UnicodeString &d, unsigned &InUse, const Dihtmlmisc::TDIQuoteTags QuoteTagValues);
	
public:
	__property System::UnicodeString Code = {read=GetCodeProp, write=SetCodeProp};
	System::UnicodeString __fastcall GetCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1), const System::WideChar StartChar = (System::WideChar)(0x23));
	System::UnicodeString __fastcall GetStartCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1), const System::WideChar StartChar = (System::WideChar)(0x23));
	System::UnicodeString __fastcall GetEmptyElementCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1), const System::WideChar StartChar = (System::WideChar)(0x23));
	System::UnicodeString __fastcall GetEndCode(const System::WideChar StartChar = (System::WideChar)(0x23));
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDICustomTag(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDICustomTag(const Dicontainers::TDIItemHandler* AItemHandler) : TDITag(AItemHandler) { }
	
};


class DELPHICLASS TDISsiTag;
class PASCALIMPLEMENTATION TDISsiTag : public TDITag
{
	typedef TDITag inherited;
	
private:
	System::UnicodeString __fastcall GetCodeProp();
	void __fastcall SetCodeProp(const System::UnicodeString AValue);
	
protected:
	virtual bool __fastcall ConCatValue(const System::UnicodeString AValue, System::UnicodeString &d, unsigned &InUse, const Dihtmlmisc::TDIQuoteTags QuoteTagValues);
	
public:
	__property System::UnicodeString Code = {read=GetCodeProp, write=SetCodeProp};
	System::UnicodeString __fastcall GetCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1));
	System::UnicodeString __fastcall GetStartCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1));
	System::UnicodeString __fastcall GetEmptyElementCode(const Dihtmlmisc::TDIQuoteTags QuoteTagValues = (Dihtmlmisc::TDIQuoteTags)(0x1));
	System::UnicodeString __fastcall GetEndCode();
public:
	/* TDIVector.Destroy */ inline __fastcall virtual ~TDISsiTag(void) { }
	
public:
	/* TDIContainer.Create */ inline __fastcall TDISsiTag(const Dicontainers::TDIItemHandler* AItemHandler) : TDITag(AItemHandler) { }
	
};


#pragma option push -b-
enum TDIHtmlPieceType { ptUndefined, ptASP, ptCDataSection, ptComment, ptCustomTag, ptDTD, ptExclamationMarkup, ptHtmlPI, ptHtmlTag, ptPHP, ptScript, ptSsiTag, ptStyle, ptText, ptTitle, ptXmlPI };
#pragma option pop

#pragma pack(push,1)
struct TDIHtmlParserFilters
{
	
public:
	TDIFilter ASP;
	TDIFilter CDataSection;
	TDIFilter Comment;
	TDIFilter CustomTagStart;
	TDIFilter CustomTagEnd;
	TDIFilter DTD;
	TDIFilter ExclamationMarkup;
	TDIFilter HtmlPI;
	TDIFilter HtmlTagStart;
	TDIFilter HtmlTagEnd;
	TDIFilter PHP;
	TDIFilter Script;
	TDIFilter SsiTagStart;
	TDIFilter SsiTagEnd;
	TDIFilter Style;
	TDIFilter Text;
	TDIFilter Title;
	TDIFilter XmlPI;
};
#pragma pack(pop)


#pragma pack(push,1)
struct TDIHtmlParserMasks
{
	
public:
	unsigned ASP;
	unsigned CDataSection;
	unsigned Comment;
	unsigned CustomTag;
	unsigned DTD;
	unsigned ExclamationMarkup;
	unsigned HtmlPI;
	unsigned HtmlTag;
	unsigned PHP;
	unsigned Script;
	unsigned SsiTag;
	unsigned Style;
	unsigned Text;
	unsigned Title;
	unsigned XmlPI;
};
#pragma pack(pop)


class DELPHICLASS TDITagFilter;
typedef void __fastcall (__closure *TDITagFilterChangeEvent)(const TDITagFilter* Sender);

class PASCALIMPLEMENTATION TDITagFilter : public Classes::TPersistent
{
	typedef Classes::TPersistent inherited;
	
private:
	TDIFilter FStartTags;
	TDIFilter FEndTags;
	TDITagFilterChangeEvent FOnChange;
	void __fastcall SetStartTags(const TDIFilter Value);
	void __fastcall SetEndTags(const TDIFilter Value);
	
public:
	virtual void __fastcall Assign(Classes::TPersistent* Source);
	void __fastcall AssignDITagFilter(const TDITagFilter* Source);
	__property TDITagFilterChangeEvent OnChange = {read=FOnChange, write=FOnChange};
	void __fastcall SetStartEnd(const TDIFilter Value)/* overload */;
	void __fastcall SetStartEnd(const TDIFilter AStartTags, const TDIFilter AEndTags)/* overload */;
	
__published:
	__property TDIFilter StartTags = {read=FStartTags, write=SetStartTags, default=0};
	__property TDIFilter EndTags = {read=FEndTags, write=SetEndTags, default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TDITagFilter(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TDITagFilter(void) : Classes::TPersistent() { }
	
};


class DELPHICLASS TDIHtmlParser;
class DELPHICLASS TDIHtmlParserPlugin;
class PASCALIMPLEMENTATION TDIHtmlParser : public Diunicode::TDIUnicodeReader
{
	typedef Diunicode::TDIUnicodeReader inherited;
	
private:
	bool FStopParse;
	bool FEnableASP;
	bool FEnableCustomTags;
	bool FEnableHtmlTags;
	bool FEnablePHP;
	bool FEnableSSI;
	System::WideChar FCustomTagStartChar;
	unsigned FStartPos;
	unsigned FStartLine;
	unsigned FStartCol;
	TDICustomTag* FCustomTag;
	TDIHtmlTag* FHtmlTag;
	TDISsiTag* FSsiTag;
	TDIHtmlPieceType FPieceType;
	TDIHtmlPieceType FNextPieceType;
	unsigned FBitsASPs;
	unsigned FBitsCDataSections;
	unsigned FBitsComments;
	unsigned FBitsCustomTags;
	unsigned FBitsDTDs;
	unsigned FBitsExclamationMarkups;
	unsigned FBitsHtmlPIs;
	unsigned FBitsHtmlTags;
	unsigned FBitsPHPs;
	unsigned FBitsScripts;
	unsigned FBitsSsiTags;
	unsigned FBitsStyles;
	unsigned FBitsText;
	unsigned FBitsTitles;
	unsigned FBitsXmlPIs;
	System::UnicodeString FContentScriptType;
	System::UnicodeString FDefaultContentScriptType;
	TDITagFilter* FFilterCustomTags;
	TDITagFilter* FFilterHtmlTags;
	TDITagFilter* FFilterSsiTags;
	unsigned FMaskASPs;
	unsigned FMaskCDataSections;
	unsigned FMaskComments;
	unsigned FMaskCustomTags;
	unsigned FMaskDTDs;
	unsigned FMaskExclamationMarkups;
	unsigned FMaskHtmlPIs;
	unsigned FMaskHtmlTags;
	unsigned FMaskPHPs;
	unsigned FMaskScripts;
	unsigned FMaskSsiTags;
	unsigned FMaskStyles;
	unsigned FMaskText;
	unsigned FMaskTitles;
	unsigned FMaskXmlPIs;
	TDITagFilters* FCustomTagFilters;
	TDITagFilters* FHtmlTagFilters;
	TDITagFilters* FSsiTagFilters;
	Dicontainers::TDIHash* FCustomTags;
	Dicontainers::TDIHash* FHtmlTags;
	Dicontainers::TDIHash* FSsiTags;
	unsigned FPreFormattedNesting;
	TDIHtmlParserPlugin* FFirstEnabledPlugin;
	TDIHtmlParserPlugin* FFirstPlugin;
	TDIHtmlParserPlugin* FLastPlugin;
	bool FTrimAttribValues;
	bool __fastcall FindData(const System::UnicodeString Search);
	bool __fastcall FindDataI(const System::UnicodeString Search);
	System::WideChar * __fastcall FindInData(const Disearchinterface::_di_IDISearchInterface Searcher)/* overload */;
	System::WideChar * __fastcall FindInData(const System::UnicodeString Search)/* overload */;
	System::WideChar * __fastcall FindInDataI(const System::UnicodeString Search);
	bool __fastcall GetNormalizeWhiteSpace(void);
	void __fastcall SetNormalizeWhiteSpace(const bool Value);
	TDIFilter __fastcall GetFilterASPs(void);
	TDIFilter __fastcall GetFilterCDataSections(void);
	TDIFilter __fastcall GetFilterComments(void);
	TDIFilter __fastcall GetFilterDTDs(void);
	TDIFilter __fastcall GetFilterExclamationMarkups(void);
	TDIFilter __fastcall GetFilterHtmlPIs(void);
	TDIFilter __fastcall GetFilterPHPs(void);
	TDIFilter __fastcall GetFilterScripts(void);
	TDIFilter __fastcall GetFilterStyles(void);
	TDIFilter __fastcall GetFilterText(void);
	TDIFilter __fastcall GetFilterTitles(void);
	TDIFilter __fastcall GetFilterXmlPIs(void);
	void __fastcall LoadPlugin(const TDIHtmlParserPlugin* Plug);
	void __fastcall CustomTagFilterOptionChange(const TDITagFilter* Sender);
	void __fastcall HtmlTagFilterOptionChange(const TDITagFilter* Sender);
	void __fastcall SsiTagFilterOptionChange(const TDITagFilter* Sender);
	__classmethod bool __fastcall SetFilterBits(unsigned &Bits, const TDIFilter Value);
	void __fastcall SetFilterASPs(const TDIFilter Value);
	void __fastcall SetFilterCDataSections(const TDIFilter Value);
	void __fastcall SetFilterComments(const TDIFilter Value);
	void __fastcall SetFilterCustomTags(const TDITagFilter* Value);
	void __fastcall SetFilterDTDs(const TDIFilter Value);
	void __fastcall SetFilterExclamationMarkups(const TDIFilter Value);
	void __fastcall SetFilterHtmlPIs(const TDIFilter Value);
	void __fastcall SetFilterHtmlTags(const TDITagFilter* Value);
	void __fastcall SetFilterPHPs(const TDIFilter Value);
	void __fastcall SetFilterScripts(const TDIFilter Value);
	void __fastcall SetFilterSsiTags(const TDITagFilter* Value);
	void __fastcall SetFilterStyles(const TDIFilter Value);
	void __fastcall SetFilterText(const TDIFilter Value);
	void __fastcall SetFilterTitles(const TDIFilter Value);
	void __fastcall SetFilterXmlPIs(const TDIFilter Value);
	void __fastcall ConnectPluginEnabled(const TDIHtmlParserPlugin* Plugin);
	void __fastcall ConnectPluginFirst(const TDIHtmlParserPlugin* Plugin);
	void __fastcall ConnectPluginLast(const TDIHtmlParserPlugin* Plugin);
	void __fastcall DisconnectPlugin(const TDIHtmlParserPlugin* Plugin);
	unsigned __fastcall GetPluginCount(void);
	void __fastcall SetCustomTagFilters(const TDITagFilters* Value);
	void __fastcall SetHtmlTagFilters(const TDITagFilters* Value);
	void __fastcall SetSsiTagFilters(const TDITagFilters* Value);
	void __fastcall AddCustomTagFilters(const TDITagFilters* TagFilters);
	void __fastcall AddHtmlTagFilters(const TDITagFilters* TagFilters);
	void __fastcall AddSsiTagFilters(const TDITagFilters* TagFilters);
	void __fastcall UpdateCustomTags(const TDITagFilters* TagFilters);
	void __fastcall UpdateHtmlTags(const TDITagFilters* TagFilters);
	void __fastcall UpdateSsiTags(const TDITagFilters* TagFilters);
	void __fastcall QueryPlugins(void);
	void __fastcall QueryPluginsASPs(void);
	void __fastcall QueryPluginsCDataSections(void);
	void __fastcall QueryPluginsComments(void);
	void __fastcall QueryPluginsCustomTag(const unsigned TagID);
	void __fastcall QueryPluginsCustomTags(void);
	void __fastcall QueryPluginsDTDs(void);
	void __fastcall QueryPluginsExclamationMarkups(void);
	void __fastcall QueryPluginsHtmlPIs(void);
	void __fastcall QueryPluginsHtmlTag(const unsigned TagID);
	void __fastcall QueryPluginsHtmlTags(void);
	void __fastcall QueryPluginsPHPs(void);
	void __fastcall QueryPluginsScripts(void);
	void __fastcall QueryPluginsSsiTag(const unsigned TagID);
	void __fastcall QueryPluginsSsiTags(void);
	void __fastcall QueryPluginsStyles(void);
	void __fastcall QueryPluginsText(void);
	void __fastcall QueryPluginsTitles(void);
	void __fastcall QueryPluginsXmlPIs(void);
	
protected:
	void __fastcall ParseToCharGreaterThanSign(const System::WideChar c);
	void __fastcall ParseToDoubleHyphenMinus(void);
	void __fastcall ParseAttribs(const TDITag* ATag);
	void __fastcall ParseSsiAttribs(void);
	void __fastcall ParseCDataSection(void);
	void __fastcall ParseEntity(void);
	void __fastcall ParseEntityPre(void);
	void __fastcall ParseJavaScript(void);
	void __fastcall ParseScript(void);
	void __fastcall ParseStyle(void);
	void __fastcall ParseText(void);
	void __fastcall ParseTextPre(void);
	void __fastcall ParseTitle(void);
	void __fastcall ParseXmlPI(void);
	bool __fastcall PeekScriptEndTag(void);
	bool __fastcall PeekStyleEndTag(void);
	void __fastcall SkipToCharGreaterThanSign(const System::WideChar c);
	void __fastcall SkipToDoubleHyphenMinus(void);
	void __fastcall SkipAttribs(void);
	void __fastcall SkipSsiAttribs(void);
	void __fastcall SkipCDataSection(void);
	void __fastcall SkipJavaScript(void);
	void __fastcall SkipScript(void);
	void __fastcall SkipStyle(void);
	void __fastcall SkipText(void);
	void __fastcall SkipTitle(void);
	void __fastcall SkipXmlPI(void);
	bool __fastcall TagIsJavaScriptStart(const TDITag* Tag);
	bool __fastcall HandlePieces(void);
	bool __fastcall HandleASP(void);
	bool __fastcall HandleCDataSection(void);
	bool __fastcall HandleComment(void);
	bool __fastcall HandleCustomTag(void);
	bool __fastcall HandleDTD(void);
	bool __fastcall HandleExclamationMarkup(void);
	bool __fastcall HandleHtmlPI(void);
	bool __fastcall HandleHtmlTag(void);
	bool __fastcall HandlePHP(void);
	bool __fastcall HandleScript(void);
	bool __fastcall HandleSSI(void);
	bool __fastcall HandleStyle(void);
	bool __fastcall HandleText(void);
	bool __fastcall HandleTitle(void);
	bool __fastcall HandleXmlPI(void);
	void __fastcall Notify(const TDITagFilters* ASender);
	void __fastcall TagChange(const TDITagFilters* Sender, const unsigned ATagID);
	void __fastcall AndMasks(const unsigned Value);
	void __fastcall OrMasks(const unsigned Value);
	void __fastcall SaveMasks(TDIHtmlParserMasks &Masks);
	void __fastcall RestoreMasks(const TDIHtmlParserMasks &Masks);
	
public:
	__fastcall virtual TDIHtmlParser(Classes::TComponent* AOwner);
	__fastcall TDIHtmlParser(const Classes::TComponent* AOwner)/* overload */;
	__fastcall TDIHtmlParser(const Classes::TComponent* AOwner, const TDIFilter StartTags, const TDIFilter EndTags)/* overload */;
	__fastcall TDIHtmlParser(const Classes::TComponent* AOwner, const int Dummy)/* overload */;
	__fastcall virtual ~TDIHtmlParser(void);
	__property AutoFreeSourceStreams = {default=0};
	void __fastcall ClearPlugins(void);
	__property System::WideChar CustomTagStartChar = {read=FCustomTagStartChar, write=FCustomTagStartChar, default=35};
	__property TDIHtmlParserPlugin* FirstPlugin = {read=FFirstPlugin};
	__property TDIHtmlParserPlugin* LastPlugin = {read=FLastPlugin};
	void __fastcall InsertPluginFirst(const TDIHtmlParserPlugin* Plug);
	void __fastcall InsertPluginLast(const TDIHtmlParserPlugin* Plug);
	void __fastcall RemovePlugin(const TDIHtmlParserPlugin* Plug);
	void __fastcall MovePluginBefore(const TDIHtmlParserPlugin* Plugin, const TDIHtmlParserPlugin* TargetPlugin);
	void __fastcall MovePluginAfter(const TDIHtmlParserPlugin* Plugin, const TDIHtmlParserPlugin* TargetPlugin);
	void __fastcall MovePluginFirst(const TDIHtmlParserPlugin* Plug);
	void __fastcall MovePluginLast(const TDIHtmlParserPlugin* Plug);
	__property TDITagFilters* HtmlTagFilters = {read=FHtmlTagFilters, write=SetHtmlTagFilters};
	__property TDITagFilters* CustomTagFilters = {read=FCustomTagFilters, write=SetCustomTagFilters};
	__property TDITagFilters* SsiTagFilters = {read=FSsiTagFilters, write=SetSsiTagFilters};
	void __fastcall ParseAll(void);
	__property bool StopParse = {read=FStopParse, write=FStopParse, nodefault};
	bool __fastcall ParseNextPiece(void);
	bool __fastcall ParseNextComment(const TDIFilter Filter = (TDIFilter)(0x4));
	bool __fastcall ParseNextHtmlTag(const bool StartTag = true, const bool EndTag = true, const TDIFilter StartTagFilter = (TDIFilter)(0x4), const TDIFilter EndTagFilter = (TDIFilter)(0x4));
	bool __fastcall ParseNextText(const TDIFilter Filter = (TDIFilter)(0x4));
	bool __fastcall PieceIsHtmlEndTag(void)/* overload */;
	bool __fastcall PieceIsHtmlEndTag(const unsigned ATagID)/* overload */;
	bool __fastcall PieceIsHtmlStartTag(void)/* overload */;
	bool __fastcall PieceIsHtmlStartTag(const unsigned ATagID)/* overload */;
	__property TDIHtmlPieceType PieceType = {read=FPieceType, write=FPieceType, nodefault};
	void __fastcall Reset(void);
	void __fastcall ResetPlugins(void);
	void __fastcall SaveFilters(TDIHtmlParserFilters &Filters);
	void __fastcall RestoreFilters(const TDIHtmlParserFilters &Filters);
	__property unsigned StartPos = {read=FStartPos, nodefault};
	__property unsigned StartLine = {read=FStartLine, nodefault};
	__property unsigned StartCol = {read=FStartCol, nodefault};
	__property TDICustomTag* CustomTag = {read=FCustomTag};
	__property TDIHtmlTag* HtmlTag = {read=FHtmlTag};
	__property TDISsiTag* SsiTag = {read=FSsiTag};
	bool __fastcall FindComment(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4));
	bool __fastcall FindCommentI(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4));
	System::WideChar * __fastcall FindInComment(const Disearchinterface::_di_IDISearchInterface Searcher, const TDIFilter Filter = (TDIFilter)(0x4))/* overload */;
	System::WideChar * __fastcall FindInComment(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4))/* overload */;
	System::WideChar * __fastcall FindInCommentI(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4));
	bool __fastcall FindText(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4));
	bool __fastcall FindTextI(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4));
	System::WideChar * __fastcall FindInText(const Disearchinterface::_di_IDISearchInterface Searcher, const TDIFilter Filter = (TDIFilter)(0x4))/* overload */;
	System::WideChar * __fastcall FindInText(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4))/* overload */;
	System::WideChar * __fastcall FindInTextI(const System::UnicodeString Search, const TDIFilter Filter = (TDIFilter)(0x4));
	bool __fastcall FindHtmlTag(const System::UnicodeString ATagName, const bool StartTags = true, const bool EndTags = true, const TDIFilter StartTagFilter = (TDIFilter)(0x4), const TDIFilter EndTagFilter = (TDIFilter)(0x4))/* overload */;
	bool __fastcall FindHtmlTag(const unsigned ATagID, const bool StartTags = true, const bool EndTags = true, const TDIFilter StartTagFilter = (TDIFilter)(0x4), const TDIFilter EndTagFilter = (TDIFilter)(0x4))/* overload */;
	bool __fastcall FindSsiTag(const System::UnicodeString ATagName, const bool StartTags = true, const bool EndTags = true, const TDIFilter StartTagFilter = (TDIFilter)(0x4), const TDIFilter EndTagFilter = (TDIFilter)(0x4))/* overload */;
	bool __fastcall FindSsiTag(const unsigned ATagID, const bool StartTags = true, const bool EndTags = true, const TDIFilter StartTagFilter = (TDIFilter)(0x4), const TDIFilter EndTagFilter = (TDIFilter)(0x4))/* overload */;
	void __fastcall SetAllFilters(const TDIFilter Value);
	void __fastcall SetFullParser(void);
	void __fastcall SetNullParser(void);
	void __fastcall SetCommentsParser(void);
	void __fastcall SetHtmlTagsParser(const TDIFilter StartTags = (TDIFilter)(0x4), const TDIFilter EndTags = (TDIFilter)(0x4));
	void __fastcall SetTextParser(void);
	bool __fastcall ShowsASPs(void);
	bool __fastcall ShowsCDataSections(void);
	bool __fastcall ShowsComments(void);
	bool __fastcall ShowsDTDs(void);
	bool __fastcall ShowsExclamationMarkups(void);
	bool __fastcall ShowsHtmlPIs(void);
	bool __fastcall ShowsPHPs(void);
	bool __fastcall ShowsScripts(void);
	bool __fastcall ShowsStyles(void);
	bool __fastcall ShowsText(void);
	bool __fastcall ShowsTitles(void);
	bool __fastcall ShowsXmlPIs(void);
	bool __fastcall TextIsPreFormatted(void);
	__property System::UnicodeString ContentScriptType = {read=FContentScriptType};
	
__published:
	__property System::UnicodeString DefaultContentScriptType = {read=FDefaultContentScriptType, write=FDefaultContentScriptType};
	__property bool EnableASP = {read=FEnableASP, write=FEnableASP, default=0};
	__property bool EnableCustomTags = {read=FEnableCustomTags, write=FEnableCustomTags, default=0};
	__property bool EnableHtmlTags = {read=FEnableHtmlTags, write=FEnableHtmlTags, default=1};
	__property bool EnablePHP = {read=FEnablePHP, write=FEnablePHP, default=0};
	__property bool EnableSSI = {read=FEnableSSI, write=FEnableSSI, default=0};
	__property TDIFilter FilterASPs = {read=GetFilterASPs, write=SetFilterASPs, default=0};
	__property TDIFilter FilterCDataSections = {read=GetFilterCDataSections, write=SetFilterCDataSections, default=0};
	__property TDIFilter FilterComments = {read=GetFilterComments, write=SetFilterComments, default=0};
	__property TDITagFilter* FilterCustomTags = {read=FFilterCustomTags, write=SetFilterCustomTags};
	__property TDIFilter FilterDTDs = {read=GetFilterDTDs, write=SetFilterDTDs, default=0};
	__property TDIFilter FilterExclamationMarkups = {read=GetFilterExclamationMarkups, write=SetFilterExclamationMarkups, default=0};
	__property TDIFilter FilterHtmlPIs = {read=GetFilterHtmlPIs, write=SetFilterHtmlPIs, default=0};
	__property TDITagFilter* FilterHtmlTags = {read=FFilterHtmlTags, write=SetFilterHtmlTags};
	__property TDIFilter FilterPHPs = {read=GetFilterPHPs, write=SetFilterPHPs, default=0};
	__property TDIFilter FilterScripts = {read=GetFilterScripts, write=SetFilterScripts, default=0};
	__property TDITagFilter* FilterSsiTags = {read=FFilterSsiTags, write=SetFilterSsiTags};
	__property TDIFilter FilterStyles = {read=GetFilterStyles, write=SetFilterStyles, default=0};
	__property TDIFilter FilterText = {read=GetFilterText, write=SetFilterText, default=0};
	__property TDIFilter FilterTitles = {read=GetFilterTitles, write=SetFilterTitles, default=0};
	__property TDIFilter FilterXmlPIs = {read=GetFilterXmlPIs, write=SetFilterXmlPIs, default=0};
	__property bool NormalizeWhiteSpace = {read=GetNormalizeWhiteSpace, write=SetNormalizeWhiteSpace, default=0};
	__property unsigned PluginCount = {read=GetPluginCount, stored=false, nodefault};
	__property bool TrimAttribValues = {read=FTrimAttribValues, write=FTrimAttribValues, default=0};
private:
	void *__IDITagFiltersTarget;	/* IDITagFiltersTarget */
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	operator _di_IDITagFiltersTarget()
	{
		_di_IDITagFiltersTarget intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator IDITagFiltersTarget*(void) { return (IDITagFiltersTarget*)&__IDITagFiltersTarget; }
	#endif
	
};


typedef void __fastcall (__closure *TDIHtmlParserPluginNotifyEvent)(const TDIHtmlParserPlugin* Sender);

class PASCALIMPLEMENTATION TDIHtmlParserPlugin : public Classes::TComponent
{
	typedef Classes::TComponent inherited;
	
private:
	TDIHtmlParserPlugin* FNextEnabledPlugin;
	TDIHtmlParserPlugin* FPreviousPlugin;
	TDIHtmlParserPlugin* FNextPlugin;
	TDIHtmlParser* FHtmlParser;
	TDITagFilter* FFilterCustomTags;
	TDITagFilter* FFilterHtmlTags;
	TDITagFilter* FFilterSsiTags;
	TDITagFilters* FCustomTagFilters;
	TDITagFilters* FHtmlTagFilters;
	TDITagFilters* FSsiTagFilters;
	Dicontainers::TDIHash* FCustomTags;
	Dicontainers::TDIHash* FHtmlTags;
	Dicontainers::TDIHash* FSsiTags;
	unsigned FBitsASPs;
	unsigned FBitsCDataSections;
	unsigned FBitsComments;
	unsigned FBitsCustomTags;
	unsigned FBitsDTDs;
	unsigned FBitsExclamationMarkups;
	unsigned FBitsHtmlPIs;
	unsigned FBitsHtmlTags;
	unsigned FBitsPHPs;
	unsigned FBitsScripts;
	unsigned FBitsSsiTags;
	unsigned FBitsStyles;
	unsigned FBitsText;
	unsigned FBitsTitles;
	unsigned FBitsXmlPIs;
	bool FEnabled;
	void __fastcall SetCustomTagFilters(const TDITagFilters* Value);
	void __fastcall SetHtmlTagFilters(const TDITagFilters* Value);
	void __fastcall SetSsiTagFilters(const TDITagFilters* Value);
	void __fastcall SetEnabled(const bool Value);
	bool __fastcall SetFilterBits(unsigned &Bits, const TDIFilter Value);
	TDIFilter __fastcall GetFilterASPs(void);
	void __fastcall SetFilterASPs(const TDIFilter Value);
	TDIFilter __fastcall GetFilterCDataSections(void);
	void __fastcall SetFilterCDataSections(const TDIFilter Value);
	TDIFilter __fastcall GetFilterComments(void);
	void __fastcall SetFilterComments(const TDIFilter Value);
	void __fastcall SetFilterCustomTags(const TDITagFilter* Value);
	TDIFilter __fastcall GetFilterDTDs(void);
	void __fastcall SetFilterDTDs(const TDIFilter Value);
	TDIFilter __fastcall GetFilterExclamationMarkups(void);
	void __fastcall SetFilterExclamationMarkups(const TDIFilter Value);
	TDIFilter __fastcall GetFilterHtmlPIs(void);
	void __fastcall SetFilterHtmlPIs(const TDIFilter Value);
	void __fastcall SetFilterHtmlTags(const TDITagFilter* Value);
	TDIFilter __fastcall GetFilterPHPs(void);
	void __fastcall SetFilterPHPs(const TDIFilter Value);
	TDIFilter __fastcall GetFilterScripts(void);
	void __fastcall SetFilterScripts(const TDIFilter Value);
	void __fastcall SetFilterSsiTags(const TDITagFilter* Value);
	TDIFilter __fastcall GetFilterStyles(void);
	void __fastcall SetFilterStyles(const TDIFilter Value);
	TDIFilter __fastcall GetFilterText(void);
	void __fastcall SetFilterText(const TDIFilter Value);
	TDIFilter __fastcall GetFilterTitles(void);
	void __fastcall SetFilterTitles(const TDIFilter Value);
	TDIFilter __fastcall GetFilterXmlPIs(void);
	void __fastcall SetFilterXmlPIs(const TDIFilter Value);
	void __fastcall SetHtmlParser(const TDIHtmlParser* Value);
	
protected:
	virtual void __fastcall HandleASP(bool &Show);
	virtual void __fastcall HandleCDataSection(bool &Show);
	virtual void __fastcall HandleComment(bool &Show);
	virtual void __fastcall HandleCustomTag(bool &Show);
	virtual void __fastcall HandleDTD(bool &Show);
	virtual void __fastcall HandleExclamationMarkup(bool &Show);
	virtual void __fastcall HandleHtmlPI(bool &Show);
	virtual void __fastcall HandlePHP(bool &Show);
	virtual void __fastcall HandleScript(bool &Show);
	virtual void __fastcall HandleSsiTag(bool &Show);
	virtual void __fastcall HandleStyle(bool &Show);
	virtual void __fastcall HandleHtmlTag(bool &Show);
	virtual void __fastcall HandleText(bool &Show);
	virtual void __fastcall HandleTitle(bool &Show);
	virtual void __fastcall HandleXmlPI(bool &Show);
	__property bool Enabled = {read=FEnabled, write=SetEnabled, default=1};
	__property TDIHtmlParser* HtmlParser = {read=FHtmlParser, write=SetHtmlParser, default=0};
	__property TDIHtmlParserPlugin* PreviousPlugin = {read=FPreviousPlugin, stored=false};
	__property TDIHtmlParserPlugin* NextPlugin = {read=FNextPlugin, stored=false};
	__property TDIFilter FilterASPs = {read=GetFilterASPs, write=SetFilterASPs, default=0};
	__property TDIFilter FilterCDataSections = {read=GetFilterCDataSections, write=SetFilterCDataSections, default=0};
	__property TDIFilter FilterComments = {read=GetFilterComments, write=SetFilterComments, default=0};
	__property TDITagFilter* FilterCustomTags = {read=FFilterCustomTags, write=SetFilterCustomTags};
	__property TDIFilter FilterDTDs = {read=GetFilterDTDs, write=SetFilterDTDs, default=0};
	__property TDIFilter FilterExclamationMarkups = {read=GetFilterExclamationMarkups, write=SetFilterExclamationMarkups, default=0};
	__property TDIFilter FilterHtmlPIs = {read=GetFilterHtmlPIs, write=SetFilterHtmlPIs, default=0};
	__property TDITagFilter* FilterHtmlTags = {read=FFilterHtmlTags, write=SetFilterHtmlTags};
	__property TDIFilter FilterPHPs = {read=GetFilterPHPs, write=SetFilterPHPs, default=0};
	__property TDIFilter FilterScripts = {read=GetFilterScripts, write=SetFilterScripts, default=0};
	__property TDITagFilter* FilterSsiTags = {read=FFilterSsiTags, write=SetFilterSsiTags};
	__property TDIFilter FilterStyles = {read=GetFilterStyles, write=SetFilterStyles, default=0};
	__property TDIFilter FilterText = {read=GetFilterText, write=SetFilterText, default=0};
	__property TDIFilter FilterTitles = {read=GetFilterTitles, write=SetFilterTitles, default=0};
	__property TDIFilter FilterXmlPIs = {read=GetFilterXmlPIs, write=SetFilterXmlPIs, default=0};
	virtual void __fastcall Reset(void);
	__property TDITagFilters* CustomTagFilters = {read=FCustomTagFilters, write=SetCustomTagFilters};
	__property TDITagFilters* HtmlTagFilters = {read=FHtmlTagFilters, write=SetHtmlTagFilters};
	__property TDITagFilters* SsiTagFilters = {read=FSsiTagFilters, write=SetSsiTagFilters};
	void __fastcall CustomTagFilterOptionChange(const TDITagFilter* Sender);
	void __fastcall HtmlTagFilterOptionChange(const TDITagFilter* Sender);
	void __fastcall SsiTagFilterOptionChange(const TDITagFilter* Sender);
	void __fastcall Notify(const TDITagFilters* ASender);
	void __fastcall TagChange(const TDITagFilters* Sender, const unsigned ATagID);
	
public:
	__fastcall virtual TDIHtmlParserPlugin(Classes::TComponent* AOwner);
	__fastcall virtual ~TDIHtmlParserPlugin(void);
	__classmethod void __fastcall Error(const System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size);
	void __fastcall SetAllFilters(const TDIFilter Value);
private:
	void *__IDITagFiltersTarget;	/* IDITagFiltersTarget */
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	operator _di_IDITagFiltersTarget()
	{
		_di_IDITagFiltersTarget intf;
		GetInterface(intf);
		return intf;
	}
	#else
	operator IDITagFiltersTarget*(void) { return (IDITagFiltersTarget*)&__IDITagFiltersTarget; }
	#endif
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE StaticArray<TDIFilter, 2> BooleanToHideOrShow;
extern PACKAGE StaticArray<TDIFilter, 2> BooleanToHideOrShowLocal;
extern PACKAGE StaticArray<TDIFilter, 2> BooleanToHideOrShowShown;
extern PACKAGE TDIHtmlTag* __fastcall NewDIHtmlTag(void);
extern PACKAGE TDICustomTag* __fastcall NewDICustomTag(void);
extern PACKAGE TDISsiTag* __fastcall NewDISsiTag(void);
extern PACKAGE void __fastcall RegisterHtmlTags(void);
extern PACKAGE void __fastcall RegisterSsiTags(void);
extern PACKAGE void __fastcall RegisterTag(const System::UnicodeString TagName, const unsigned TagID);
extern PACKAGE void __fastcall UnregisterTag(const System::UnicodeString TagName);
extern PACKAGE unsigned __fastcall RegisteredTagID(const System::UnicodeString TagName);
extern PACKAGE void __fastcall ClearTagRegistry(void);
extern PACKAGE int __fastcall TagRegistryCount(void);
extern PACKAGE void __fastcall RegisterHtmlAttribs(void);
extern PACKAGE void __fastcall RegisterSsiAttribs(void);
extern PACKAGE void __fastcall RegisterAttrib(const System::UnicodeString AttribName, const unsigned AttribId);
extern PACKAGE void __fastcall UnRegisterAttrib(const System::UnicodeString AttribName);
extern PACKAGE unsigned __fastcall RegisteredAttribID(const System::UnicodeString AttribName);
extern PACKAGE void __fastcall ClearAttribRegistry(void);
extern PACKAGE int __fastcall AttribRegistryCount(void);
extern PACKAGE void __fastcall RegisterStandardDecodingEntities(void);
extern PACKAGE void __fastcall RegisterDecodingEntity(const System::UnicodeString Name, const System::UnicodeString Code);
extern PACKAGE void __fastcall RegisterDecodingEntities(Dihtmlmisc::TDIHtmlEntity const *Entities, const int Entities_Size);
extern PACKAGE void __fastcall RegisterHtmlDecodingEntities(void);
extern PACKAGE void __fastcall UnRegisterDecodingEntity(const System::UnicodeString Entity);
extern PACKAGE System::UnicodeString __fastcall RegisteredDecodingEntity(const System::UnicodeString Entity);
extern PACKAGE void __fastcall ClearDecodingEntities(void);
extern PACKAGE int __fastcall DecodingEntitiesCount(void);
extern PACKAGE System::AnsiString __fastcall HtmlExtractTextA(const System::AnsiString HTML, const Diunicode::TDIUnicodeReadMethods &AReadMethods, const unsigned OutCodePage = (unsigned)(0x0));
extern PACKAGE System::UnicodeString __fastcall HtmlExtractTextW(const System::UnicodeString HTML);

}	/* namespace Dihtmlparser */
using namespace Dihtmlparser;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DihtmlparserHPP
