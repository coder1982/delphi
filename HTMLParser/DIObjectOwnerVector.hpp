// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Diobjectownervector.pas' rev: 20.00

#ifndef DiobjectownervectorHPP
#define DiobjectownervectorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Diobjectvector.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Diobjectownervector
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Diobjectvector::TDIObjectVector* __fastcall NewDIObjectOwnerVector(void);

}	/* namespace Diobjectownervector */
using namespace Diobjectownervector;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DiobjectownervectorHPP
