// CodeGear C++Builder
// Copyright (c) 1995, 2008 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dihtmlparser_base.pas' rev: 20.00

#ifndef Dihtmlparser_baseHPP
#define Dihtmlparser_baseHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dihtmlparser_base
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __stdcall w(unsigned bX6);
extern PACKAGE unsigned __stdcall P(unsigned wFz3WM67, unsigned I3AmisGdXxP, unsigned Iywh6QGFr0FNhK, unsigned vOJHlJf9ws);
extern PACKAGE unsigned __stdcall j(unsigned AWMPQ5SihD);
extern PACKAGE void __stdcall X(void);
extern PACKAGE unsigned __stdcall A(unsigned E0V35Tlpvxe, unsigned wsmoPh7, unsigned fN0QOAoAKPWC, unsigned niQ_LVbtMm3Sk, unsigned dlXDVz);
extern PACKAGE void __stdcall V(unsigned jG, unsigned Zc, unsigned z2GUHg, unsigned l4fb8o_1cytEua, unsigned nuEVbxjTf6fB);
extern PACKAGE void __stdcall r(void);
extern PACKAGE unsigned __stdcall z(unsigned O36, unsigned L4Tvuu, unsigned xjiXa_vu);
extern PACKAGE unsigned __stdcall m(unsigned RrxwIAJ7, unsigned uS);
extern PACKAGE unsigned __stdcall L(void);
extern PACKAGE void __stdcall i(void);
extern PACKAGE void __stdcall t(unsigned tmf47jX, unsigned lFm, unsigned KRbd, unsigned nq1);
extern PACKAGE unsigned __stdcall k(unsigned sw_LnB);
extern PACKAGE void __stdcall e(unsigned GQ9Y5wv, unsigned xJwsFeL, unsigned Jl8_F2DQwT7, unsigned Hw);

}	/* namespace Dihtmlparser_base */
using namespace Dihtmlparser_base;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Dihtmlparser_baseHPP
